//
//  VideoConnection.swift
//  MLS Connect
//
//  Created by Chris Mutkoski on 4/27/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation
import Alamofire


class VideoConnection {

    var groupCallSessionID: String?
    var groupCallToken: String?
    
    func getVideoGroupCallToken(groupCallID: String, name: String, completion: (result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "GETUSERGROUPCALLTOKEN",
            "groupCallID": groupCallID,
            "groupCallName": name
        ]
        
        let options = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        let jsonString = String(data: options, encoding: NSUTF8StringEncoding)
        let jsonParams = ["json" : jsonString as! AnyObject]
        
        Alamofire.request(.POST, MSLConnect_Constants.baseURL, parameters: jsonParams)
            .responseJSON { response in
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                
                if let JSON = response.result.value {
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON as! NSDictionary

                        let result = response.objectForKey("result") as! NSDictionary
                        
                        self.groupCallSessionID = result["groupcall_session_id"] as? String
                        self.groupCallToken = result["token"] as? String

                        completion(result: "success")
                    }
                    else {
                        
                        completion(result: JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion(result: "No data returned from server.")
                }
        }
    }

    func getVideoEmailLinkToken(uID: String, uGUID: String, meetingGUID: String, completion: (result: String, meetingInfo: [String:AnyObject]) -> Void) {
        
        // Get token and group list
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "GETEMAILMEETINGTOKEN",
            "uID": uID,
            "uGUID": uGUID,
            "meetingID": meetingGUID
            ] as [String:AnyObject!]

        let options = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        let jsonString = String(data: options, encoding: NSUTF8StringEncoding)
        let jsonParams = ["json" : jsonString as! AnyObject]
        
        Alamofire.request(.GET, MSLConnect_Constants.baseURL, parameters: jsonParams)
            .responseJSON { response in
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                
                if let JSON = response.result.value {
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON as! NSDictionary
                        
                        let result = response.objectForKey("result") as! NSDictionary

                        self.groupCallSessionID = result["groupcall_session_id"] as? String
                        self.groupCallToken = result["token"] as? String

                        let meetingInfo: [String:AnyObject!] = [
                            "meetingID" : meetingGUID,
                            "meetingTime" : result.objectForKey("meetingTime"),
                            "uID" : result.objectForKey("uID"),
                            "token" : result.objectForKey("token"),
                            "users" : result.objectForKey("users"),
                            "guests" : result.objectForKey("guests")
                        ]

                        completion(result: "success", meetingInfo: meetingInfo)
                    }
                    else {
                        
                        completion(result: JSON["code"] as! String, meetingInfo: [:])
                    }
                }
                else {
                    
                    completion(result: "Error parsing GETEMAILMEETINGTOKEN response JSON", meetingInfo: [:])
                }
        }
    }

}

