//
//  GroupTag.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 8/9/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation


class GroupTag {
    
    var gID: String             // Group Tag ID
    var text : String           // Group Tag Text
    
    init(gID: String, text: String) {
        
        self.gID = gID
        self.text = text
    }
}
