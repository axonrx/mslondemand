//
//  PushNotifications.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 1/13/17.
//  Copyright © 2017 Chris Mutkoski. All rights reserved.
//


import Foundation
import Alamofire


class PushNotifications {

    func registerUserAPNS(deviceToken: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "REGISTERUSER",
            "uID": userList.getLoginUserID(),
            "devicetoken": deviceToken
        ]

        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .post, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    if JSON["status"] as! String == "success" {

                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func unRegisterUserAPNS(deviceToken: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "UNREGISTERUSER",
            "uID": userList.getLoginUserID(),
            "devicetoken": deviceToken
        ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .post, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    if JSON["status"] as! String == "success" {
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func setUserAPNS(message: String, sound: Bool, badge: Bool, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "SENDPUSHTOUSER",
            "uID": userList.getLoginUserID(),
            "message": message,
            "sound": sound,
            "sound": badge
        ] as [String : Any]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .post, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    if JSON["status"] as! String == "success" {
                        
                        print(JSON["code"] as! String)
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

}
