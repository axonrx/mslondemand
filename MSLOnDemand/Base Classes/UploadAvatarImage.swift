//
//  UploadAvatarImage.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/6/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import MBProgressHUD


class UploadAvatarImage {
    
    static func uploadAvatarImageToAxonServer(_ uID: String, fileUrl: URL, hud: MBProgressHUD, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "SAVEUSERPIC",
            "uID": uID,
            ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        if !FileManager.default.fileExists(atPath: fileUrl.path) {
            
            print("file not found")
            completion("failed: file not found")
            return
        }
        
        let fileData = try? Data(contentsOf: fileUrl)
        let fileName = fileUrl.lastPathComponent

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(fileData!, withName: "headshot", fileName: fileName, mimeType: "image/jpeg")
            
            for (key, value) in jsonParams {
                
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: MSLConnect_Constants.baseURL)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress { progress in
                    
                    DispatchQueue.main.async {
                        
                        hud.detailsLabel.text = "100/"+String((progress.fractionCompleted * 100))
                    }

                    print(progress.fractionCompleted)
                }

                upload.responseJSON { response in
                    
                    print(response.result)
                    completion("success")
                }
                
            case .failure(let encodingError):
                print(encodingError.localizedDescription)
                completion("failure")
            }
        }
    }
}
