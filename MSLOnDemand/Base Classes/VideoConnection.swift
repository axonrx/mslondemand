//
//  VideoConnection.swift
//  MLS Connect
//
//  Created by Chris Mutkoski on 4/27/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation
import Alamofire


class VideoConnection {

    var groupCallSessionID: String?
    var groupCallToken: String?
    var groupSIPCallToken: String?
    
    func getVideoGroupCallToken(_ groupCallID: String, name: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "GETUSERGROUPCALLTOKEN",
            "groupCallID": groupCallID,
            "groupCallName": name
        ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        
                        let result = response.object(forKey: "result") as! NSDictionary
                        
                        self.groupCallSessionID = result["groupcall_session_id"] as? String
                        self.groupCallToken = result["token"] as? String
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func getSIPGroupCallToken(_ groupCallID: String, name: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "GETUSERGROUPCALLTOKEN",
            "groupCallID": groupCallID,
            "groupCallName": name
        ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        
                        let result = response.object(forKey: "result") as! NSDictionary
                        
                        self.groupCallSessionID = result["groupcall_session_id"] as? String
                        self.groupSIPCallToken = result["token"] as? String
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func getVideoEmailLinkToken(_ uID: String, uGUID: String, meetingGUID: String, completion: @escaping (_ result: String, _ meetingInfo: [String:AnyObject]) -> Void) {
        
        // Get token and group list
        let parameters = [
            "token": MSLConnect_Constants.apiToken as ImplicitlyUnwrappedOptional<AnyObject>,
            "command": "GETEMAILMEETINGTOKEN" as Optional<AnyObject>,
            "uID": uID as Optional<AnyObject>,
            "uGUID": uGUID as Optional<AnyObject>,
            "meetingID": meetingGUID as Optional<AnyObject>
            ] as [String:AnyObject?]

        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        
                        let result = response.object(forKey: "result") as! NSDictionary
                        
                        self.groupCallSessionID = result["groupcall_session_id"] as? String
                        self.groupCallToken = result["token"] as? String
                        
                        let meetingInfo: [String:AnyObject?] = [
                            "meetingID" : meetingGUID as Optional<AnyObject>,
                            "meetingTime" : result.object(forKey: "meetingTime") as Optional<AnyObject>,
                            "uID" : result.object(forKey: "uID") as Optional<AnyObject>,
                            "token" : result.object(forKey: "token") as Optional<AnyObject>,
                            "users" : result.object(forKey: "users") as Optional<AnyObject>,
                            "guests" : result.object(forKey: "guests") as Optional<AnyObject>
                        ]
                        
                        completion("success", meetingInfo as [String : AnyObject])
                    }
                    else {
                        
                        completion(JSON["code"] as! String, [:])
                    }
                }
                else {
                    
                    completion("Error parsing GETEMAILMEETINGTOKEN response JSON", [:])
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed", [:])
                break
                
            }
        }
    }

}

