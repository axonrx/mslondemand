//
//  GlobalSignalHandler.swift
//  MLS Connect
//
//  Created by Chris Mutkoski on 4/25/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation
import Alamofire


class GlobalSignalHandler: NSObject {
    
    var signalSession : OTSession?
    var uID: String?


    // MARK: - Setup Signal Methods

    func setupGlobalSignalSession(_ completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "GETSIGNALSESSIONID",
            "appID": 666
        ] as [String : Any]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        
                        let result = response.object(forKey: "result") as! NSDictionary
                        
                        let sessionID = result.object(forKey: "signal_session_id") as! String
                        let token = result.object(forKey: "token") as! String
                        
                        if self.signalSession != nil {
                            
                            var error : OTError?
                            self.signalSession?.disconnect(&error)
                            if error != nil {
                                
                                print("Disconnect global signal error ", error!)
                            } else {
                                
                                print("Disconnect global signal success")
                            }
                        }
                        
                        self.signalSession = OTSession(apiKey: ApiKey, sessionId: sessionID, delegate: self)
                        var error : OTError?
                        if let session = self.signalSession {
                            
                            session.connect(withToken: token, error: &error)
                            if let error = error {
                                
                                print("FAILED TO CREATE GLOBAL SIGNAL SESSION -> ", error.localizedDescription)
                            }
                        }
                        
                        completion("success")
                    }
                    else {
                        
                        completion("failed")
                    }
                }
                else {
                    
                    print("Error parsing GETGROUPCALLS response JSON")
                    completion("failed")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func disconnectGlobalSignalSession() {
        
        var error : OTError?
        self.signalSession?.disconnect(&error)
        if error != nil {
            
            print("Disconnect global signal error ", error!)
        } else {
            
            print("Disconnect global signal success")
        }
    }

    // MARK: - Send Signal Methods

    func sendGlobalSignalClientMute(_ uID: String) {

        var error : OTError?
        self.signalSession!.signal(withType: "global_msg_client_mute", string: uID, connection: nil, retryAfterReconnect:true, error: &error)
        if error != nil {
            
            print("client mute signal error ", error!)
        } else {
            
            print("client mute signal sent")
        }
    }

    func sendGlobalSignalClientUnMute(_ uID: String) {
        
        var error : OTError?
        self.signalSession!.signal(withType: "global_msg_client_unmute", string: uID, connection: nil, retryAfterReconnect:true, error: &error)
        if error != nil {
            
            print("client unmute signal error ", error!)
        } else {
            
            print("client unmute signal sent")
        }
    }

    func sendGlobalSignalClientDisconnect(_ uID: String) {
        
        var error : OTError?
        self.signalSession!.signal(withType: "global_msg_client_disconnect", string: uID, connection: nil, retryAfterReconnect:true, error: &error)
        if error != nil {
            
            print("client disconnect signal error ", error!)
        } else {
            
            print("client disconnect signal sent")
        }
    }

    func sendGlobalSignalClientSwitchVideoToUser(_ uID: String) {
        
        var error : OTError?
        self.signalSession!.signal(withType: "global_msg_client_switch_video_to_user", string: uID, connection: nil, retryAfterReconnect:true, error: &error)
        if error != nil {
            
            print("client switch video to user signal error ", error!)
        } else {
            
            print("client switch video to user signal sent")
        }
    }
    
    func sendGlobalSignalVideoCallRequest(_ groupID: String, groupCallName: String, users: Array <String>) {
        
        do {

            let dic = ["groupCallID":  groupID, "groupCallName": groupCallName, "uID" : userList.getLoginUserID(), "users": users] as [String : Any]

            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.ascii.rawValue) as! String

            var otError : OTError?
            self.signalSession!.signal(withType: "global_msg_video_call_request", string: jsonString, connection: nil, retryAfterReconnect:true, error: &otError)
            if otError != nil {
                
                print("video call request signal error ", otError!)
            } else {
                
                print("video call request signal sent")
            }

        } catch let error as NSError {
            
            print(error)
        }
    }

    func sendGlobalSignalPresenterCallRequest(_ groupID: String, groupCallName: String, users: Array <String>) {
        
        do {
            
            let dic = ["groupCallID":  groupID, "groupCallName": groupCallName, "uID" : userList.getLoginUserID(), "users": users] as [String : Any]
            
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.ascii.rawValue) as! String
            
            var otError : OTError?
            self.signalSession!.signal(withType: "global_msg_presenter_call_request", string: jsonString, connection: nil, retryAfterReconnect:true, error: &otError)
            if otError != nil {
                
                print("presenter call request signal error ", otError!)
            } else {
                
                print("presenter call request signal sent")
            }
            
        } catch let error as NSError {
            
            print(error)
        }
    }

    func sendGlobalSignalAudioCallRequest(_ groupID: String, groupCallName: String, users: Array <String>) {
        
        do {
            
            let dic = ["groupCallID":  groupID, "groupCallName": groupCallName, "uID" : userList.getLoginUserID(), "users": users] as [String : Any]
            
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.ascii.rawValue) as! String
            
            var otError : OTError?
            self.signalSession!.signal(withType: "global_msg_audio_call_request", string: jsonString, connection: nil, retryAfterReconnect:true, error: &otError)
            if otError != nil {
                
                print("audio call request signal error ", otError!)
            } else {
                
                print("audio call request signal sent")
            }
            
        } catch let error as NSError {
            
            print(error)
        }
    }

    func sendGlobalSignalScreenShareNotification(_ groupID: String, fileName: String, users: Array <String>) {
        
        do {
            
            let dic = ["groupCallID":  groupID, "fileName": fileName, "uID" : userList.getLoginUserID(), "users": users] as [String : Any]
            
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.ascii.rawValue) as! String
            
            var otError : OTError?
            self.signalSession!.signal(withType: "global_msg_screenshare_notification", string: jsonString, connection: nil, retryAfterReconnect:true, error: &otError)
            if otError != nil {
                
                print("screen share notification signal error ", otError!)
            } else {
                
                print("screen share notification signal sent")
            }
            
        } catch let error as NSError {
            
            print(error)
        }
    }

    func sendGlobalSignalChat(_ gID: String, text: String) {
        
        do {
            
            let dic = ["groupCallID":  gID, "uID" : userList.getLoginUserID(), "text": text]
            
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.ascii.rawValue) as! String
            
            var otError : OTError?
            self.signalSession!.signal(withType: "global_msg_client_chat", string: jsonString, connection: nil, retryAfterReconnect:true, error: &otError)
            if otError != nil {
                
                print("chat signal error ", otError!)
            } else {
                
                print("chat signal sent")
            }
            
        } catch let error as NSError {
            
            print(error)
        }
    }

    func sendGlobalSignalQuestion(_ gID: String, text: String) {
        
        do {
            
            let dic = ["groupCallID":  gID, "uID" : userList.getLoginUserID(), "question_text": text]
            
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.ascii.rawValue) as! String
            
            var otError : OTError?
            self.signalSession!.signal(withType: "global_msg_client_question", string: jsonString, connection: nil, retryAfterReconnect:true, error: &otError)
            if otError != nil {
                
                print("question signal error ", otError!)
            } else {
                
                print("question signal sent")
            }
            
        } catch let error as NSError {
            
            print(error)
        }
    }

    func sendGlobalSignalLeaderQuestion(_ gID: String, leader: String, text: String) {
        
        do {
            
            let dic = ["groupCallID":  gID, "leader": leader, "uID" : userList.getLoginUserID(), "question_text": text]
            
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.ascii.rawValue) as! String
            
            var otError : OTError?
            self.signalSession!.signal(withType: "global_msg_client_leader_question", string: jsonString, connection: nil, retryAfterReconnect:true, error: &otError)
            if otError != nil {
                
                print("question signal error ", otError!)
            } else {
                
                print("question signal sent")
            }
            
        } catch let error as NSError {
            
            print(error)
        }
    }

    func sendGlobalSignalRejectCall(_ gID: String) {
        
        do {
            
            let dic = ["groupCallID":  gID, "uID" : userList.getLoginUserID()]
            
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.ascii.rawValue) as! String
            
            var otError : OTError?
            self.signalSession!.signal(withType: "global_msg_client_call_reject", string: jsonString, connection: nil, retryAfterReconnect:true, error: &otError)
            if otError != nil {
                
                print("call reject signal error ", otError!)
            } else {
                
                print("call reject signal sent")
            }
            
        } catch let error as NSError {
            
            print(error)
        }
    }

    func sendGlobalSignalUserStatusPing(_ status: UserOnlineStatus) {
        
        do {
            
            let dic = ["uID" : userList.getLoginUserID(), "status": status.rawValue] as [String : Any]
            
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.ascii.rawValue) as! String
            
            var otError : OTError?
            self.signalSession!.signal(withType: "global_msg_client_state_ping", string: jsonString, connection: nil, retryAfterReconnect:true, error: &otError)
            if otError != nil {
                
                print("client status ping signal error ", otError!)
            }

        } catch let error as NSError {
            
            print(error)
        }
    }

    // MARK: - Helper Methods

    func convertStringToDictionary(_ text: String) -> [String:AnyObject]? {
        
        if let data = text.data(using: String.Encoding.utf8) {
            
            do {
                
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                
                print("Error parsing global_msg JSON string")
            }
        }
        return nil
    }

}

extension GlobalSignalHandler: OTSessionDelegate {
    
    func sessionDidConnect(_ session: OTSession) {
        
        print("GLOBAL->sessionDidConnect (\(session.sessionId))")
    }
    
    func sessionDidDisconnect(_ session : OTSession) {
        
        print("GLOBAL->Session disconnected (\( session.sessionId))")
    }

    func session(_ session: OTSession, receivedSignalType type: String, from connection: OTConnection, with string: String) {
    
        //print("Signal session received: session id = %@, type = %@, string = %@", session.sessionId, type, string)
    
        if type == "global_msg_client_change" {

            print("Recevied global_msg -> global_msg_client_change" + " user ID " + string)
            
            return
        }

        if type == "global_msg_client_mute" {
            
            print("Recevied global_msg -> global_msg_client_mute" + " user ID " + string)

            let dic = ["uID":  string]

            NotificationCenter.default.post(name: Notification.Name(rawValue: incomingClientMuteNotificationKey), object: self, userInfo: dic)

            return
        }
        
        if type == "global_msg_client_unmute" {
            
            print("Recevied global_msg -> global_msg_client_unmute" + " user ID " + string)
            
            let dic = ["uID":  string]
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: incomingClientUnMuteNotificationKey), object: self, userInfo: dic)
            
            return
        }
        
        if type == "global_msg_client_disconnect" {
            
            print("Recevied global_msg -> global_msg_client_disconnect" + " user ID " + string)
            
            let dic = ["uID":  string]
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: presenterDisconnectUserGroupCallNotificationKey), object: self, userInfo: dic)
            
            return
        }

        if type == "global_msg_client_switch_video_to_user" {
            
            print("Recevied global_msg -> global_msg_client_switch_video_to_user" + " user ID " + string)
            
            let dic = ["uID":  string]
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: presenterSwitchVideoToUserGroupCallNotificationKey), object: self, userInfo: dic)
            
            return
        }
        
        if type == "global_msg_video_call_request" {
            
            let loginUid = userList.getLoginUserID()
            if loginUid.isEmpty {
                
                return
            }

            //print("video JSON -> ", string)

            let videoJSON = convertStringToDictionary(string)! as Dictionary<String,AnyObject>

            let users = videoJSON["users"] as? Array<String>

            if users == nil {

                return;
            }

            for uID: String in users! {
                
                if uID == userList.getLoginUserID() {
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: incomingVideoGroupCallNotificationKey), object: self, userInfo: videoJSON)
                }
            }
            
            return
        }
        
        if type == "global_msg_presenter_call_request" {
            
            let loginUid = userList.getLoginUserID()
            if loginUid.isEmpty {
                
                return
            }
            
            let presenterJSON = convertStringToDictionary(string)! as Dictionary<String,AnyObject>
            
            let users = presenterJSON["users"] as? Array<String>

            if users == nil {
                
                return;
            }

            for uID: String in users! {
                
                if uID == userList.getLoginUserID() {
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: incomingPresenterGroupCallNotificationKey), object: self, userInfo: presenterJSON)
                }
            }
            
            return
        }
        
        if type == "global_msg_audio_call_request" {
            
            let loginUid = userList.getLoginUserID()
            if loginUid.isEmpty {
                
                return
            }
            
            let audioJSON = convertStringToDictionary(string)! as Dictionary<String,AnyObject>
            
            let users = audioJSON["users"] as? Array<String>

            if users == nil {
                
                return;
            }

            for uID: String in users! {
                
                if uID == userList.getLoginUserID() {
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: incomingAudioGroupCallNotificationKey), object: self, userInfo: audioJSON)
                }
            }
            
            return
        }
        
        if type == "global_msg_screenshare_notification" {
            
            let loginUid = userList.getLoginUserID()
            if loginUid.isEmpty {
                
                return
            }
            
            let shareJSON = convertStringToDictionary(string)! as Dictionary<String,AnyObject>
            
            let users = shareJSON["users"] as! Array<String>
            
            for uID: String in users {
                
                if uID == userList.getLoginUserID() {
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: incomingScreenShareNotificationKey), object: self, userInfo: shareJSON)
                }
            }
            
            return
        }
        
        if type == "global_msg_client_chat" {
            
            let loginUid = userList.getLoginUserID()
            if loginUid.isEmpty {
                
                return
            }
            
            if currentGroupCallID.isEmpty {
            
                return
            }

            let chatJSON = convertStringToDictionary(string)! as Dictionary<String,AnyObject>

            let gID = chatJSON["groupCallID"] as! String
            
            if gID == currentGroupCallID {
                
                chatManager.addNewChat(gID, uID: chatJSON["uID"] as! String, text: chatJSON["text"] as! String, question: false)
                NotificationCenter.default.post(name: Notification.Name(rawValue: incomingGroupChatNotificationKey), object: self, userInfo: nil)
            }

            return
        }

        if type == "global_msg_client_question" {
            
            let loginUid = userList.getLoginUserID()
            if loginUid.isEmpty {
                
                return
            }
            
            if currentGroupCallID.isEmpty {
                
                return
            }
            
            let questionJSON = convertStringToDictionary(string)! as Dictionary<String,AnyObject>
            
            let gID = questionJSON["groupCallID"] as! String
            
            if gID == currentGroupCallID {
                
                chatManager.addNewChat(gID, uID: questionJSON["uID"] as! String, text: questionJSON["question_text"] as! String, question: true)
                NotificationCenter.default.post(name: Notification.Name(rawValue: incomingGroupQuestionNotificationKey), object: self, userInfo: questionJSON)
            }
            
            return
        }

        if type == "global_msg_client_leader_question" {
            
            let loginUid = userList.getLoginUserID()
            if loginUid.isEmpty {
                
                return
            }
            
            if currentGroupCallID.isEmpty {
                
                return
            }

            let questionJSON = convertStringToDictionary(string)! as Dictionary<String,AnyObject>

            let leaderUid = questionJSON["leader"] as! String
            
            if loginUid == leaderUid {
                
                let gID = questionJSON["groupCallID"] as! String
                
                if gID == currentGroupCallID {
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: incomingGroupLeaderQuestionNotificationKey), object: self, userInfo: questionJSON)
                }
            }

            return
        }

        if type == "global_msg_client_call_reject" {
            
            let loginUid = userList.getLoginUserID()
            if loginUid.isEmpty {
            
                return
            }

            let rejectJSON = convertStringToDictionary(string)! as Dictionary<String,AnyObject>
            
            let gID = rejectJSON["groupCallID"] as! String

            if (gID == currentGroupCallID) && (currentGroupCallLeadUiD == loginUid) {
            
                NotificationCenter.default.post(name: Notification.Name(rawValue: incomingGroupRejectNotificationKey), object: self, userInfo: rejectJSON)
            }
            
            return
        }

        if type == "global_msg_client_state_ping" {
            
            let loginUid = userList.getLoginUserID()
            if loginUid.isEmpty {
                
                return
            }
            
            let pingJSON = convertStringToDictionary(string)! as Dictionary<String,AnyObject>
            
            let uID = pingJSON["uID"] as! String

            if uID != loginUid {
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: incomingUserStatusPingNotificationKey), object: self, userInfo: pingJSON)
            }
            
            return
        }
    }

    func session(_ session: OTSession, streamCreated stream: OTStream) {
        
        print("GLOBAL->session streamCreated (\(stream.streamId))")
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        
        print("GLOBAL->session streamCreated (\(stream.streamId))")
    }
    
    func session(_ session: OTSession, connectionCreated connection : OTConnection) {
        
        print("GLOBAL->session connectionCreated (\(connection.connectionId))")
    }
    
    func session(_ session: OTSession, connectionDestroyed connection : OTConnection) {
        
        print("GLOBAL->session connectionDestroyed (\(connection.connectionId))")
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        
        print("GLOBAL->session didFailWithError :", error)
    }

    func sessionDidBeginReconnecting(_ session: OTSession) {
        
        print("GLOBAL->sessionDidBeginReconnecting (trying to reconnect to global signal session)***")
    }

}

