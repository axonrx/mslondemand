//
//  Chat.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/28/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation

class Chat {
    
    var gID: String             // Group Call ID
    var uID: String             // User ID of chat text
    var userThumbnail : String  // User thumbnail URL
    var userName : String       // User user's name
    var text : String           // Chat Text
    var isQuestion : Bool       // Is Question Text
    
    init(gID: String, uID: String, userName: String, userThumbnail: String, text: String, question: Bool) {
        
        self.gID = gID
        self.uID = uID
        self.userThumbnail = userThumbnail
        self.text = text
        self.userName = userName
        self.isQuestion = question
    }
}
