//
//  PresenterConnection.swift
//  MLS Connect
//
//  Created by Chris Mutkoski on 5/2/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation
import Alamofire


class PresenterConnection {
    
    var groupCallSessionID: String?
    var groupCallToken: String?
    
    func getPresenterGroupCallToken(_ groupCallID: String, name: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token":  MSLConnect_Constants.apiToken,
            "command": "GETPRESENTERGROUPCALLTOKEN",
            "groupCallID": groupCallID,
            "groupCallName": name
        ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        
                        let result = response.object(forKey: "result") as! NSDictionary
                        
                        self.groupCallSessionID = result["groupcall_session_id"] as? String
                        self.groupCallToken = result["token"] as? String
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }
    
}
