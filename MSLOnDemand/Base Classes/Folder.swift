//
//  Folder.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/18/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation


class Folder {
    
    var fID: String                  // Folder ID
    var dIDs: Array<String>          // Document ID's
    var name: String                 // Folder name
    var uID: String                  // User ID
    var date_created : String        // Date folder created
    
    init(fID: String, dIDs: Array<String>, name: String, uID: String, date_created: String) {
        
        self.fID = fID
        self.dIDs = dIDs
        self.name = name
        self.uID = uID
        self.date_created = date_created
    }
}
