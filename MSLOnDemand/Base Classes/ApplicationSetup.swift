//
//  ApplicationSetup.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/8/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation
import Alamofire


class ApplicationSetup {

    var appID: String?
    var appName: String?
    var appLogoUrl: String?
    var backgroundUrl: String?
    var gradientStart: String?
    var gradientEnd: String?
    var setupNetworkingComplete = false

    // MARK: - Application Setup Network Calls
    
    func getApplicationSetup(_ appID: String, completion: @escaping (_ result: String, _ code: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "GETAPPSETUP",
            "appID": appID,
        ]
        
        self.appID = appID

        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        //print(jsonParams)

        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {

                    //print(response.result.value!)
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        
                        let result = response.object(forKey: "result") as! NSDictionary
                        
                        self.appName = result.object(forKey: "app_name") as? String
                        self.appLogoUrl = result.object(forKey: "logo_url") as? String
                        self.backgroundUrl = result.object(forKey: "background_url") as? String
                        self.gradientStart = result.object(forKey: "gradient_start") as? String
                        self.gradientEnd = result.object(forKey: "gradient_end") as? String
                        
                        self.setupNetworkingComplete = true
                        
                        completion("success", "")
                    }
                    else {
                        
                        let response = JSON
                        let errorMessage = response.object(forKey: "code") as! String
                        completion("failed", errorMessage)
                    }
                    
                }
                else {
                    
                    completion("failed", "Error logging in. Please make sure you have an internet connection.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed", response.result.error!.localizedDescription)
                break
                
            }
        }
    }

    func getApplicationID() -> String {
        
        return self.appID!
    }

    func getApplicationName() -> String {
        
        return self.appName!
    }

    func getApplicationLogoUrl() -> String? {
        
        return self.appLogoUrl
    }

    func getApplicationBackgroundUrl() -> String? {
        
        return self.backgroundUrl
    }

    func getApplicationGradientStart() -> String {
        
        return self.gradientStart!
    }

    func getApplicationGradientEnd() -> String {
        
        return self.gradientEnd!
    }

    func getApplicationSetupNetworkComplete() -> Bool {
        
        return setupNetworkingComplete
    }

}
