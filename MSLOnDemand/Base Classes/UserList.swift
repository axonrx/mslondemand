//
//  UserList.swift
//  MLS Connect
//
//  Created by Chris Mutkoski on 4/21/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation
import Alamofire


class UserList {
    
    var userList: [User] = [User]()   // Array of CallGroups -> history of previous group calls
    var userLoggedIn: User?           // User Object of logged in User
    var uID: String = String()        // User ID user using MLS Connect

    init() {

        //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UserList.handleUserStatusPingUpdate), name: incomingUserStatusPingNotificationKey, object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: incomingUserStatusPingNotificationKey), object: nil, queue: nil,using: handleUserStatusPingUpdate)
    }

    // MARK: - User Network Calls

    func getAllUsersWhenLogin(_ username: String, password: String, completion: @escaping (_ result: String, _ code: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "login",
            "username": username,
            "password": password,
            "appID": 666
        ] as [String : Any]

        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]

        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        
                        let result = response.object(forKey: "result") as! NSDictionary
                        self.uID = result.object(forKey: "loggedUserID") as! String
                        
                        // NOTE(tsd): Have server return total number users at "result" level in JSON
                        
                        var curUser = 0
                        let seeUsersReturned = result.object(forKey: String(curUser))
                        if seeUsersReturned == nil {
                            
                            completion("success", "")
                            return
                        }
                        var user = result.object(forKey: String(curUser)) as! NSDictionary
                        while (result.object(forKey: String(curUser)) != nil) {
                            
                            user = result.object(forKey: String(curUser)) as! NSDictionary
                            let newUser = User(uID: user.object(forKey: "uID") as! String,
                                               firstName: user.object(forKey: "firstName") as! String,
                                               lastName: user.object(forKey: "lastName") as! String,
                                               credentials: user.object(forKey: "credentials") as! String,
                                               jobTitle: user.object(forKey: "jobTitle") as! String,
                                               picPath: user.object(forKey: "picPath") as! String,
                                               lastSeen: 0,
                                               available: false,
                                               phoneNumber: user.object(forKey: "phoneNumber") as! String,
                                               email: user.object(forKey: "email") as! String,
                                               cell: user.object(forKey: "cell") as! String,
                                               fax: user.object(forKey: "fax") as! String,
                                               location: user.object(forKey: "location") as! String,
                                               techniques: user.object(forKey: "techniques") as! String,
                                               focus: user.object(forKey: "focus") as! String,
                                               programs: user.object(forKey: "programs") as! String,
                                               experience: user.object(forKey: "experience") as! String,
                                               education: user.object(forKey: "education") as! String,
                                               callState: UserCallState.none)
                            
                            if user.object(forKey: "uID") as! String != self.getLoginUserID() {
                                
                                self.userList.append(newUser)
                            }
                            else {
                                
                                self.userLoggedIn = newUser
                            }
                            curUser = curUser + 1
                        }
                        
                        completion("success", "")
                    }
                    else {
                        
                        let response = JSON
                        let errorMessage = response.object(forKey: "code") as! String
                        completion("failed", errorMessage)
                    }
                    
                }
                else {
                    
                    completion("failed", "Error parsing LOGIN response JSON")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed", response.result.error!.localizedDescription)
                break
                
            }
        }
    }

    func getUpdatedUsersList(_ completion: @escaping (_ result: String, _ code: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "uID": getLoginUserID(),
            "command": "GETUSERLIST"
        ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]

        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    //print(response.result.value!)
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        
                        let result = response.object(forKey: "result") as! NSDictionary
                        self.uID = result.object(forKey: "loggedUserID") as! String
                        
                        // NOTE(tsd): Have server return total number users at "result" level in JSON
                        
                        self.userList.removeAll()
                        
                        var curUser = 0
                        let seeUsersReturned = result.object(forKey: String(curUser))
                        if seeUsersReturned == nil {
                            
                            return
                        }
                        var user = result.object(forKey: String(curUser)) as! NSDictionary
                        while (result.object(forKey: String(curUser)) != nil) {
                            
                            user = result.object(forKey: String(curUser)) as! NSDictionary
                            let newUser = User(uID: user.object(forKey: "uID") as! String,
                                               firstName: user.object(forKey: "firstName") as! String,
                                               lastName: user.object(forKey: "lastName") as! String,
                                               credentials: user.object(forKey: "credentials") as! String,
                                               jobTitle: user.object(forKey: "jobTitle") as! String,
                                               picPath: user.object(forKey: "picPath") as! String,
                                               lastSeen: 0,
                                               available: false,
                                               phoneNumber: user.object(forKey: "phoneNumber") as! String,
                                               email: user.object(forKey: "email") as! String,
                                               cell: user.object(forKey: "cell") as! String,
                                               fax: user.object(forKey: "fax") as! String,
                                               location: user.object(forKey: "location") as! String,
                                               techniques: user.object(forKey: "techniques") as! String,
                                               focus: user.object(forKey: "focus") as! String,
                                               programs: user.object(forKey: "programs") as! String,
                                               experience: user.object(forKey: "experience") as! String,
                                               education: user.object(forKey: "education") as! String,
                                               callState: UserCallState.none)
                            
                            if user.object(forKey: "uID") as! String != self.getLoginUserID() {
                                
                                self.userList.append(newUser)
                            }
                            else {
                                
                                self.userLoggedIn = newUser
                            }
                            curUser = curUser + 1
                        }
                        
                        completion("success", "")
                    }
                    else {
                        
                        let response = JSON
                        let errorMessage = response.object(forKey: "code") as! String
                        completion("failed", errorMessage)
                    }
                }
                else {
                    
                    completion("failed", "Error parsing GETUSERLIST response JSON")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed", response.result.error!.localizedDescription)
                break
                
            }
        }
    }

    func updateUser(_ user: User, completion: @escaping (_ result: String, _ code: String) -> Void) {

        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "UPDATEUSER",
            "userData": [
                "uID": getLoginUserID(),
                "firstName": user.firstName,
                "lastName": user.lastName,
                "credentials": user.credentials,
                "jobTitle": user.jobTitle,
                "phoneNumber": user.phoneNumber,
                "cell": user.cell,
                "fax": user.fax,
                "location": user.location,
                "techniques": user.techniques,
                "focus": user.focus,
                "programs": user.programs,
                "experience": user.experience,
                "education": user.education
            ]
        ] as [String : Any]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]

        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    //print(response.result.value!)
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        completion("success", "")
                    }
                    else {
                        
                        let response = JSON
                        let errorMessage = response.object(forKey: "code") as! String
                        completion("failed", errorMessage)
                    }
                }
                else {
                    
                    completion("failed", "Error parsing UPDATEUSER response JSON")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed", response.result.error!.localizedDescription)
                break
                
            }
        }
    }

    // MARK: - User Information
    
    func getUserList() -> [User] {

        return self.userList
    }

    func getLoginUserID() -> String {
        
        let loggedIn = UserDefaults.standard.bool(forKey: "userLoggedIn")
        
        if loggedIn == true && self.uID.isEmpty {

            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            return userId
        }
        else {
            
            return self.uID
        }
    }

    func getUserThumbnailPath(_ uID: String) -> String {
        
        if userLoggedIn?.uID == uID {
            
            return userLoggedIn!.picPath
        }

        for user: User in self.userList {
            
            if user.uID == uID {
              
                return user.picPath
            }
        }
        
        return String()
    }

    func getUserName(_ uID: String) -> String {
        
        if uID == getLoginUserID() {
            
            return userLoggedIn!.jobTitle + " " + userLoggedIn!.firstName + " " + userLoggedIn!.lastName + ", " + userLoggedIn!.credentials
        }

        for user: User in self.userList {
            
            if user.uID == uID {
                
                return user.jobTitle + " " + user.firstName + " " + user.lastName + ", " + user.credentials
            }
        }
        
        return String()
    }

    func getUser(_ uID: String) -> User? {

        if uID == getLoginUserID() {
            
            return userLoggedIn
        }

        for user: User in self.userList {
            
            if user.uID == uID {
                
                return user
            }
        }
        
        return nil
    }

    func getUserCallState(_ uID: String) -> UserCallState? {
        
        if uID == getLoginUserID() {
            
            return userLoggedIn?.callState
        }

        for user: User in self.userList {
            
            if user.uID == uID {
                
                return user.callState
            }
        }
        
        return UserCallState.none
    }
    
    func setUsersCallState(_ uID: String, callState: UserCallState) {
        
        if uID == getLoginUserID() {

            userLoggedIn?.callState = callState
        }

        for user: User in self.userList {
            
            if user.uID == uID {
                
                user.callState = callState
            }
        }
    }

    func setUserStatus(_ status: UserOnlineStatus) {
        
        userLoggedIn?.userStatus = status
    }

    func setUserMuteState(_ uID: String, mute: Bool) {
        
        if uID == getLoginUserID() {
            
            userLoggedIn?.isMuted = mute
        }
        
        for user: User in self.userList {
            
            if user.uID == uID {
                
                user.isMuted = mute
            }
        }
    }

    func resetUsersMuteState() {
        
        userLoggedIn?.isMuted = false
        
        for user: User in self.userList {
            
            user.isMuted = false
        }
    }

    // MARK: - User Sorting Methods

    func sortUsersAscendingLastName() {
        
        userList.sort { $0.lastName.compare($1.lastName) == .orderedAscending }
    }

    func sortUsersDescendingOnlineStatus() {
        
        userList.sort { $0.userStatus.rawValue > $1.userStatus.rawValue }
    }

    func sortUsersDescendingLastSeen() {
        
        userList.sort { $0.lastSeen > $1.lastSeen }
    }

    func sortUsersDescendingFocus() {
        
        userList.sort { $0.focus > $1.focus }
    }

    // MARK: - User Tag Support Methods
    
    func findTagInUserProgram(_ tag: String) -> [User] {

        let tagArray = getUserList()
        let tagResults = tagArray.filter { $0.programs.lowercased().range(of: (tag.lowercased())) != nil }

        return tagResults
    }

    // MARK: - User Status Update Timer
    var timerStatus: DispatchSourceTimer!
    var timerTimeout: DispatchSourceTimer!

    func startSendingUserStatusTimer() {
        
        let queue = DispatchQueue(label: "com.axonrx.mslondemand.userStatusSendStateTimer", attributes: .concurrent)
        
        timerStatus?.cancel()
        
        timerStatus = DispatchSource.makeTimerSource(queue: queue)
        
        timerStatus?.scheduleRepeating(deadline: .now(), interval: .seconds(3), leeway: .seconds(1))
        
        timerStatus?.setEventHandler { [weak self] in // `[weak self]` only needed if you reference `self` in this closure and you want to prevent strong reference cycle

            if let loginUserInfo = self?.userLoggedIn {
                
                globalSignal.sendGlobalSignalUserStatusPing(loginUserInfo.userStatus)
            }
        }
        
        timerStatus?.resume()
    }
    
    func stopSendingUserStatusTimer() {

        timerStatus?.cancel()
        timerStatus = nil
    }

    func startUserStatusTimeoutTimer() {
        
        let queue = DispatchQueue(label: "com.axonrx.mslondemand.userStatusSendStateTimer", attributes: .concurrent)
        
        timerTimeout?.cancel()
        
        timerTimeout = DispatchSource.makeTimerSource(queue: queue)
        
        timerTimeout?.scheduleRepeating(deadline: .now(), interval: .seconds(3), leeway: .seconds(1))
        
        timerTimeout?.setEventHandler { [weak self] in // `[weak self]` only needed if you reference `self` in this closure and you want to prevent strong reference cycle
            
            var changes = false
            for user: User in (self?.userList)! {
                
                if (user.lastStatusPing - Date().timeIntervalSince1970 < 0) && (user.userStatus != .offline) {
                    
                    user.userStatus = .offline
                    changes = true
                }
            }
            
            if changes == true {
                
                DispatchQueue.main.async {
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: updateUserStatusNotificationKey), object: self)
                }
            }
        }
        
        timerTimeout?.resume()
    }
    
    func stopUserStatusTimeoutTimer() {
        
        timerTimeout?.cancel()
        timerTimeout = nil
    }

/*
    // Swift 2 implementation
    var timerStatus: DispatchSource!
    var timerTimeout: DispatchSource!
    
    // every 2 seconds, with leeway of 1 second
    func startSendingUserStatusTimer() {

        let queue = DispatchQueue(label: "com.axonrx.mslondemand.userStatusSendStateTimer", attributes: [])
        timerStatus = DispatchSource.makeTimerSource(flags: DispatchSource.TimerFlags(rawValue: 0), queue: queue) /*Migrator FIXME: Use DispatchSourceTimer to avoid the cast*/ as! DispatchSource
        timerStatus.setTimer(start: DispatchTime.now(), interval: 3 * NSEC_PER_SEC, leeway: 1 * NSEC_PER_SEC)
        timerStatus.setEventHandler {

            if let loginUserInfo = self.userLoggedIn {
                
                globalSignal.sendGlobalSignalUserStatusPing(loginUserInfo.userStatus)
            }
        }

        timerStatus.resume()
    }
    
    func stopSendingUserStatusTimer() {
        
        timerStatus.cancel()
        timerStatus = nil
    }

    // every 2 seconds, with leeway of 1 second
    func startUserStatusTimeoutTimer() {
        
        let queue = DispatchQueue(label: "com.axonrx.mslondemand.userStatusTimeoutTimer", attributes: [])
        timerTimeout = DispatchSource.makeTimerSource(flags: DispatchSource.TimerFlags(rawValue: 0), queue: queue) /*Migrator FIXME: Use DispatchSourceTimer to avoid the cast*/ as! DispatchSource
        timerTimeout.setTimer(start: DispatchTime.now(), interval: 3 * NSEC_PER_SEC, leeway: 1 * NSEC_PER_SEC)
        timerTimeout.setEventHandler {
            
            var changes = false
            for user: User in self.userList {

                if (user.lastStatusPing - Date().timeIntervalSince1970 < 0) && (user.userStatus != .offline) {

                    user.userStatus = .offline
                    changes = true
                }
            }
            
            if changes == true {
                
                DispatchQueue.main.async {

                    NotificationCenter.default.post(name: Notification.Name(rawValue: updateUserStatusNotificationKey), object: self)
                }
            }
        }
        
        timerTimeout.resume()
    }
    
    func stopUserStatusTimeoutTimer() {
        
        timerTimeout.cancel()
        timerTimeout = nil
    }
*/
    func handleUserStatusPingUpdate(_ notification: Notification) {
        
        let userPingStatus = notification.userInfo as! Dictionary<String,AnyObject>
        let userId = userPingStatus["uID"] as! String
        
        let userStatus = userPingStatus["status"] as! NSNumber
        let statusUser = UserOnlineStatus(rawValue: userStatus.intValue)

        //print("handleUserStatusPingUpdate for ", userId, "user status = ", userStatus)

        for user: User in self.userList {
            
            if user.uID == userId {
                
                user.userStatus = statusUser!
                user.lastStatusPing = Date().timeIntervalSince1970 + userStatusTimeout
                DispatchQueue.main.async {
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: updateUserStatusNotificationKey), object: self)
                }
            }
        }
    }
}
