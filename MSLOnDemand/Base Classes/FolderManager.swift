//
//  FolderManager.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/18/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD


class FolderManager {
    
    var folderList: [Folder] = [Folder]()   // Array of all folders and documents they contain

    func getFolders(_ completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "GETUSERFOLDERS",
            "uID": userList.getLoginUserID(),
            ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        let result = response.object(forKey: "result") as! NSDictionary
                        //let totalFolders = result.objectForKey("total") as! Int
                        //print("TotalFolders = ", totalFolders)
                        let folders = result.object(forKey: "folders")
                        
                        self.folderList.removeAll()
                        
                        for folder in folders as! [Dictionary<String, AnyObject>] {
                            
                            var docIDs = Array<String>()
                            docIDs = folder["documentIDs"] as! Array<String>
                            
                            let newFolder = Folder(fID: folder["fID"] as! String,
                                                   dIDs: docIDs,
                                                   name: folder["name"] as! String,
                                                   uID: folder["uID"] as! String,
                                                   date_created: folder["date_created"] as! String)
                            
                            self.folderList.append(newFolder)
                        }
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }
    
    func updateFolder(_ fID: String, folderName: String, documentIDs: Array<String>, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "UPDATEFOLDER",
            "fID": fID,
            "uID": userList.getLoginUserID(),
            "documentIDs": documentIDs,
            "folderName": folderName
        ] as [String : Any]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let folders = JSON["result"] as! NSArray
                        
                        self.folderList.removeAll()
                        
                        for folder in folders {
                            
                            var docIDs = Array<String>()
                            docIDs = (folder as AnyObject).object(forKey: "documentIDs") as! Array<String>
                            
                            let newFolder = Folder(fID: (folder as AnyObject).object(forKey: "fID") as! String,
                                                   dIDs: docIDs,
                                                   name: (folder as AnyObject).object(forKey: "name") as! String,
                                                   uID: (folder as AnyObject).object(forKey: "uID") as! String,
                                                   date_created: (folder as AnyObject).object(forKey: "date_created") as! String)
                            
                            self.folderList.append(newFolder)
                        }
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func newFolder(_ uID: String, folderName: String, documentIDs: Array<String>, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "NEWFOLDER",
            "uID": uID,
            "documentIDs": documentIDs,
            "folderName": folderName
        ] as [String : Any]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let folders = JSON["result"] as! NSArray
                        
                        self.folderList.removeAll()
                        
                        for folder in folders {
                            
                            var docIDs = Array<String>()
                            docIDs = (folder as AnyObject).object(forKey: "documentIDs") as! Array<String>
                            
                            let newFolder = Folder(fID: (folder as AnyObject).object(forKey: "fID") as! String,
                                                   dIDs: docIDs,
                                                   name: (folder as AnyObject).object(forKey: "name") as! String,
                                                   uID: (folder as AnyObject).object(forKey: "uID") as! String,
                                                   date_created: (folder as AnyObject).object(forKey: "date_created") as! String)
                            
                            self.folderList.append(newFolder)
                        }
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func deleteFolder(_ fID: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "DELETEFOLDER",
            "uID": userList.getLoginUserID(),
            "fID": fID
        ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let folders = JSON["result"] as! NSArray
                        
                        self.folderList.removeAll()
                        
                        for folder in folders {
                            
                            var docIDs = Array<String>()
                            docIDs = (folder as AnyObject).object(forKey: "documentIDs") as! Array<String>
                            
                            let newFolder = Folder(fID: (folder as AnyObject).object(forKey: "fID") as! String,
                                                   dIDs: docIDs,
                                                   name: (folder as AnyObject).object(forKey: "name") as! String,
                                                   uID: (folder as AnyObject).object(forKey: "uID") as! String,
                                                   date_created: (folder as AnyObject).object(forKey: "date_created") as! String)
                            
                            self.folderList.append(newFolder)
                        }
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }
    
    func updateFoldersDocumentDeleted(_ dID: String) {
        
        for folder: Folder in self.folderList {

            var indx = 0
            for fDocumentID: String in folder.dIDs {
                
                if fDocumentID == dID {
                    
                    folder.dIDs.remove(at: indx)
                    updateFolder(folder.fID, folderName: folder.name, documentIDs: folder.dIDs, completion: folderFinishedUpdating)
                    break
                }

                indx += 1
            }
        }
    }

    func removeDocumentFromFolder(_ fID: String, dID: String) {
        
        for folder: Folder in self.folderList {
            
            if folder.fID == fID {
                
                var indx = 0
                for fDocumentID: String in folder.dIDs {
                    
                    if fDocumentID == dID {
                        
                        folder.dIDs.remove(at: indx)
                        updateFolder(folder.fID, folderName: folder.name, documentIDs: folder.dIDs, completion: folderFinishedUpdating)
                        break
                    }
                    
                    indx += 1
                }

                break
            }
        }
    }

    func getFolderByID(_ fID: String) -> Folder? {
        
        for folder: Folder in self.folderList {
            
            if folder.fID == fID {
                
                return folder
            }
        }
        
        return nil
    }

    // MARK: - Completion Methods

    func folderFinishedUpdating(_ result: String!) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: updateFolderNotificationKey), object: self)
    }

}
