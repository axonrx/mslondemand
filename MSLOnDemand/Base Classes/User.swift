//
//  User.swift
//  MLS Connect
//
//  Created by Chris Mutkoski on 4/21/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation


class User {

    var uID: String                     // User ID
    var firstName: String               // User first name
    var lastName: String                // User last name
    var credentials: String             // User credentials
    var jobTitle: String                // User job title
    var picPath: String                 // User picture path
    var lastSeen: Double                // User last seen
    var userStatus: UserOnlineStatus    // Availablity status of user
    var lastStatusPing: Double          // User last seen
    var phoneNumber: String             // User Phone Number
    var email: String                   // User Email Address
    var cell: String                    // User Cell Number
    var fax: String                     // User Fax Number
    var location: String                // User Location
    var techniques: String              // User Techniques
    var focus: String                   // User Focus
    var programs: String                // User Programs
    var experience: String              // User Experience
    var education: String               // User Education
    var callState: UserCallState        // User state on group call
    var isMuted: Bool                   // User mute state on group call
    
    init(uID: String, firstName: String, lastName: String, credentials: String, jobTitle: String, picPath: String, lastSeen: Double, available: Bool, phoneNumber: String, email: String, cell: String, fax: String, location: String, techniques: String, focus: String, programs: String, experience: String, education: String, callState: UserCallState) {

        self.uID = uID
        self.firstName = firstName
        self.lastName = lastName
        self.credentials = credentials
        self.jobTitle = jobTitle
        self.picPath = picPath
        self.lastSeen = lastSeen
        self.phoneNumber = phoneNumber
        self.email = email
        self.cell = cell
        self.fax = fax
        self.location = location
        self.techniques = techniques
        self.focus = focus
        self.programs = programs
        self.experience = experience
        self.education = education
        self.callState = callState
        self.isMuted = false
        self.userStatus = .offline
        self.lastStatusPing = Date().timeIntervalSince1970 + userStatusTimeout
    }
}
