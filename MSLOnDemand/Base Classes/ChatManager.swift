//
//  ChatManager.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/28/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation


class ChatManager {
    
    var chatList: [Chat] = [Chat]()   // Array of all chats for a group call

    func addNewChat(_ gID: String, uID: String, text: String, question: Bool) {

        let thumbnailPath = userList.getUserThumbnailPath(uID)
        let userName = userList.getUserName(uID)
        self.chatList.append(Chat(gID: gID, uID: uID, userName: userName, userThumbnail:thumbnailPath, text: text, question: question))
    }

}
