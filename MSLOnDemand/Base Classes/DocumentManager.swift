//
//  DocumentManager.swift
//  MLS Connect
//
//  Created by Chris Mutkoski on 5/18/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD


class DocumentManager {

    var documentList: [Document] = [Document]()   // Array of all users documents

    func uploadFileToAxonServer(_ uID: String, fileName: String, notes: String, fileUrl: URL, hud: MBProgressHUD, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "SAVEDOCUMENT",
            "name": fileName,
            "notes": notes,
            "uID": uID,
            ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        if !FileManager.default.fileExists(atPath: fileUrl.path) {
            
            print("file not found")
            completion("failed: file not found")
            return
        }

        let fileData = try? Data(contentsOf: fileUrl)
        let fileName = fileUrl.lastPathComponent
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(fileData!, withName: "document", fileName: fileName, mimeType: "document")
            
            for (key, value) in jsonParams {
                
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to: MSLConnect_Constants.baseURL)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress { progress in
                    
                    DispatchQueue.main.async {
                        
                        hud.detailsLabel.text = "\(progress.fractionCompleted)/"
                    }
                    print(progress.fractionCompleted)
                }
                
                upload.responseJSON { response in
                    print(response.result)
                }
                
            case .failure(let encodingError):
                print(encodingError.localizedDescription)
            }
        }
    }

    func getUserDocuments(_ uID: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "GETDOCUMENTS",
            "uID": uID,
            ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let documents = JSON["result"] as! NSArray
                        
                        self.processDocuments(documents: documents as! Array<Dictionary<String, String>>)
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func updateDocumentNotes(_ dID: String, notes: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "UPDATEDOCUMENTNOTES",
            "dID": dID,
            "notes": notes,
            "uID": userList.getLoginUserID()
            ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]

        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let documents = JSON["result"] as! NSArray
                        
                        self.processDocuments(documents: documents as! Array<Dictionary<String, String>>)
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func deleteDocument(_ dID: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "DELETEDOCUMENT",
            "dID": dID,
            "uID": userList.getLoginUserID(),
            ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let documents = JSON["result"] as! NSArray
                        
                        self.processDocuments(documents: documents as! Array<Dictionary<String, String>>)
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }
    
    // MARK: - Public methods

    func getDocumentByID(_ dID: String) -> Document? {

        for document: Document in self.documentList {
            
            if document.dID == dID {
                
                return document
            }
        }
        
        return nil
    }

    func processDocuments(documents: Array<Dictionary<String, String>>) {

        self.documentList.removeAll()
        
        for document in documents {
            
            var date_uploaded = (document as AnyObject).object(forKey: "date_uploaded")
            if date_uploaded == nil {
                
                date_uploaded = ""
            }
            
            let newDocument = Document(dID: (document as AnyObject).object(forKey: "dID") as! String,
                                       name: (document as AnyObject).object(forKey: "name") as! String,
                                       uID: (document as AnyObject).object(forKey: "uID") as! String,
                                       path: (document as AnyObject).object(forKey: "path") as! String,
                                       visible: (document as AnyObject).object(forKey: "visible") as! String,
                                       type: (document as AnyObject).object(forKey: "type") as! String,
                                       notes: (document as AnyObject).object(forKey: "notes") as! String,
                                       date_uploaded: date_uploaded as! String)
            
            self.documentList.append(newDocument)
        }
    }

}
