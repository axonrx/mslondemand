//
//  GroupTagManager.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 8/9/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD

//case "NEWGROUP" 			: $result = newGroupTag($call->uID, $call->groupText); break;
//case "DELETEGROUP" 			: $result = deleteGroupTag($call->uID, $call->gID); break;

class GroupTagManager {
    
    var groupTagList: [GroupTag] = [GroupTag]()   // Array of all group tags for a user
    
    func getUserGroupTags(_ completion: @escaping (_ result: String) -> Void) {

        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "GETUSERGROUPS",
            "uID": userList.getLoginUserID(),
            ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]

        // NEEDS TO BE MOVE TO SWIFT3 AND ALAMOFIRE 4.0
        Alamofire.request(MSLConnect_Constants.baseURL, method: .post, parameters: jsonParams, encoding: JSONEncoding.default)
            .responseJSON { response in
                //print(response)
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        let result = response.object(forKey: "result") as! NSDictionary
                        //let totalFolders = result.objectForKey("total") as! Int
                        //print("TotalFolders = ", totalFolders)
                        let groupTags = result.object(forKey: "groupTags")
                        
                        self.groupTagList.removeAll()
                        
                        for groupTag in groupTags as! [Dictionary<String, AnyObject>] {

                            let newGroupTag = GroupTag(gID: groupTag["gID"] as! String,
                                text: groupTag["text"] as! String)
                            
                            self.groupTagList.append(newGroupTag)
                        }
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
        }
    }

    
    func newGroupTag(_ text: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "NEWGROUPTAG",
            "uID": userList.getLoginUserID(),
            "groupTagText": text
        ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .post, parameters: jsonParams, encoding: JSONEncoding.default)
            .responseJSON { response in
                //print(response)
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let groupTags = JSON["result"] as! NSArray
                        
                        self.groupTagList.removeAll()
                        
                        for groupTag in groupTags as! [Dictionary<String, AnyObject>] {
                            
                            let newGroupTag = GroupTag(gID: groupTag["gID"] as! String,
                                text: groupTag["text"] as! String)
                            
                            self.groupTagList.append(newGroupTag)
                        }
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
        }
    }
    
    func deleteGroupTag(_ gID: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "DELETEGROUPTAG",
            "uID": userList.getLoginUserID(),
            "gID": gID
        ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .post, parameters: jsonParams, encoding: JSONEncoding.default)
            .responseJSON { response in
                //print(response)
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let groupTags = JSON["result"] as! NSArray
                        
                        self.groupTagList.removeAll()
                        
                        for groupTag in groupTags as! [Dictionary<String, AnyObject>] {
                            
                            let newGroupTag = GroupTag(gID: groupTag["gID"] as! String,
                                text: groupTag["text"] as! String)
                            
                            self.groupTagList.append(newGroupTag)
                        }
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
        }
    }

    // MARK: - Class methods

    func getGroupTagByText(_ text: String) -> GroupTag? {
        
        for tag: GroupTag in self.groupTagList {
            
            if tag.text == text {
                
                return tag
            }
        }
        
        return nil
    }

}
