//
//  Document.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/15/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation


class Document {
    
    var dID: String                  // Document ID
    var name: String                 // Document name
    var uID: String                  // User ID
    var path: String                 // Document path
    var visible: String              // Document visible
    var type: String                 // Document type
    var notes: String                // Document notes
    var date_uploaded: String        // Document date first uploaded

    init(dID: String, name: String, uID: String, path: String, visible: String, type: String, notes: String, date_uploaded: String) {
        
        self.dID = dID
        self.name = name
        self.uID = uID
        self.path = path
        self.visible = visible
        self.type = type
        self.notes = notes
        self.date_uploaded = date_uploaded
    }
}
