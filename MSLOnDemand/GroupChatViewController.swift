//
//  GroupChatViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/14/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class GroupChatViewController: UIViewController {
    
    @IBOutlet weak var tblViewGroupChat: UITableView!

    var selectedGroupCallID: String = String()
    var groupCallDetails: GroupCall?

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        groupCallDetails = groupCallList.getGroupCallDetails(selectedGroupCallID)

        NotificationCenter.default.addObserver(self, selector: #selector(GroupChatViewController.groupChatNotification), name: NSNotification.Name(rawValue: incomingGroupChatNotificationKey), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(GroupChatViewController.groupQuestionNotification), name: NSNotification.Name(rawValue: incomingGroupQuestionNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GroupChatViewController.groupLeaderQuestionNotification), name: NSNotification.Name(rawValue: incomingGroupLeaderQuestionNotificationKey), object: nil)
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Notification Methods

    func groupChatNotification(_ notification: Notification) {

        tblViewGroupChat.reloadData()
        tableViewScrollToBottom(true)
    }

    func groupQuestionNotification(_ notification: Notification) {

        let userInfo = notification.userInfo as! Dictionary<String,AnyObject>
        let fromUserId = userInfo["uID"] as! String

        let localUserId = userList.getLoginUserID()
        if groupCallDetails?.uID == localUserId && fromUserId != localUserId {
            
            let userName = userList.getUserName(fromUserId)

            let alert = UIAlertController(title: "Question", message: String(format: "%@ %@", "Question in Chat from user ", userName), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in }))
            self.present(alert, animated: true, completion: {})
        }

        tblViewGroupChat.reloadData()
        tableViewScrollToBottom(true)
    }

    func groupLeaderQuestionNotification(_ notification: Notification) {
        
        let userInfo = notification.userInfo as! Dictionary<String,AnyObject>
        let leaderUid = userInfo["leader"] as! String
        let fromUserId = userInfo["uID"] as! String
        let questionText = userInfo["question_text"] as! String

        let loginUid = userList.getLoginUserID()

        if loginUid == leaderUid {

            let userName = userList.getUserName(fromUserId)

            let alert = UIAlertController(title: "Question", message: String(format: "From: %@, Question: %@", userName, questionText), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ignore", style: UIAlertActionStyle.default, handler:{ (UIAlertAction) in
                
                // Do nothing - leader ignoring question request
            }))
            alert.addAction(UIAlertAction(title: "Accept", style: UIAlertActionStyle.default, handler:{ (UIAlertAction) in
                
                // Send out question to all so leader can reply
                globalSignal.sendGlobalSignalQuestion(currentGroupCallID, text: questionText)
            }))
            self.present(alert, animated: true, completion: {})
        }
    }

    // MARK: - Local Methods

    func tableViewScrollToBottom(_ animated: Bool) {
        
        let delay = 0.5 * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: time, execute: {
            
            let numberOfSections = self.tblViewGroupChat.numberOfSections
            let numberOfRows = self.tblViewGroupChat.numberOfRows(inSection: numberOfSections-1)
            
            if numberOfRows > 0 {

                let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                self.tblViewGroupChat.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: animated)
            }
            
        })
    }

    // MARK: - Public Methods

    func cleanupChat() {
        
        currentGroupCallID = ""
        currentGroupCallLeadUiD = ""
        chatManager.chatList.removeAll()
    }

    func removeOldChat() {
        
        chatManager.chatList.removeAll()
    }

}

// MARK: - UITableViewDataSource and UITableViewDelegate

extension GroupChatViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return chatManager.chatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupChatTableViewCell", for: indexPath) as! GroupChatTableViewCell
        
        let chat = chatManager.chatList[indexPath.row]

        if chat.isQuestion == true {
            
            cell.lblUserText.text = chat.text
            cell.lblUserName.text = ""
        }
        else {
            
            cell.lblUserText.text = chat.text
            cell.lblUserName.text = chat.userName
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
