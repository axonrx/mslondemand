//
//  CalliPhoneViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 8/24/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import MKDropdownMenu
import Alamofire
import AlamofireImage
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



protocol CalliPhoneViewDelegate {
    
    func newFileSelected(_ dID: String)
    
}

class CalliPhoneViewController: UIViewController {

    @IBOutlet weak var containerVideoView: UIView!
    @IBOutlet weak var containerChatView: UIView!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var textFieldChat: UITextField!
    @IBOutlet weak var btnSendChat: UIButton!
    @IBOutlet weak var textFieldContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnDocumentShare: UIButton!
    @IBOutlet weak var vwMKDropDown: MKDropdownMenu!

    var videoViewController: VideoViewController?
    var groupChatViewController: GroupChatViewController?

    var selectedGroupCallID: String = String()
    var callType = GroupCallType.groupCall

    var allUsersOnCall = Array<String>()

    var isRemoteRequestForGroupCall = false
    var isEmailLinkGroupCall = false
    var isPresentation = false
    
    var remoteGroupCallInfo: Dictionary<String,AnyObject>?
    var groupCallDetails: GroupCall?
    
    var timer: Timer?
    var toggleBtnChat = false

    override func viewDidLoad() {
        
        super.viewDidLoad()

        registerForKeyboardNotifications(self)

        NotificationCenter.default.addObserver(self, selector: #selector(CalliPhoneViewController.groupChatNotification), name: NSNotification.Name(rawValue: incomingGroupChatNotificationKey), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CalliPhoneViewController.groupQuestionNotification), name: NSNotification.Name(rawValue: incomingGroupQuestionNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CalliPhoneViewController.groupLeaderQuestionNotification), name: NSNotification.Name(rawValue: incomingGroupLeaderQuestionNotificationKey), object: nil)

        Gradient.setBackgroundGradient(self.view)

        //vwMKDropDown.layer.borderColor = UIColor.clearColor().CGColor
        vwMKDropDown.layer.borderWidth = 0.0
        vwMKDropDown.selectedComponentBackgroundColor = UIColor(red: 0.91, green: 0.92, blue: 0.94, alpha: 1.0)
        vwMKDropDown.dropdownBackgroundColor = UIColor(red: 0.91, green: 0.92, blue: 0.94, alpha: 1.0)
        vwMKDropDown.backgroundDimmingOpacity = 0.05

        userList.resetUsersMuteState()
        
        if isRemoteRequestForGroupCall == true {
            
            currentGroupCallID = remoteGroupCallInfo!["groupCallID"] as! String
            currentGroupCallLeadUiD = remoteGroupCallInfo!["uID"] as! String
            allUsersOnCall = remoteGroupCallInfo!["users"] as! Array<String>
            allUsersOnCall.append(remoteGroupCallInfo!["uID"] as! String)
        }
        else {
            
            groupCallDetails = groupCallList.getGroupCallDetails(selectedGroupCallID)
            
            currentGroupCallID = (groupCallDetails?.groupCallID)!
            currentGroupCallLeadUiD = (groupCallDetails?.uID)!
            
            allUsersOnCall = (groupCallDetails?.users)!
            allUsersOnCall.append((groupCallDetails?.uID)!)
        }
        
        UIApplication.shared.isIdleTimerDisabled = true

        let storyboard = UIStoryboard(name: "Call", bundle: nil)
        
        videoViewController = storyboard.instantiateViewController(withIdentifier: "VideoViewController") as? VideoViewController
        videoViewController?.delegate = self
        videoViewController?.callType = callType
        videoViewController?.isEmailLinkGroupCall = isEmailLinkGroupCall
        videoViewController?.selectedGroupCallID = selectedGroupCallID
        videoViewController!.view.frame = containerVideoView.bounds
        containerVideoView.addSubview(videoViewController!.view)
        addChildViewController(videoViewController!)
        videoViewController!.didMove(toParentViewController: self)

        groupChatViewController = storyboard.instantiateViewController(withIdentifier: "GroupChatViewController") as? GroupChatViewController
        groupChatViewController!.view.frame = containerChatView.bounds
        groupChatViewController?.selectedGroupCallID = selectedGroupCallID
        groupChatViewController!.removeOldChat()
        containerChatView.addSubview(groupChatViewController!.view)
        addChildViewController(groupChatViewController!)
        groupChatViewController!.didMove(toParentViewController: self)
        
        containerChatView.isHidden = true
        btnChat.tag = 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        unregisterFromKeyboardNotifications()

        vwMKDropDown.closeAllComponents(animated: true)
    }

    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: IBActions Methods
    
    @IBAction func chatButtonPressed(_ button: UIButton) {
    
        if btnChat.tag == 0 {
            
            btnChat.tag = 1
            containerChatView.isHidden = false
            timer?.invalidate()
            timer = nil
        }
        else {
            
            btnChat.tag = 0
            containerChatView.isHidden = true
            btnChat.setImage(UIImage(named: "call-view-chat-open"), for: UIControlState())
        }
    }

    @IBAction func sendButtonPressed(_ sender: AnyObject) {

        if textFieldChat.text?.characters.count > 0 {
            
            globalSignal.sendGlobalSignalChat(currentGroupCallID, text: textFieldChat.text!)
            textFieldChat.text = ""
        }
        
        textFieldChat.resignFirstResponder()
    }

    @IBAction func shareDocumentButtonPressed(_ sender: AnyObject) {

        let storyboard = UIStoryboard(name: "MainiPhone", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DocumentsiPhoneViewController") as! DocumentsiPhoneViewController
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        vc.isVideoCall = true
        self.present(vc, animated: true, completion: nil)
    }

    // MARK: - Notification Methods
    
    func groupChatNotification(_ notification: Notification) {

        if btnChat.tag == 1 || timer != nil {

            return;
        }

        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CalliPhoneViewController.updateChatButton(_:)), userInfo: nil, repeats: true)
    }
    
    func groupQuestionNotification(_ notification: Notification) {
        
        if btnChat.tag == 1 || timer != nil {
            
            return;
        }

        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CalliPhoneViewController.updateChatButton(_:)), userInfo: nil, repeats: true)
    }

    func groupLeaderQuestionNotification(_ notification: Notification) {

        let userInfo = notification.userInfo as! Dictionary<String,AnyObject>
        let leaderUid = userInfo["leader"] as! String
        let fromUserId = userInfo["uID"] as! String
        let questionText = userInfo["question_text"] as! String

        let loginUid = userList.getLoginUserID()

        if loginUid == leaderUid {

            let userName = userList.getUserName(fromUserId)

            let alert = UIAlertController(title: "Question", message: String(format: "From: %@, Question: %@", userName, questionText), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ignore", style: UIAlertActionStyle.default, handler:{ (UIAlertAction) in
            
                // Do nothing - leader ignoring question request
            }))
            alert.addAction(UIAlertAction(title: "Accept", style: UIAlertActionStyle.default, handler:{ (UIAlertAction) in

                // Send out question to all so leader can reply
                globalSignal.sendGlobalSignalQuestion(currentGroupCallID, text: questionText)
            }))
            self.present(alert, animated: true, completion: {})
        }
    }
    
    func updateChatButton(_ timer: Timer) {

        if toggleBtnChat == false {
            
            btnChat.setImage(UIImage(named: "call-view-chat-message"), for: UIControlState())
            toggleBtnChat = true
        }
        else {
            
            btnChat.setImage(UIImage(named: "call-view-chat-open"), for: UIControlState())
            toggleBtnChat = false
        }
    }

}

extension CalliPhoneViewController: VideoViewButtonPressedDelegate {
    
    func callEndedButtonPressed() {
        
        UIApplication.shared.isIdleTimerDisabled = false

        groupChatViewController!.cleanupChat()

        self.dismiss(animated: true, completion: nil)
    }
    
    func userConnectedToStream(_ uID: String) {
        
    }
    
    func userDisconnectedFromStream(_ uID: String) {
        
        var idx = 0
        for userId:String in allUsersOnCall {
            
            if userId == uID {
                
                allUsersOnCall.remove(at: idx)
                break
            }
            
            idx += 1
        }
        
        if self.allUsersOnCall.count == 1 {
            
            let alert = UIAlertController(title: "Call Ended", message: "Everyone has disconnected. The Call will end now.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                
                self.videoViewController!.endCallNow()
            }))
            self.present(alert, animated: true, completion: {})
        }
    }
    
    func isRemoteCall() -> Bool {
        
        return isRemoteRequestForGroupCall
    }
    
    func getRemoteCallInfo() -> Dictionary<String,AnyObject>? {
        
        return remoteGroupCallInfo
    }
    
    func isPresentaCall() -> Bool {
        
        return false
    }
    
    func setPresentaCall(_ presenta: Bool) {
        
        _ = presenta
    }
    
    func isPresentationCall() -> Bool {
        
        return isPresentation
    }
    
    func stopDocumentSharingCall() {
        
        vwMKDropDown.isHidden = false
    }

}

// MARK: - UITextFieldDelegate Methods

extension CalliPhoneViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textFieldChat.text?.characters.count > 0 {
            
            globalSignal.sendGlobalSignalChat(currentGroupCallID, text: textFieldChat.text!)
            textFieldChat.text = ""
        }
        
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - KeyboardStateDelegate Methods

extension CalliPhoneViewController: KeyboardStateDelegate {
    
    func keyboardWillTransition(_ state: KeyboardState) {
        
        // keyboard will show or hide
    }
    
    func keyboardTransitionAnimation(_ state: KeyboardState) {
        
        switch state {
        case .activeWithHeight(let height):
            textFieldContainerBottomConstraint.constant = height
        case .hidden:
            textFieldContainerBottomConstraint.constant = 0.0
        }
        
        view.layoutIfNeeded()
    }
    
    func keyboardDidTransition(_ state: KeyboardState) {
        
        // keyboard animation finished
    }
}

// MARK: - CalliPhoneViewDelegate Methods

extension CalliPhoneViewController: CalliPhoneViewDelegate {

    func newFileSelected(_ dID: String) {
        
        vwMKDropDown.isHidden = true
        videoViewController?.shareDocument(dID)
    }

}

// MARK: - MKDropdownMenuDataSource Methods

extension CalliPhoneViewController: MKDropdownMenuDataSource {
    
    func numberOfComponents(in dropdownMenu: MKDropdownMenu) -> Int {
        
        return 1
    }

    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, numberOfRowsInComponent component: Int) -> Int {
        
        return allUsersOnCall.count
    }

}

// MARK: - MKDropdownMenuDelegate Methods

extension CalliPhoneViewController: MKDropdownMenuDelegate {
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, rowHeightForComponent component: Int) -> CGFloat {
        
        return 60    // use default row height
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, widthForComponent component: Int) -> CGFloat {
        
        return 0     // use automatic width
    }

    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, shouldUseFullRowWidthForComponent component: Int) -> Bool {
        
        return false
    }

    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, attributedTitleForComponent component: Int) -> NSAttributedString? {
        
        return  NSAttributedString(string: "Active Callers",
                                   attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16, weight: UIFontWeightLight),
                                    NSForegroundColorAttributeName: UIColor.black])
    }

    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, attributedTitleForSelectedComponent component: Int) -> NSAttributedString? {

        return  NSAttributedString(string: "Active Callers",
                                   attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16, weight: UIFontWeightRegular),
                                    NSForegroundColorAttributeName: UIColor.black])
    }

    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let userID = allUsersOnCall[row]
        let userName = userList.getUserName(userID)
        let thumbnailPath = userList.getUserThumbnailPath(userID)

        let view = ActiveUserView.instanceFromNib() as! ActiveUserView

        view.lblUserName.text = userName

        Alamofire.request(thumbnailPath)
            .responseImage { response in
                
                if let image = response.result.value {
                    
                    let circularImage = image.af_imageRoundedIntoCircle()
                    view.imgViewAvatar.image = circularImage
                }
        }

        return view
    }

    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, backgroundColorForRow row: Int, forComponent component: Int) -> UIColor? {
        
        return nil
    }

    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, didSelectRow row: Int, inComponent component: Int) {
    
        let uID = allUsersOnCall[row]

        // Move video view to this user
        print("Change video to user ", uID)
        
        videoViewController?.switchVideoToUser(uID)

        vwMKDropDown.closeAllComponents(animated: true)
    }

}
