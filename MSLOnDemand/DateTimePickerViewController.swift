//
//  DateTimePickerViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/8/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class DateTimePickerViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtFieldDateTime: UITextField!
    @IBOutlet weak var segControlCallType: UISegmentedControl!
    @IBOutlet weak var btnCallStateButton: UIButton!

    var delegate:CallDetailsViewDelegate?
    var popDatePicker: PopDatePicker?
    var callType = GroupCallType.groupCall

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        popDatePicker = PopDatePicker(forTextField: txtFieldDateTime)
        txtFieldDateTime.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Class Methods

    func resign() {
        
        txtFieldDateTime.resignFirstResponder()
    }

    // MARK: - UITextFieldDelegate

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField === txtFieldDateTime {
            
            resign()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd h:mm a"
            var initDate : Date? = formatter.date(from: txtFieldDateTime.text!)
            
            if initDate == nil {
                
                initDate = Date()
            }
            
            let dataChangedCallback : PopDatePicker.PopDatePickerCallback = { (newDate : Date, forTextField : UITextField) -> () in
                
                // here we don't use self (no retain cycle)
                forTextField.text = (newDate.ToDateTimeSimpleString() ?? "?") as String
            }
            
            popDatePicker!.pick(self, initDate: initDate, dataChanged: dataChangedCallback)
            return false
        }
        else {
            
            return true
        }
    }

    // MARK: - IBAction Methods

    @IBAction func callStateButtonPressed(_ button: UIButton) {
    
        if button.tag == 0 {    // CALL NOW
/*
            if selectedUsers.count == 0 {
                
                let alert = UIAlertController(title: "Call Error", message: "You need to select contacts for a call.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
*/
            delegate!.callNowButtonPressed()
        }
        else {                  // SAVE
/*
            if selectedUsers.count == 0 {
                
                let alert = UIAlertController(title: "Schedule Error", message: "You need to select contacts before you can schedule a call.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
*/
            delegate!.scheduleCallButtonPressed()
        }
    }

    // MARK: - Public Methods
    
    func setDateTime(_ dateTime: String) {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = TimeZone(identifier: "UTC")
       
        if let date = df.date(from: dateTime) {
        
            df.timeZone = TimeZone.autoupdatingCurrent
            df.dateFormat = "yyyy-MM-dd h:mm a"
            let twelveHourDateTime = df.string(from: date)
            txtFieldDateTime.text = twelveHourDateTime
       }
    }

    func getDateTime() -> String {
        
        let dateAsString = txtFieldDateTime.text!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm a"
        let date = dateFormatter.date(from: dateAsString)
        
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date24 = dateFormatter.string(from: date!)
        
        return date24
    }

    func changeButtonToCallNow() {
        
        btnCallStateButton.setTitle("CALL NOW",for: .normal)
        btnCallStateButton.tag = 0
    }
    
    func changeButtonToSave() {
        
        btnCallStateButton.setTitle("SAVE",for: .normal)
        btnCallStateButton.tag = 1
    }

}
