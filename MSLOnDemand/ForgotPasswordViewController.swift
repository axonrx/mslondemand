//
//  ForgotPasswordViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 6/29/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var lblServerErrorMessages: UILabel!

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.clear
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.view.insertSubview(blurEffectView, at: 0)

        let labelWidth = self.view.bounds.width / 2
        let emailLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: 160, width: labelWidth, height: 60))
        emailLabel.textColor = UIColor.black
        emailLabel.enableMaterialPlaceHolder = true
        emailLabel.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        emailLabel.placeholder = "Email"
        emailLabel.lineColor = UIColor.black
        emailLabel.tintColor = UIColor.black
        emailLabel.autocapitalizationType = .none
        self.view.addSubview(emailLabel)

        lblServerErrorMessages.isHidden = true;
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions

    @IBAction func submitButtonPressed(_ sender: AnyObject) {
    
    }
    
    @IBAction func closeButtonPressed(_ sender: AnyObject) {
    
        self.dismiss(animated: true, completion: nil)
    }
}
