//
//  FileTableViewCell.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/14/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class FileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var imgViewFile: UIImageView!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
}
