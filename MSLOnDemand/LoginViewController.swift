//
//  LoginViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 6/29/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import MBProgressHUD

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var lblServerErrorMessages: UILabel!
    @IBOutlet weak var imgViewBrandLogo: UIImageView!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var imgBackgroundiPhone: UIImageView!
    @IBOutlet weak var imgViewBrandLogoiPhone: UIImageView!
    
    var emailLabel: JJMaterialTextfield? = nil
    var passwordLabel: JJMaterialTextfield? = nil
    
    var hud: MBProgressHUD?
    
    // MARK: - UIViewController handlers

    override func viewDidLoad() {
        
        super.viewDidLoad()

        lblVersion.text = "Version: " + Bundle.main.releaseVersionNumber! + " Build: " + Bundle.main.buildVersionNumber!

        let labelWidth = self.view.bounds.width / 2
        emailLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: 54, width: labelWidth, height: 60))
        emailLabel!.textColor = UIColor.black
        emailLabel!.enableMaterialPlaceHolder = true
        emailLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        //emailLabel!.lineColor = UIColor.init(red: 0.482, green: 0.800, blue: 1.000, alpha: 1.000)
        //emailLabel!.tintColor = UIColor.init(red: 0.482, green: 0.800, blue: 1.000, alpha: 1.000)
        emailLabel!.placeholder = "Email"
        emailLabel!.lineColor = UIColor.black
        emailLabel!.tintColor = UIColor.black
        emailLabel!.autocapitalizationType = .none
        emailLabel!.autocorrectionType = .no
        emailLabel!.delegate = self
        self.view.addSubview(emailLabel!)

        passwordLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: 140, width: labelWidth, height: 60))
        passwordLabel!.textColor = UIColor.black
        passwordLabel!.enableMaterialPlaceHolder = true
        passwordLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        passwordLabel!.lineColor = UIColor.black
        passwordLabel!.tintColor = UIColor.black
        passwordLabel!.placeholder = "Password"
        passwordLabel!.isSecureTextEntry = true
        passwordLabel!.delegate = self
        self.view.addSubview(passwordLabel!)

        let attributes = [
            NSForegroundColorAttributeName: UIColor.black,
            NSFontAttributeName : UIFont(name: "Helvetica Neue", size: 17)!
        ]
        
        //emailLabel!.attributedPlaceholder = NSAttributedString(string: "Email", attributes:attributes)
        passwordLabel!.attributedPlaceholder = NSAttributedString(string: "Password", attributes:attributes)

        lblServerErrorMessages.isHidden = true;
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        applicationSetup.getApplicationSetup("666", completion: { (result, code) in

            let imageUrl = applicationSetup.getApplicationLogoUrl()
            if imageUrl != nil {
                
                Alamofire.request(imageUrl!)
                .responseImage { response in
                    //debugPrint(response)
                    //print(response.request)
                    //print(response.response)
                    //debugPrint(response.result)
                    
                    if let image = response.result.value {
                        
                        //print("image downloaded: \(image)")
                        if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO {
                            
                            self.imgViewBrandLogo.image = image
                        }
                        else {
                            
                            self.imgViewBrandLogoiPhone.image = image
                        }
                    }
                }
            }

            let backgroundUrl = applicationSetup.getApplicationBackgroundUrl()
            if backgroundUrl != nil {
                
                Alamofire.request(backgroundUrl!)
                    .responseImage { response in
                        //debugPrint(response)
                        //print(response.request)
                        //print(response.response)
                        //debugPrint(response.result)
                        
                        if let image = response.result.value {
                            
                            //print("image downloaded: \(image)")
                            if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO {
                                
                                self.imgBackground.image = image
                            }
                            else {
                                
                                self.imgBackgroundiPhone.image = image
                            }
                        }
                        else {
                            
                            Gradient.setBackgroundGradient(self.view)
                        }
                }
            }
            else {
                
                Gradient.setBackgroundGradient(self.view)
            }
        })
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator);
        
        coordinator.animate(alongsideTransition: nil, completion: {
            _ in
            
            let labelWidth = self.view.bounds.width / 2
            let newX = (self.view.bounds.width - labelWidth) / 2
            self.emailLabel?.frame = CGRect(x: newX, y: 54, width: labelWidth, height: 60)
            self.passwordLabel?.frame = CGRect(x: newX, y: 140, width: labelWidth, height: 60)
        })
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }

    // MARK: - IBActions

    @IBAction func submitButtonPressed(_ sender: AnyObject) {

        hud = MBProgressHUD.showAdded(to: self.view, animated:true)
        hud!.mode = MBProgressHUDMode.indeterminate
        userList.getAllUsersWhenLogin((emailLabel?.text)!, password: (passwordLabel?.text)!, completion: loginCallback)
    }

    func loginCallback(_ result: String, code: String) {
        
        hud?.hide(animated: true)

        if result == "success" {

            if !(UserDefaults.standard.object(forKey: "userAvaialble") != nil) {
                
                UserDefaults.standard.set(false, forKey: "userAvaialble")
            }

            UserDefaults.standard.set(true, forKey: "userLoggedIn")
            UserDefaults.standard.setValue(userList.getLoginUserID(), forKey: "userId")

            if !DeviceType.IS_IPAD && !DeviceType.IS_IPAD_PRO {
 
                let mainVcIntial = kConstantObj.SetIntialMainViewController("MainiPhoneViewController")
                UIApplication.shared.keyWindow?.rootViewController = mainVcIntial

                let window = UIApplication.shared.windows[0] as UIWindow;
                UIView.transition(
                    from: window.rootViewController!.view,
                    to: mainVcIntial.view,
                    duration: 0.65,
                    options: .transitionCrossDissolve,
                    completion: {
                        finished in //window.rootViewController = vc
                        UIApplication.shared.keyWindow?.rootViewController = mainVcIntial;
                })
            }
            else {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController")
                self.present(vc, animated: true, completion: nil)
            }
        }
        else {
            
            lblServerErrorMessages.isHidden = false
            lblServerErrorMessages.text = code
        }
    }

    // MARK: - Local Completion Methods
    
    // MARK: - UITextFieldDelegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == passwordLabel {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated:true)
            hud.mode = MBProgressHUDMode.indeterminate
            userList.getAllUsersWhenLogin((emailLabel?.text)!, password: (passwordLabel?.text)!, completion: loginCallback)
        }
        
        self.view.endEditing(true)
        return false
    }

}

