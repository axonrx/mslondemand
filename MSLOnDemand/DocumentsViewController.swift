//
//  DocumentsViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/14/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


protocol DocumentsViewDelegate {
    
    func newFileSelected(_ dID: String)
    func newFolderSelected(_ row: Int)
    func getFilePreviewNotes() -> String
    func fileSelectedMoveToFolder(_ dID: String)
    func documentDeletedUpdateFolders(_ dID: String)
    func folderSelectedForCallDetails(_ fID: String)
    func addFolderToCall()
    func addDocumentToCall(_ dID: String)
    func adjustViewForKeyboard(_ adjust: Bool)

}

class DocumentsViewController: UIViewController {

    @IBOutlet weak var imgBackground: UIImageView!

    @IBOutlet weak var imgViewBrandLogo: UIImageView!
    @IBOutlet weak var lblProgramLocation: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblUserInfo: UILabel!
    @IBOutlet weak var imgViewAvatar: UIImageView!
    @IBOutlet weak var vwTitleView: UIView!
    @IBOutlet weak var vwFilePreview: UIView!
    @IBOutlet weak var vwFileListView: UIView!
    
    var backButtonString = "< Main"
    var selectedGroupCallID: String = String()
    var groupCallDetails: GroupCall?

    var currentlySelectedDocument = 0
    var currentlySelectedFolder = 0

    var isSelectFolder = true
    var curSelectFolder = 0

    var screenMode = 0

    var hideAddFolderToCall = true
    var adjustViewForKeyboard = true

    var delegate:DocumentsViewDelegate?

    var filePreviewViewController: FilePreviewViewController?
    var fileListViewController: FileListViewController?
    var folderViewController: FolderViewController?

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        userList.getUpdatedUsersList(uploadUserListComplete)

        if applicationSetup.getApplicationSetupNetworkComplete() == true {
            
            setupApplicationLook()
        }
        else {
            
            applicationSetup.getApplicationSetup("666", completion: { (result, code) in
                
                self.setupApplicationLook()
            })
        }

        groupCallDetails = groupCallList.getGroupCallDetails(selectedGroupCallID)

        folderViewController?.hideAddFolderToCall = hideAddFolderToCall
        fileListViewController?.hideAddFolderToCall = hideAddFolderToCall

        NotificationCenter.default.addObserver(self, selector: #selector(DocumentsViewController.keyboardWillShowNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DocumentsViewController.keyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        lblVersion.text = "Version: " + Bundle.main.releaseVersionNumber! + " Build: " + Bundle.main.buildVersionNumber!
        
        vwTitleView.layer.borderWidth = 1
        vwTitleView.layer.borderColor = UIColor.darkGray.cgColor

        vwFilePreview.layer.borderWidth = 1
        vwFilePreview.layer.borderColor = UIColor.darkGray.cgColor

        vwFileListView.layer.borderWidth = 1
        vwFileListView.layer.borderColor = UIColor.darkGray.cgColor
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.incomingVideoGroupCallNotification), name: NSNotification.Name(rawValue: incomingVideoGroupCallNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.incomingAudioGroupCallNotification), name: NSNotification.Name(rawValue: incomingAudioGroupCallNotificationKey), object: nil)
        
        let orient = UIApplication.shared.statusBarOrientation
        
        switch orient {
            
        case .portrait:
            self.screenMode = 0
            break
        default:
            self.screenMode = 1
            break
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        folderViewController!.selectFolder(0)
        fileListViewController!.showFolder(0)
        filePreviewViewController!.showDocument("-1")
    }

/*
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator);
        
        coordinator.animateAlongsideTransition(nil, completion: {
            _ in
            
            Gradient.setBackgroundGradient(self.view)
        })
    }
*/

    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: incomingVideoGroupCallNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: incomingAudioGroupCallNotificationKey), object: nil)
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator);
        
        coordinator.animate(alongsideTransition: nil, completion: {
            _ in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
                
            case .portrait:
                self.screenMode = 0
                break
            default:
                self.screenMode = 1
                break
            }
            
        })
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //print(segue.identifier)
        if let vc = segue.destination as? FilePreviewViewController, segue.identifier == "FilePreviewView" {
            
            filePreviewViewController = vc
        }
        
        if let vc = segue.destination as? FileListViewController, segue.identifier == "FileListView" {
            
            fileListViewController = vc
            vc.delegate = self
        }
        
        if let vc = segue.destination as? FolderViewController, segue.identifier == "FolderView" {
            
            folderViewController = vc
            vc.delegate = self
        }
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        
        self .dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Keyboard Notifications

    func keyboardWillShowNotification(_ notification: Notification) {
        
        if adjustViewForKeyboard == false {
            
            return
        }

        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.view.frame.origin.y -= keyboardSize.height
        }
        
    }
    
    func keyboardWillHideNotification(_ notification: Notification) {
        
        if adjustViewForKeyboard == false {
            
            return
        }

        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.view.frame.origin.y += keyboardSize.height
        }
    }

    // MARK: - Notification Methods
    
    func incomingVideoGroupCallNotification(_ notification: Notification) {
        
        let videoInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let groupCallID = videoInfo["groupCallID"] as! String
        print("GroupCall ID = " + groupCallID)
        let groupCallName = videoInfo["groupCallName"] as! String
        print("GroupCall Name = " + groupCallName)
        let groupCallUserId = videoInfo["uID"] as! String
        print("FROM: " + groupCallUserId)
        //let users = videoInfo["users"] as! Array<String>
        
        let alert = UIAlertController(title: "Video Group Call", message: "Do you want to join group call: " + groupCallName + " from " + userList.getUserName(groupCallUserId), preferredStyle: UIAlertControllerStyle.alert)
        
        let rejectCallAction = UIAlertAction(title: "Reject", style: .cancel) { (action:UIAlertAction!) in
            
            globalSignal.sendGlobalSignalRejectCall(groupCallID)
        }
        alert.addAction(rejectCallAction)
        
        let OKAction = UIAlertAction(title: "Accept", style: .default) { (action:UIAlertAction!) in
            
            userList.setUserStatus(UserOnlineStatus.onlineOnCall)
            
            print("Accept Video Group Call button pressed")
            var storyboard = UIStoryboard(name: "Call", bundle: nil)
            if self.screenMode == 1 {
                
                storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
            }
            let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = groupCallID
            vc.isPresentation = false
            vc.remoteGroupCallInfo = videoInfo
            vc.callType = .groupCall
            vc.isRemoteRequestForGroupCall = true
            vc.isEmailLinkGroupCall = false
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func incomingAudioGroupCallNotification(_ notification: Notification) {
        
        let audioInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let groupCallID = audioInfo["groupCallID"] as! String
        print("GroupCall ID = " + groupCallID)
        let groupCallName = audioInfo["groupCallName"] as! String
        print("GroupCall Name = " + groupCallName)
        let groupCallUserId = audioInfo["uID"] as! String
        print("FROM: " + groupCallUserId)
        
        let alert = UIAlertController(title: "Audio Group Call", message: "Do you want to join a Audio Group Call: " + groupCallName + " from " + userList.getUserName(groupCallUserId), preferredStyle: UIAlertControllerStyle.alert)
        
        let rejectCallAction = UIAlertAction(title: "Reject", style: .cancel) { (action:UIAlertAction!) in
            
            globalSignal.sendGlobalSignalRejectCall(groupCallID)
        }
        alert.addAction(rejectCallAction)
        
        let OKAction = UIAlertAction(title: "Accept", style: .default) { (action:UIAlertAction!) in
            
            userList.setUserStatus(UserOnlineStatus.onlineOnCall)
            
            print("Accept Audio Call button pressed")
            var storyboard = UIStoryboard(name: "Call", bundle: nil)
            if self.screenMode == 1 {
                
                storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
            }
            let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = groupCallID
            vc.isPresentation = false
            vc.isRemoteRequestForGroupCall = true
            vc.isEmailLinkGroupCall = false
            vc.remoteGroupCallInfo = audioInfo
            vc.callType = .audioCall
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Local Methods
    
    func setupApplicationLook() {

        let imageUrl = applicationSetup.getApplicationLogoUrl()
        if imageUrl != nil {
            
            Alamofire.request(imageUrl!)
                .responseImage { response in
                    
                    if let image = response.result.value {
                        
                        self.imgViewBrandLogo.image = image
                    }
            }
        }

        let backgroundUrl = applicationSetup.getApplicationBackgroundUrl()
        if backgroundUrl != nil {
            
            Alamofire.request(backgroundUrl!)
                .responseImage { response in
                    //debugPrint(response)
                    //print(response.request)
                    //print(response.response)
                    //debugPrint(response.result)
                    
                    if let image = response.result.value {
                        
                        //print("image downloaded: \(image)")
                        self.imgBackground.image = image
                    }
                    else {
                        
                        Gradient.setBackgroundGradient(self.view)
                    }
            }
        }
        else {
            
            Gradient.setBackgroundGradient(self.view)
        }
    }
    
    // MARK: - Local Completion Methods
    
    func uploadUserListComplete(_ result: String, code: String) {
        
        if result == "success" {
            
            if (UserDefaults.standard.object(forKey: "userAvaialble") as! Bool) == true {
                
                userList.setUserStatus(UserOnlineStatus.onlineAvailable)
            }
            
            let userPicPath = userList.userLoggedIn?.picPath
            //print(userPicPath)
            
            Alamofire.request(userPicPath!)
                .responseImage { response in
                    //debugPrint(response)
                    //print(response.request)
                    //print(response.response)
                    //debugPrint(response.result)
                    
                    if let image = response.result.value {
                        
                        //print("image downloaded: \(image)")
                        //let radius: CGFloat = 5.0
                        //let roundedImage = image.af_imageWithRoundedCornerRadius(radius)
                        //self.imgViewAvatar.image = roundedImage
                        let circularImage = image.af_imageRoundedIntoCircle()
                        self.imgViewAvatar.image = circularImage
                    }
            }
            
            // Update login user image and user name
            let user = userList.userLoggedIn
            lblUserInfo.text = user!.jobTitle + " " + user!.firstName + " " + user!.lastName + ", " + user!.credentials
            lblProgramLocation.text = user!.programs + ", " + user!.location
        }
    }

}

extension DocumentsViewController: DocumentsViewDelegate {
    
    func newFileSelected(_ dID: String) {
        
        filePreviewViewController!.showDocument(dID)
    }

    func newFolderSelected(_ row: Int) {
        
        fileListViewController!.showFolder(row)
        curSelectFolder = row
    }

    func getFilePreviewNotes() -> String {
        
        return filePreviewViewController!.getDocumentNotes()
    }

    func fileSelectedMoveToFolder(_ dID: String) {
        
        folderViewController!.documentSelectedMoveToFolder(dID)
    }
    
    func folderSelectedForCallDetails(_ fID: String) {
        
    }

    func addFolderToCall() {
        
        if curSelectFolder != 0 && isSelectFolder == true {
            
            let folder = folderManager.folderList[curSelectFolder - 1]
            delegate!.folderSelectedForCallDetails(folder.fID)
        }
        
        self .dismiss(animated: true, completion: nil)
    }

    func addDocumentToCall(_ dID: String) {
        
        delegate!.addDocumentToCall(dID)
        self .dismiss(animated: true, completion: nil)
    }

    func documentDeletedUpdateFolders(_ dID: String) {
        
        folderViewController!.documentDeletedUpdateFolders(dID)
    }

    func adjustViewForKeyboard(_ adjust: Bool) {

        // false -> do not adjust, true adjust

        adjustViewForKeyboard = adjust
    }

}

