//
//  VideoViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/14/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import JWT


protocol VideoViewButtonPressedDelegate {
    
    func callEndedButtonPressed()
    func userConnectedToStream(_ uID: String)
    func userDisconnectedFromStream(_ uID: String)
    func isRemoteCall() -> Bool
    func getRemoteCallInfo() -> Dictionary<String,AnyObject>?
    func isPresentaCall() -> Bool
    func setPresentaCall(_ presenta: Bool)
    func isPresentationCall() -> Bool
    func stopDocumentSharingCall()

}

class VideoViewController: UIViewController {
    
    @IBOutlet weak var lblAudioOnlyCall: UILabel!
    @IBOutlet weak var lblPresentation: UILabel!
    @IBOutlet weak var vwUsersVideoView: UIView!
    @IBOutlet weak var btnQuestion: UIButton!
    @IBOutlet weak var btnMute: UIButton!
    @IBOutlet weak var btnVideoState: UIButton!
    @IBOutlet weak var btnEndCall: UIButton!
    @IBOutlet weak var btnStopDocumentSharing: UIButton!
    @IBOutlet weak var webViewDocumentSharing: UIWebView!
    @IBOutlet weak var imgViewNetworkStatus: UIImageView!
    @IBOutlet weak var vwButtons: UIView!

    var delegate:VideoViewButtonPressedDelegate?
    var selectedGroupCallID: String = String()
    var groupCallID: String?
    var groupCallUiD: String?
    var groupCallName: String?
    var groupCallUsers = Array<String>()
    var callType = GroupCallType.groupCall

    var audioCallUid = ""

    var video_pl_ratio: Double = -1
    var audio_pl_ratio: Double = -1
    var prevVideoPacketsLost: UInt64 = 0
    var prevVideoPacketsRcvd: UInt64 = 0
    var prevAudioPacketsLost: UInt64 = 0
    var prevAudioPacketsRcvd: UInt64 = 0
    var prevVideoTimestamp: Double = 0
    var prevVideoBytes: UInt64 = 0
    var prevAudioTimestamp: Double = 0
    var prevAudioBytes: UInt64 = 0
    var video_bw: UInt64 = 0
    var audio_bw: UInt64 = 0
    var runOneSession = false

    var txtFieldQuestion: UITextField!
    var questionText = ""

    var isEmailLinkGroupCall = false

    var videoConnection: VideoConnection = VideoConnection()
    var presenterConnection: PresenterConnection = PresenterConnection()

    var isDocumentSharing = false
    var oldVideoCapture : OTVideoCapture?

    var session : OTSession?
    var publisher : OTPublisher?
    var subscribers = [OTSubscriber]()
    var currentSubscriberVideoView: OTSubscriber?

    let TIME_WINDOW: Double = 3000 // 3 seconds

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(VideoViewController.applicationDidEnterBackgroundNotification), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(VideoViewController.applicationDidBecomeActiveNotification), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(VideoViewController.screenShareNotification), name: NSNotification.Name(rawValue: incomingScreenShareNotificationKey), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(VideoViewController.presenterDisconnectUserGroupCallNotification), name: NSNotification.Name(rawValue: presenterDisconnectUserGroupCallNotificationKey), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(VideoViewController.presenterSwitchVideoToUserGroupCallNotification), name: NSNotification.Name(rawValue: presenterSwitchVideoToUserGroupCallNotificationKey), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(VideoViewController.updateCallBaudwidthStatusNotification), name: NSNotification.Name(rawValue: updateCallBaudwidthStatusNotificationKey), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(VideoViewController.incomingClientChangeMute), name: NSNotification.Name(rawValue: incomingClientMuteNotificationKey), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(VideoViewController.incomingClientChangeUnMute), name: NSNotification.Name(rawValue: incomingClientUnMuteNotificationKey), object: nil)

        webViewDocumentSharing.isHidden = true
        btnStopDocumentSharing.isHidden = true

        delegate?.setPresentaCall(false)
        lblPresentation.isHidden = true

        if delegate?.isPresentationCall() == true {
            
            lblPresentation.isHidden = false
            lblPresentation.text = "Presentation"
        }

        btnMute.tag = 0
        btnVideoState.tag = 0

        if callType == GroupCallType.audioCall {
            
            lblAudioOnlyCall.isHidden = false
            btnVideoState.isHidden = true
        }
        else {
            
            lblAudioOnlyCall.isHidden = true
            btnVideoState.isHidden = false
            view.bringSubview(toFront: lblAudioOnlyCall)
        }

        view.bringSubview(toFront: imgViewNetworkStatus)
        
        if delegate!.isRemoteCall() == true {

            let remoteGroupCallInfo = delegate!.getRemoteCallInfo()
            groupCallID = remoteGroupCallInfo!["groupCallID"] as? String
            groupCallUiD = remoteGroupCallInfo!["uID"] as? String
            groupCallName = remoteGroupCallInfo!["groupCallName"] as? String
            groupCallUsers = remoteGroupCallInfo!["users"] as! Array

            videoConnection.getVideoGroupCallToken(groupCallID!, name: groupCallName!, completion: startVideoGroupCall)
        }
        else if (isEmailLinkGroupCall == true) {
            
            let remoteGroupCallInfo = delegate!.getRemoteCallInfo()
            let uID = remoteGroupCallInfo!["uID"]! as! String
            let uGUID = remoteGroupCallInfo!["uGUID"]! as! String
            let meetingGUID = remoteGroupCallInfo!["mGUID"]! as! String
            
            videoConnection.getVideoEmailLinkToken(uID, uGUID: uGUID, meetingGUID: meetingGUID, completion: joinEmailLinkCall)
        } else {
            
            let groupCallDetails = groupCallList.getGroupCallDetails(selectedGroupCallID)

            groupCallID = (groupCallDetails?.groupCallID)!
            groupCallUiD = (groupCallDetails?.uID)!
            groupCallName = (groupCallDetails?.groupCallName)!
            groupCallUsers = (groupCallDetails?.users)!
            callType = (groupCallDetails?.callType)!

            if callType == GroupCallType.audioCall {

                audioCallUid = groupCallUsers[0]
                //globalSignal.sendGlobalSignalAudioCallRequest(groupCallID!, groupCallName: groupCallName!, users: groupCallUsers)
                videoConnection.getSIPGroupCallToken(groupCallID!, name: groupCallName!, completion: startSIPGroupCall)
                videoConnection.getVideoGroupCallToken(groupCallID!, name: groupCallName!, completion: startVideoGroupCall)
            }
            else if callType == GroupCallType.groupCall {
                
                videoConnection.getVideoGroupCallToken(groupCallID!, name: groupCallName!, completion: startVideoGroupCall)
                globalSignal.sendGlobalSignalVideoCallRequest(groupCallID!, groupCallName: groupCallName!, users: groupCallUsers)
            }
            else if callType == GroupCallType.presentationCall {

                lblPresentation.isHidden = false
                if delegate!.isRemoteCall() == false {
                    
                    delegate?.setPresentaCall(true)
                    lblPresentation.text = "Presenter"
                }

                presenterConnection.getPresenterGroupCallToken(groupCallID!, name: groupCallName!, completion: startPresenterGroupCall)
                globalSignal.sendGlobalSignalPresenterCallRequest(groupCallID!, groupCallName: groupCallName!, users: groupCallUsers)
            }
        }
        
        groupCallUsers.append(userList.getLoginUserID())
        self.runOneSession = false
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func endCallButtonPressed(_ button: UIButton) {
    
        for user: User in userList.getUserList() {
            
            user.callState = UserCallState.none
        }

        endCallNow()
    }

    @IBAction func questionButtonPressed(_ button: UIButton) {

        questionText = ""
        let alert = UIAlertController(title: "Enter Question for Lead", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addTextField(configurationHandler: self.questionTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:self.handleCancel))
        alert.addAction(UIAlertAction(title: "Send", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            
            if self.txtFieldQuestion.text?.characters.count == 0 {
                
                return
            }

            self.questionText = self.txtFieldQuestion.text!
            globalSignal.sendGlobalSignalLeaderQuestion(currentGroupCallID, leader: currentGroupCallLeadUiD, text: self.questionText)
        }))
        self.present(alert, animated: true, completion: {
            
            //print("completion block")
        })
    }
    
    @IBAction func muteButtonPressed(_ button: UIButton) {
    
        // Sesssion might not be set up yet (publishe/subscriber)
        if publisher == nil {
            
            return
        }
        
        // mute button OFF (tag = 1, then mute ON)
        if (button.tag == 0) {
            
            globalSignal.sendGlobalSignalClientMute(userList.getLoginUserID())
            
            publisher!.publishAudio = false
            
            button.setImage(UIImage(named: "button_image_mute_off"), for: UIControlState())
            button.tag = 1
        }
        else {
            
            globalSignal.sendGlobalSignalClientUnMute(userList.getLoginUserID())
            
            publisher!.publishAudio = true
            
            button.setImage(UIImage(named: "button_image_mute_on"), for: UIControlState())
            button.tag = 0
        }
    }
    
    @IBAction func videoStateButtonPressed(_ button: UIButton) {
    
        // Sesssion might not be set up yet (publishe/subscriber)
        if publisher == nil {
            
            return
        }
        
        // video button OFF (tag = 1, then video ON)
        if (button.tag == 0) {
            
            publisher!.publishVideo = false
            
            button.setImage(UIImage(named: "button_image_video_off"), for: UIControlState())
            button.tag = 1
        }
        else {
            
            publisher!.publishVideo = true
            
            button.setImage(UIImage(named: "button_image_video_on"), for: UIControlState())
            button.tag = 0
        }

    }

    @IBAction func stopSharingDocumentButtonPressed(_ button: UIButton) {
    
        // stop document sharing
        webViewDocumentSharing.isHidden = true
        btnStopDocumentSharing.isHidden = true

        let url = URL(string: "about:blank")
        webViewDocumentSharing.loadRequest(URLRequest(url: url!));
        
//        self.view.addSubview((currentSubscriberVideoView?.view)!)
        
        var tempVideoCapture = publisher?.videoCapture
        
        publisher!.videoCapture = oldVideoCapture!
        publisher!.videoType = .camera
        
        view.addSubview(publisher!.view)
        self.view.bringSubview(toFront: publisher!.view)
        
        tempVideoCapture?.stop()
        tempVideoCapture = nil
        
        delegate!.stopDocumentSharingCall()
        
        isDocumentSharing = false
    }

    func incomingClientChangeMute(_ notification: Notification) {
        
        let userInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let uID = userInfo["uID"] as! String
        print("user ID to Mute = " + uID)
        
        if userList.getLoginUserID() == uID {
            
            publisher?.publishAudio = false
            userList.setUserMuteState(uID, mute: true)
            btnMute.setImage(UIImage(named: "button_image_mute_off"), for: UIControlState())
            btnMute.tag = 1
        }
    }

    func incomingClientChangeUnMute(_ notification: Notification) {
        
        let userInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let uID = userInfo["uID"] as! String
        print("user ID to Unmute = " + uID)
        
        if userList.getLoginUserID() == uID {

            publisher?.publishAudio = true
            userList.setUserMuteState(uID, mute: false)
            btnMute.setImage(UIImage(named: "button_image_mute_on"), for: UIControlState())
            btnMute.tag = 0
        }
    }
    
    // MARK: - Public Methods

    func switchVideoToUser(_ uID: String) {
        
        if callType == .audioCall || isDocumentSharing == true || uID == userList.userLoggedIn?.uID {
            
            return
        }

        print("current Subscriber uID: ", currentSubscriberVideoView?.stream!.name! as Any)
        
        if currentSubscriberVideoView?.stream!.name != uID && callType != .audioCall {
            
            currentSubscriberVideoView?.view.removeFromSuperview()
        }

        for subscriber: OTSubscriber in subscribers {
            
            if subscriber.stream!.name == uID {
                
                let view = subscriber.view
                    
                view.frame =  self.view.frame
                
                currentSubscriberVideoView = subscriber

                if currentSubscriberVideoView?.stream!.hasVideo == false {
                    
                    let imageView = UIImageView(image: UIImage(named: "silhouette"))
                    imageView.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
                    self.view.addSubview(imageView)
                }
                else {
                    
                    self.view.addSubview(view)
                }
                
                self.view.bringSubview(toFront: publisher!.view)
                self.view.bringSubview(toFront: vwButtons)
                self.view.bringSubview(toFront: lblPresentation)
            }
        }
    }

    func shareDocument(_ dID: String) {

        webViewDocumentSharing.isHidden = false

        let document = documentManager.getDocumentByID(dID)
        let urlwithPercentEscapes = document!.path.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        webViewDocumentSharing.loadRequest(URLRequest(url: URL(string: urlwithPercentEscapes!)!))
        webViewDocumentSharing.scalesPageToFit = true

        view.bringSubview(toFront: webViewDocumentSharing)

//        currentSubscriberVideoView?.view.removeFromSuperview()
        
        oldVideoCapture = publisher?.videoCapture

        btnStopDocumentSharing.isHidden = false
        view.bringSubview(toFront: btnStopDocumentSharing)

        publisher!.videoCapture = DocumentViewVideoStream.init(view: webViewDocumentSharing)
        publisher!.videoType = .screen
        
        isDocumentSharing = true

        globalSignal.sendGlobalSignalScreenShareNotification(groupCallID!, fileName: document!.name, users: groupCallUsers)
    }

    func endCallNow() {
 
        groupCallList.endGroupCall(groupCallID!, completion: {_ in })
        
        // If not user that started group call then can not disconnect session.
        // Just kill you publish and subscription
        if delegate!.isRemoteCall() == true {
            
            //doUnsubscribe(userList.getLoginUserID())
            doUnPublish()
            
            //print("session count = %d", session?.streams.count)
            if session?.streams.count == 1 {
                
                var error : OTError?
                session?.disconnect(&error)
                if (error != nil) {
                    
                    print("Disconnect group call error ", error!)
                } else {
                    
                    print("Disconnect group call success")
                }
                
                session = nil
            }
        }
        else {
            
            // NOTE(tsd): Don't we want to unpublish before we disconnect??
            var error : OTError?
            session?.disconnect(&error)
            if (error != nil) {
                
                print("Disconnect group call error ", error!)
            } else {
                
                print("Disconnect group call success")
            }
            
            session = nil
        }
        
        let status = UserDefaults.standard.object(forKey: "userAvaialble")
        if status == nil || (status as! Bool) == true {
            userList.setUserStatus(UserOnlineStatus.onlineAvailable)
        }
        
        delegate!.callEndedButtonPressed()
    }
    
    func addUserToCallAudio(_ uID: String) {
        
        //globalSignal.sendGlobalSignalAudioCallRequest(groupCallID!, groupCallName: groupCallName!, users: groupCallUsers)
        videoConnection.getSIPGroupCallToken(groupCallID!, name: groupCallName!, completion: startSIPGroupCall)
        audioCallUid = uID
    }
    
    func addUserToCallVideo(_ uID: String) {
        
        groupCallUsers.append(uID)
        globalSignal.sendGlobalSignalVideoCallRequest(groupCallID!, groupCallName: groupCallName!, users: groupCallUsers)
    }

    // MARK: - Local Completion Methods
    
    func startVideoGroupCall(_ result: String) {
        
        if (result == "success") {
            
            print("Connecting to session " + videoConnection.groupCallSessionID!)
            
            session = OTSession(apiKey: ApiKey, sessionId: videoConnection.groupCallSessionID!, delegate: self)
            
            doVideoConnect()
        }
    }

    func startPresenterGroupCall(_ result: String) {
        
        if (result == "success") {
            
            print("Connecting to session " + presenterConnection.groupCallSessionID!)
            
            session = OTSession(apiKey: ApiKey, sessionId: presenterConnection.groupCallSessionID!, delegate: self)
            
            doPresenterConnect()
        }
    }

    func startSIPGroupCall(_ result: String) {
        
        if (result == "success") {
            
            if callType == .audioCall {
                
                // INVITE sip:+15108675309@{example}.pstn.us1.twilio.com SIP/2.0
                // NOTE(tsd): Make connection secure

                let user = userList.getUser(audioCallUid)
                let sipUri = "sip:+1" +  (user?.phoneNumber)! + "@mslondemand.pstn.twilio.com"

                let parameters = [
                    "sessionId": videoConnection.groupCallSessionID!,
                    "token": videoConnection.groupSIPCallToken!,
                    "sip": [
                        "uri": sipUri,
                        "auth": [
                            "username": "mslondemandaxonrx2",
                            "password": "Axon1234Axon1234",
                        ],
                    ],
                    "secure": "false"
                    ] as [String : Any]

                let urlString =  "https://api.opentok.com/v2/project/45103162/dial"

                let currentTime = Int(Date().timeIntervalSince1970)
                let expireTime = Int(Date().addingTimeInterval(4.0 * 60.0).timeIntervalSince1970)
                
                let uuid = UUID().uuidString

                let iat = String(format: "%d", currentTime)
                let exp = String(format: "%d", expireTime)

                let jwtAuth = JWT.encode(["iss": "45103162", "ist": "project", "iat": iat, "exp": exp, "jti": uuid], algorithm: .hs256("505365a06e67ef745c00665f74ac0c2d404a6add".data(using: .utf8)!))

                //print(jwtAuth)

                let url = URL(string: urlString)!
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = "POST"

                do {

                    urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                } catch {
                    // No-op
                }
                
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                urlRequest.setValue(jwtAuth, forHTTPHeaderField: "X-OPENTOK-AUTH")
                
                Alamofire.request(urlRequest).responseJSON { (response:DataResponse<Any>) in
                    
                    print(response.response!)
                    
                    switch(response.result) {
                    case .success(_):
                        
                        if response.result.value != nil {
                            
                            print(response.result.value!)
                        }
                        break
                        
                    case .failure(_):
                        print(response.request!)
                        print(response.response!)
                        print(response.result.error!)
                        break
                    }
                }
            }
        }
    }
    
    func joinEmailLinkCall(_ result: String, meetingInfo: [String:AnyObject]) {
        
        if (result == "success") {
            
            isEmailLinkGroupCall = false
            
            if delegate!.isRemoteCall() == true {
                
                var remoteGroupCallInfo = delegate!.getRemoteCallInfo()
                remoteGroupCallInfo!["groupCallID"] = meetingInfo["meetingID"] as! String as AnyObject?
                remoteGroupCallInfo!["meetingTime"] = meetingInfo["meetingTime"] as! String as AnyObject?
                remoteGroupCallInfo!["uID"] = meetingInfo["uID"] as! String as AnyObject?
                remoteGroupCallInfo!["token"] = meetingInfo["token"] as! String as AnyObject?
                
                let userList = meetingInfo["users"]
                print(userList!)
                let guestList = meetingInfo["guests"]
                print(guestList!)
            }
            
            print("Connecting to session " + videoConnection.groupCallSessionID!)
            
            session = OTSession(apiKey: ApiKey, sessionId: videoConnection.groupCallSessionID!, delegate: self)
            
            doVideoConnect()
        }
        else {
            
            let alert = UIAlertController(title: "Email Link Error", message: result, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    // MARK: - Class Methods
    
    func questionTextField(_ textField: UITextField!) {
        
        if questionText == "" {
            
            textField.placeholder = "Enter Question"
        }
        else {
            
            textField.placeholder = questionText
        }
        
        txtFieldQuestion = textField
    }

    func handleCancel(_ alertView: UIAlertAction!) {
        
    }

    // MARK: - Notification Methods

    func applicationDidBecomeActiveNotification() {
        
        if let publisher = self.publisher {
            
            // NOTE(tsd): Should check if muted here
            
            publisher.publishAudio = true
            publisher.publishVideo = true
        }

        for subscriber: OTSubscriber in subscribers {
            
            subscriber.subscribeToAudio = true
            subscriber.subscribeToVideo = true
        }
    }
    
    func applicationDidEnterBackgroundNotification() {
        
        if let publisher = self.publisher {
            
            // NOTE(tsd): Should check if muted here
            
            publisher.publishAudio = true
            publisher.publishVideo = false
        }
        
        for subscriber: OTSubscriber in subscribers {
            
            subscriber.subscribeToAudio = true
            subscriber.subscribeToVideo = false
        }
    }

    func screenShareNotification(_ notification: Notification) {
/*
        let userInfo = notification.userInfo as! Dictionary<String,AnyObject>
        let groupCallName = userInfo["fileName"] as! String
        
        let alert = UIAlertController(title: "Screen Share", message: groupCallName, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
*/
    }

    func presenterDisconnectUserGroupCallNotification(_ notification: Notification) {
        
        let userInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let uID = userInfo["uID"] as! String
        print("user ID to disconnect from session = " + uID)
        
        if userList.getLoginUserID() == uID {
            
            //doUnsubscribe(userList.getLoginUserID())
            doUnPublish()
            
            //print("session count = %d", session?.streams.count)
            
            var error : OTError?
            session?.disconnect(&error)
            if (error != nil) {
                
                print("Disconnect group call error ", error!)
            } else {
                
                print("Disconnect group call success")
            }
            
            session = nil
        }
    }

    func presenterSwitchVideoToUserGroupCallNotification(_ notification: Notification) {
        
        let userInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let uID = userInfo["uID"] as! String
        print("user ID to switch video to = " + uID)
        
        switchVideoToUser(uID)
    }

    func updateCallBaudwidthStatusNotification(_ notification: Notification) {
        
        let callNetworkStatus = notification.userInfo as! Dictionary<String,AnyObject>

        let level = callNetworkStatus["level"] as! Int
        
        if level == 0 {

            imgViewNetworkStatus.image = UIImage(named: "ns_lvl_0")

            let status = callNetworkStatus["status"] as! String
            
            lblAudioOnlyCall.isHidden = false
            lblAudioOnlyCall.textColor = UIColor.red
            lblAudioOnlyCall.text = status
            view.bringSubview(toFront: lblAudioOnlyCall)
            
            let delayTime = DispatchTime.now() + Double(Int64(15 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                self.lblAudioOnlyCall.isHidden = true
                self.lblAudioOnlyCall.textColor = UIColor.black
                self.lblAudioOnlyCall.text = "Audio Only Call"
            }
        }
        else if level == 1 {
            
            imgViewNetworkStatus.image = UIImage(named: "ns_lvl_1")
        }
        else if level == 2 {
            
            imgViewNetworkStatus.image = UIImage(named: "ns_lvl_2")
        }
        else if level == 3 {
            
            imgViewNetworkStatus.image = UIImage(named: "ns_lvl_3")
        }
        else if level == 4 {
            
            imgViewNetworkStatus.image = UIImage(named: "ns_lvl_4")
        }
        else if level == 5 {
            
            imgViewNetworkStatus.image = UIImage(named: "ns_lvl_5")
        }

        view.bringSubview(toFront: imgViewNetworkStatus)
        
        let delayTime = DispatchTime.now() + Double(Int64(10 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            
            self.checkQualityForSession()
        }
    }

    // MARK: - OpenTok Methods
    
    /**
     * Asynchronously begins the session connect process. Some time later, we will
     * expect a delegate method to call us back with the results of this action.
     */
    func doVideoConnect() {
        
        if let session = self.session {
            
            var maybeError : OTError?
            session.connect(withToken: videoConnection.groupCallToken!, error: &maybeError)
            if let error = maybeError {
                
                print("doVideoConnect(): " + error.localizedDescription)
            }
        }
    }
    
    func doPresenterConnect() {
        
        if let session = self.session {
            
            var maybeError : OTError?
            session.connect(withToken: presenterConnection.groupCallToken!, error: &maybeError)
            if let error = maybeError {
                
                print("doPresenterConnect(): " + error.localizedDescription)
            }
        }
    }
    
    /**
     * Sets up an instance of OTPublisher to use with this session. OTPubilsher
     * binds to the device camera and microphone, and will provide A/V streams
     * to the OpenTok session.
     */
    func doPublish() {

        if callType == .audioCall {
            
            publisher = OTPublisher(delegate: self, name: userList.getLoginUserID(), audioTrack: true, videoTrack: false)
        }
        else {

            /*
             OTCameraCaptureResolutionLow = 0
             OTCameraCaptureResolutionMedium = 1
             High = 2

             Rate30FPS = 0
             Rate15FPS = 1
             Rate7FPS = 2
             Rate1FPS = 3
             */
            
            publisher = OTPublisher(delegate: self, name: userList.getLoginUserID(), cameraResolution: OTCameraCaptureResolution.init(rawValue: 1)! /*OTCameraCaptureResolutionMedium*/, cameraFrameRate: OTCameraCaptureFrameRate.init(rawValue: 1)! /*OTCameraCaptureFrameRate15FPS*/)
        }
        
        var maybeError : OTError?
        session?.publish(publisher!, error: &maybeError)
        
        if let error = maybeError {
            
            print("doPublish(): " + error.localizedDescription)
        }
        
        if callType != .audioCall {
            // On phone shrink the mirror view
            if (UIDevice.current.model == "iPhone") {
                let width:CGFloat = 120
                let height:CGFloat = 160
                let edgeX = vwUsersVideoView.frame.origin.x + vwUsersVideoView.frame.width
                let edgeY = vwUsersVideoView.frame.origin.y + vwUsersVideoView.frame.height
                publisher!.view.frame = CGRect(x:edgeX-width, y:edgeY-height, width:width, height:height)
            }
            else {
                publisher!.view.frame = vwUsersVideoView.frame
            }
            view.addSubview(publisher!.view)
        }
    }
    
    /**
     * Instantiates a subscriber for the given stream and asynchronously begins the
     * process to begin receiving A/V content for this stream. Unlike doPublish,
     * this method does not add the subscriber to the view hierarchy. Instead, we
     * add the subscriber only after it has connected and begins receiving data.
     */
    func doSubscribe(_ stream : OTStream) {
        
        if let session = self.session {
            
            let subscriber = OTSubscriber(stream: stream, delegate: self)
            subscriber.networkStatsDelegate = self
            subscribers.append(subscriber)
            
            var maybeError : OTError?
            session.subscribe(subscriber, error: &maybeError)
            if let error = maybeError {
                
                print("doSubscribe(): " + error.localizedDescription)
            }
        }
    }
    
    /**
     * Cleans the subscriber from the view hierarchy, if any.
     */
    func doUnsubscribe(_ uID: String) {
        
        for subscriber: OTSubscriber in subscribers {
            
            if uID == subscriber.stream!.name {
                
                var maybeError : OTError?
                session?.unsubscribe(subscriber, error: &maybeError)
                if let error = maybeError {
                    
                    print("doUnsubscribe(): " + error.localizedDescription)
                }
                
                // Only remove if user is in the video view. If not, main user then just drop view.
                // Then the local user can push another user thumbnail to get there video next
                if currentSubscriberVideoView == subscriber && callType != .audioCall {
                    
                    subscriber.view.removeFromSuperview()
                }
            }
        }
    }
    
    /**
     * Cleans the publisher from the view hierarchy, if any.
     */
    func doUnPublish() {
        
        var maybeError : OTError?
        session?.unpublish(publisher!, error: &maybeError)
        if let error = maybeError {
            
            //showAlert(error.localizedDescription)
            print("doUnPublish(): " + error.localizedDescription)
        }
        
        if publisher == nil {
            
            return
        }

        if callType != .audioCall {
            
            publisher!.view.removeFromSuperview()
        }
        
        self.publisher = nil
    }
    
    // MARK: - Helpers
    
    func showAlert(_ message: String) {
        
        // show alertview on main UI
        DispatchQueue.main.async {
            
            //_ = UIAlertView(title: "OTError", message: message, delegate: nil, cancelButtonTitle: "OK")
            let alert = UIAlertController(title: "OTError", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func changeMuteUser(_ uID: String, muteUser: Bool) {
        
        if muteUser == true {
            
            for subscriber: OTSubscriber in subscribers {
                
                //print("trying to mute " + uID + " found stream name " + subscriber.stream.name)
                if subscriber.stream!.name == uID {
                    
                    //imgView.image = UIImage(named: "video_view_mute_on")
                    subscriber.subscribeToAudio = false
                }
            }
        }
        else {
            
            for subscriber: OTSubscriber in subscribers {
                
                //print("trying to unmute " + uID + " found stream name " + subscriber.stream.name)
                if subscriber.stream!.name == uID {
                    
                    //imgView.image = UIImage(named: "video_view_mute_off")
                    subscriber.subscribeToAudio = true
                }
            }
        }
    }

}

extension VideoViewController: OTPublisherDelegate {
    
    func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
        
        print("publisher streamCreated ", stream)
        print("publisher name = " + publisher.name!)

        delegate!.userConnectedToStream(publisher.name!)
    }
    
    func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
        
        print("publisher streamDestroyed ", stream)
        
        doUnsubscribe(stream.name!)
    }
    
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        
        print("publisher didFailWithError ", error)
    }
}

extension VideoViewController: OTSubscriberKitDelegate {
    
    func subscriberDidConnect(toStream subscriberKit: OTSubscriberKit) {
        
        print("subscriberDidConnectToStream (\(subscriberKit))")
        print("Connecting to Stream (showing in main video view) =  " + subscriberKit.stream!.name!)
        
        if publisher == nil {
            
            return
        }

        delegate!.userConnectedToStream(subscriberKit.stream!.name!)

        var groupCalluID: String?
        if delegate!.isRemoteCall() == true {

            let remoteGroupCallInfo = delegate!.getRemoteCallInfo()
            groupCalluID = remoteGroupCallInfo!["uID"] as? String
        }
        else {

            let groupCall = groupCallList.getGroupCallDetails(selectedGroupCallID)
            groupCalluID = groupCall!.users[0]
        }
        
        if callType != .audioCall {
            
            // Start the view to the Group Call originator, but if only 2 people then show both people
            // too each other
            for subscriber: OTSubscriber in subscribers {
                
                if subscriber.stream!.name == groupCalluID {
                    
                    let view = subscriber.view
                    
                    view.frame =  self.view.frame

                    currentSubscriberVideoView = subscriber

                    if currentSubscriberVideoView?.stream!.hasVideo == false {
                        
                        let imageView = UIImageView(image: UIImage(named: "silhouette"))
                        imageView.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
                        self.view.insertSubview(imageView, at: 50)
                    }
                    else {
                        
                        //self.view.addSubview(view)
                        self.view.insertSubview(view, at: 50)
                    }

                    self.view.bringSubview(toFront: publisher!.view)
                    self.view.bringSubview(toFront: vwButtons)
                    self.view.bringSubview(toFront: lblPresentation)
                }
            }
        }

        let delayTime = DispatchTime.now() + Double(Int64(10 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {

            if self.runOneSession == false {
                
                self.runOneSession = true
                self.checkQualityForSession()
            }
        }
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error : OTError) {
        
        print("subscriber " + subscriber.stream!.streamId + " didFailWithError ", error)
    }
    
    func subscriber(_ subscriberKit: OTSubscriberKit, audioLevel: Float) {
        
        print("Audio level for stream has changed =  " + subscriberKit.stream!.name!)

        changeMuteUser(subscriberKit.stream!.name!, muteUser: Bool(NSNumber(value:audioLevel)))
    }
    
    func subscriberVideoDisabled(_ subscriber: OTSubscriberKit, reason: OTSubscriberVideoEventReason) {
        
        print("Display the video disabled indicator")
        //print("subscriber " + subscriber.stream.name)
        //print("currentSubscriberVideoView " + (currentSubscriberVideoView?.stream.name)!)
        
        if subscriber.stream!.name == currentSubscriberVideoView?.stream!.name {
            
            let imageView = UIImageView(image: UIImage(named: "silhouette"))
            imageView.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            imageView.clipsToBounds = true
            imageView.contentMode = UIViewContentMode.scaleAspectFill
            self.view.addSubview(imageView)

            if publisher != nil {
                
                self.view.bringSubview(toFront: publisher!.view)
            }
            self.view.bringSubview(toFront: vwButtons)
            self.view.bringSubview(toFront: lblPresentation)
        }
    }
    
    func subscriberVideoEnabled(_ subscriber: OTSubscriberKit, reason: OTSubscriberVideoEventReason) {
        
        print("Remove the video enabled indicator")
        //print("subscriber " + subscriber.stream.name)
        //print("currentSubscriberVideoView " + (currentSubscriberVideoView?.stream.name)!)

        if subscriber.stream!.name == currentSubscriberVideoView?.stream!.name {
            
            let view = currentSubscriberVideoView!.view
                
            view.frame =  self.view.frame
            
            self.view.addSubview(view)
            
            if publisher != nil {
                
                self.view.bringSubview(toFront: publisher!.view)
            }
            self.view.bringSubview(toFront: vwButtons)
            self.view.bringSubview(toFront: lblPresentation)
        }
    }

    // MARK: - Local Methods

    func checkQualityForSession() {
        
        var callNetworkStatus: Dictionary<String, AnyObject>?

        var canDoVideo = (video_bw >= 150000 && video_pl_ratio <= 0.03)
        var canDoAudio = (audio_bw >= 25000 && audio_pl_ratio <= 0.05)

        // See if user muted audio or turned off video
        if audio_pl_ratio == -1 {
            
            canDoAudio = true
        }

        if video_pl_ratio == -1 {
            
            canDoVideo = true
        }

        if (!canDoVideo && !canDoAudio)
        {
            //print("Starting Audio Only Test");
            callNetworkStatus = ["status": "Turn off Video Stream because not enough network baudwidth at this time." as AnyObject, "level":3 as AnyObject]

            // test for audio only stream
            publisher?.publishVideo = false

            let delayTime = DispatchTime.now() + Double(Int64(6 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {

                // you can tune audio bw threshold value based on your needs.
                if (self.audio_bw >= 25000 && self.audio_pl_ratio <= 0.05) {
                    
                    //callNetworkStatus = ["status": "The quality of your network is medium you can start a voice only call."]
                    callNetworkStatus = ["status": "The quality of your network is medium. You can have a audio only call." as AnyObject, "level":2 as AnyObject]
                } else {
                    
                    //result = OTNetworkTestResult.OTNetworkTestResultNotGood;
                    callNetworkStatus = ["status": "The quality of your network is not enough to start a video or audio call, please try it again later or connect to another network." as AnyObject, "level":0 as AnyObject]
                }

                //DispatchQueue.main.async {
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: updateCallBaudwidthStatusNotificationKey), object: self, userInfo: callNetworkStatus)
                //}
            }
        }
        else if (canDoAudio && !canDoVideo) {

            callNetworkStatus = ["status": "The quality of your network is medium. Recommend voice only call." as AnyObject, "level":3 as AnyObject]
        } else {

            callNetworkStatus = ["status": "The quality of your network is good for video and audio call." as AnyObject, "level":5 as AnyObject]
        }

        //print(callNetworkStatus);

        DispatchQueue.main.async {
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: updateCallBaudwidthStatusNotificationKey), object: self, userInfo: callNetworkStatus)
        }
    }

}

extension VideoViewController: OTSessionDelegate {
    
    func sessionDidConnect(_ session: OTSession) {
        
        print("sessionDidConnect (\(session.sessionId))")
        
        doPublish()
    }
    
    func sessionDidDisconnect(_ session : OTSession) {
        
        print("Session disconnected (\( session.sessionId))")
        
        currentSubscriberVideoView?.view.removeFromSuperview()
        publisher?.view.removeFromSuperview()
        
        for subscriber: OTSubscriber in subscribers {
            
            var maybeError : OTError?
            session.unsubscribe(subscriber, error: &maybeError)
            if let error = maybeError {
                
                print("doUnsubscribe(): " + error.localizedDescription)
            }
        }
        
        if (UserDefaults.standard.object(forKey: "userAvaialble") as! Bool) == true {
            
            userList.setUserStatus(UserOnlineStatus.onlineAvailable)
        }
        
        self.dismiss(animated: true, completion: {})
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        
        print("session streamCreated (\(stream.streamId))")
        print("stream name (from publisher) = " + stream.name!)

        doSubscribe(stream)
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        
        print("session streamDestroyed (\(stream.streamId))")
        
        if (UserDefaults.standard.object(forKey: "userAvaialble") as! Bool) == true {
            
            userList.setUserStatus(UserOnlineStatus.onlineAvailable)
        }

        delegate!.userDisconnectedFromStream(stream.name!)

        if groupCallUiD == stream.name {
            
            // Close all subscribers and disconnect session
            for subscriber: OTSubscriber in subscribers {
                
                doUnsubscribe(subscriber.stream!.name!)
            }

            doUnPublish()
            
            var error : OTError?
            self.session!.disconnect(&error)
            if (error != nil) {
                
                print("Disconnect group call error ", error!)
            } else {
                
                print("Disconnect group call success")
            }
            
            self.session = nil
            
            let alert = UIAlertController(title: "Group Call", message: "The call was ended by the Group Leader", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style {
                case .default:
                    self.dismiss(animated: true, completion: {})
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func session(_ session: OTSession, connectionCreated connection : OTConnection) {
        
        print("session connectionCreated (\(connection.connectionId))")
    }
    
    func session(_ session: OTSession, connectionDestroyed connection : OTConnection) {
        
        print("session connectionDestroyed (\(connection.connectionId))")
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        
        print("session didFailWithError (%@)", error)
    }
    
}

extension VideoViewController: OTSubscriberKitNetworkStatsDelegate {

    func subscriber(_ subscriber: OTSubscriberKit, videoNetworkStatsUpdated stats : OTSubscriberKitVideoNetworkStats) {
        
        if prevVideoTimestamp == 0 {
            
            prevVideoTimestamp = stats.timestamp
            prevVideoBytes = stats.videoBytesReceived
        }
        
        if stats.timestamp - prevVideoTimestamp >= TIME_WINDOW
        {
            // swift couldn't handle as one line - Swift UInt64.init checks to make sure correct sign and size. If not, throws exception
            //video_bw = (8 * (stats.videoBytesReceived - prevVideoBytes)) / (UInt64(stats.timestamp - prevVideoTimestamp) / 1000)
            let denominator = Int64((stats.timestamp - prevVideoTimestamp) / 1000)
            let one: Int64 = Int64(stats.videoBytesReceived)
            let two: Int64 = Int64(prevVideoBytes)
            var nominator: Int64 = one - two
            if nominator < 0 {
                
                nominator = nominator * -1
            }
            video_bw = UInt64(8 * nominator / denominator)

            processStats(stats)
            prevVideoTimestamp = stats.timestamp
            prevVideoBytes = stats.videoBytesReceived
            //print("videoBytesReceived ", stats.videoBytesReceived, ", bps ", video_bw, ", packetsLost ", video_pl_ratio)
        }
    }

    func subscriber(_ subscriber: OTSubscriberKit, audioNetworkStatsUpdated stats : OTSubscriberKitAudioNetworkStats) {
        
        if prevAudioTimestamp == 0 {

            prevAudioTimestamp = stats.timestamp
            prevAudioBytes = stats.audioBytesReceived
        }
        
        if stats.timestamp - prevAudioTimestamp >= TIME_WINDOW {

            // swift couldn't handle as one line - Swift UInt64.init checks to make sure correct sign and size. If not, throws exception
            //audio_bw = (8 * (stats.audioBytesReceived - prevAudioBytes)) / (UInt64(stats.timestamp - prevAudioTimestamp) / 1000)
            let denominator = Int64((stats.timestamp - prevAudioTimestamp) / 1000)
            let one: Int64 = Int64(stats.audioBytesReceived)
            let two: Int64 = Int64(prevAudioBytes)
            var nominator: Int64 = one - two
            if nominator < 0 {
                
                nominator = nominator * -1
            }
            audio_bw = UInt64(8 * nominator / denominator)

            processStats(stats)
            prevAudioTimestamp = stats.timestamp
            prevAudioBytes = stats.audioBytesReceived
            //print("audioBytesReceived ", stats.audioBytesReceived, ", bps ", audio_bw, ", packetsLost ", audio_pl_ratio)
        }
    }

    func processStats(_ stats: AnyObject) {

        if stats is OTSubscriberKitVideoNetworkStats {

            video_pl_ratio = -1
            let videoStats = stats as! OTSubscriberKitVideoNetworkStats
            if prevVideoPacketsRcvd != 0 {
                
                // swift couldn't handle as one line - Swift UInt64.init checks to make sure correct sign and size. If not, throws exception
                //var pl: Int64 = Int64(videoStats.videoPacketsLost - prevVideoPacketsLost)
                let one: Int64 = Int64(videoStats.videoPacketsLost)
                let two: Int64 = Int64(prevVideoPacketsLost)
                var pl: Int64 = one - two
                if pl < 0 {
                    
                    pl = pl * -1
                }
                //var pr: Int64 = Int64(videoStats.videoPacketsReceived - prevVideoPacketsRcvd)
                let three: Int64 = Int64(videoStats.videoPacketsReceived)
                let four: Int64 = Int64(prevVideoPacketsRcvd)
                var pr: Int64 = three - four
                if pr < 0 {
                    
                    pr = pr * -1
                }
                let pt = pl + pr
                if pt > 0 {
                    
                    video_pl_ratio = Double(pl / pt)
                }
            }
            prevVideoPacketsLost = videoStats.videoPacketsLost
            prevVideoPacketsRcvd = videoStats.videoPacketsReceived
        }

        if stats is OTSubscriberKitAudioNetworkStats {

            audio_pl_ratio = -1
            let audioStats = stats as! OTSubscriberKitAudioNetworkStats
            if (prevAudioPacketsRcvd != 0) {
                
                // swift couldn't handle as one line - Swift UInt64.init checks to make sure correct sign and size. If not, throws exception
                //var pl: Int64 = Int64(audioStats.audioPacketsLost - prevAudioPacketsLost)
                let one: Int64 = Int64(audioStats.audioPacketsLost)
                let two: Int64 = Int64(prevAudioPacketsLost)
                var pl: Int64 = one - two
                if pl < 0 {
                    
                    pl = pl * -1
                }
                let three: Int64 = Int64(audioStats.audioPacketsReceived)
                let four: Int64 = Int64(prevAudioPacketsRcvd)
                var pr: Int64 = three - four
                if pr < 0 {
                    
                    pr = pr * -1
                }
                let pt = pl + pr
                if pt > 0 {
                    
                    audio_pl_ratio = Double(pl / pt)
                }
            }
            prevAudioPacketsLost = audioStats.audioPacketsLost
            prevAudioPacketsRcvd = audioStats.audioPacketsReceived
        }
    }

}
