//
//  MainViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 6/29/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class MainViewController: UIViewController {

    @IBOutlet weak var imgViewAvatar: UIImageView!
    //@IBOutlet weak var lblUserInfo: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var imgViewBrandLogo: UIImageView!
    @IBOutlet weak var imgBackground: UIImageView!
    //@IBOutlet weak var lblProgramLocation: UILabel!
    
    @IBOutlet weak var availSwitch: UISwitch!
    @IBOutlet weak var vwTitleView: UIView!
    @IBOutlet weak var vwCalendar: UIView!
    //@IBOutlet weak var vwHistory: UIView!
    
    var screenMode = 0

    var contactsViewController: ContactsViewController?
    var historyViewController: HistoryViewController?
    var calendarViewController: MainCalendarViewController?
    //var groupTagViewController: GroupsViewController?

    //var isSearchBarSelected = false

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.refeshUserProfileInfoNotification), name: NSNotification.Name(rawValue: incomingrefeshUserProfileInfoNotificationKey), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.refeshCalendarNotification), name: NSNotification.Name(rawValue: incomingCalendarUpdateNotificationKey), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.incomingMeetingLinkCall), name: NSNotification.Name(rawValue: incomingMeetingLinkPressedNotificationKey), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.incomingPresenterGroupCall), name: NSNotification.Name(rawValue: incomingPresenterGroupCallNotificationKey), object: nil)

        lblVersion.text = "Version: " + Bundle.main.releaseVersionNumber! + " Build: " + Bundle.main.buildVersionNumber!

        // Turn off when start application
        availSwitch.setOn(false, animated: false)
        UserDefaults.standard.set(false, forKey: "userAvaialble")
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        userList.getUpdatedUsersList(uploadUserListComplete)

        //NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.keyboardWillShowNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.keyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.incomingVideoGroupCallNotification), name: NSNotification.Name(rawValue: incomingVideoGroupCallNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.incomingAudioGroupCallNotification), name: NSNotification.Name(rawValue: incomingAudioGroupCallNotificationKey), object: nil)

        vwTitleView.layer.borderWidth = 1
        vwTitleView.layer.borderColor = UIColor.darkGray.cgColor

        vwCalendar.layer.borderWidth = 1
        vwCalendar.layer.borderColor = UIColor.darkGray.cgColor

        //vwHistory.layer.borderWidth = 1
        //vwHistory.layer.borderColor = UIColor.darkGray.cgColor

        let orient = UIApplication.shared.statusBarOrientation
        
        switch orient {
            
        case .portrait:
            self.screenMode = 0
            break
        default:
            self.screenMode = 1
            break
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if applicationSetup.getApplicationSetupNetworkComplete() == true {

            setupApplicationLook()
        }
        else {
            
            applicationSetup.getApplicationSetup("666", completion: { (result, code) in
                
                if result.contains("success") {
                    
                    self.setupApplicationLook()
                }
            })
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator);
        
        coordinator.animate(alongsideTransition: nil, completion: {
            _ in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
                
            case .portrait:
                self.screenMode = 0
                break
            default:
                self.screenMode = 1
                break
            }
            
        })
    }

    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)

        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: incomingVideoGroupCallNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: incomingAudioGroupCallNotificationKey), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //print(segue.identifier)
        if let vc = segue.destination as? ContactsViewController, segue.identifier == "ContactsView" {
            
            contactsViewController = vc
            contactsViewController?.mainDelegate = self
            contactsViewController?.turnOffAddToCallButton = true
        }
/*
        if let vc = segue.destination as? HistoryViewController, segue.identifier == "HistoryView" {
            
            historyViewController = vc
        }
*/
        if let vc = segue.destination as? MainCalendarViewController, segue.identifier == "CalendarView" {
            
            calendarViewController = vc
        }
/*
        if let vc = segue.destinationViewController as? GroupsViewController
            where segue.identifier == "GroupsView" {
            
            groupTagViewController = vc
            groupTagViewController!.allowGroupTagAdding = true
        }
*/
        if let vc = segue.destination as? UserProfileViewController, segue.identifier == "UserProfileViewController" {
            
            vc.showSignOutButton = true
        }

    }

    // MARK: - IBActions

    @IBAction func setupNewCallButtonPressed(_ sender: AnyObject) {

        let storyboard = UIStoryboard(name: "CallDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallDetailsViewController") as! CallDetailsViewController
        vc.selectedGroupCallID = String()
        self.present(vc, animated: true, completion: nil)
    }

    @IBAction func documentButtonPressed(_ sender: AnyObject) {
    
        let storyboard = UIStoryboard(name: "Documents", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DocumentsViewController") as! DocumentsViewController
        vc.isSelectFolder = false
        vc.hideAddFolderToCall = true
        self.present(vc, animated: true, completion: nil)
    }

    @IBAction func callLogButtonPressed(_ sender: Any) {
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        historyViewController = storyboard.instantiateViewController(withIdentifier: "HistoryViewController") as? HistoryViewController
        self.present(historyViewController!, animated: true, completion: nil)
    }
    
    @IBAction func avaialbeSwitchChanged(_ sender: Any) {

        if availSwitch.isOn{
            
            UserDefaults.standard.set(true, forKey: "userAvaialble")
            userList.setUserStatus(UserOnlineStatus.onlineAvailable)
        }
        else {
            
            UserDefaults.standard.set(false, forKey: "userAvaialble")
            userList.setUserStatus(UserOnlineStatus.offline)
        }
    }

    // MARK: - Local Completion Methods

    func uploadUserListComplete(_ result: String, code: String) {
        
        if result == "success" {
            
            if (UserDefaults.standard.object(forKey: "userAvaialble") as! Bool) == true {
                
                userList.setUserStatus(UserOnlineStatus.onlineAvailable)
            }

            groupCallList.getUserGroupCalls(userList.getLoginUserID(), completion: updateCallHistoryTableview)
            documentManager.getUserDocuments(userList.getLoginUserID(), completion: retrievedUserDocuments)
            folderManager.getFolders(retrievedUserFolders)
            //groupTagManager.getUserGroupTags(retrievedGroupTags)
            
            let userPicPath = userList.userLoggedIn?.picPath
            //print(userPicPath)
            
            Alamofire.request(userPicPath!)
                .responseImage { response in
                    //debugPrint(response)
                    //print(response.request)
                    //print(response.response)
                    //debugPrint(response.result)
                    
                    if let image = response.result.value {
                        
                        let circularImage = image.af_imageRoundedIntoCircle()
                        self.imgViewAvatar.image = circularImage
                    }
            }

            // Update login user image and user name
            //let user = userList.userLoggedIn
            //lblUserInfo.text = user!.jobTitle + " " + user!.firstName + " " + user!.lastName + ", " + user!.credentials
            //lblProgramLocation.text = user!.programs + ", " + user!.location
            
            contactsViewController?.tblViewContacts.reloadData()
        }
    }

    // MARK: - Local Methods
    
    func setupApplicationLook() {
        
        let imageUrl = applicationSetup.getApplicationLogoUrl()
        if imageUrl != nil {
            
            Alamofire.request(imageUrl!)
                .responseImage { response in
                    
                    if let image = response.result.value {
                        
                        self.imgViewBrandLogo.image = image
                    }
            }
        }
        
        let backgroundUrl = applicationSetup.getApplicationBackgroundUrl()
        if backgroundUrl != nil {
            
            Alamofire.request(backgroundUrl!)
                .responseImage { response in
                    //debugPrint(response)
                    //print(response.request)
                    //print(response.response)
                    //debugPrint(response.result)
                    
                    if let image = response.result.value {
                        
                        //print("image downloaded: \(image)")
                        self.imgBackground.image = image
                    }
                    else {
                        
                        Gradient.setBackgroundGradient(self.view)
                    }
            }
        }
        else {
            
            Gradient.setBackgroundGradient(self.view)
        }
    }

    func updateCallHistoryTableview(_ result: String) {
        
        groupCallList.getAllUserGroupCalls(userList.getLoginUserID(), completion: updateGroupCallsAll)

        if result == "success" {
            
            historyViewController?.tblViewCallHistory.reloadData()
            calendarViewController?.loadEvents()
        }
    }

    func updateGroupCallsAll(_ result: String) {
        
        if result == "success" {
            
            historyViewController?.tblViewCallHistory.reloadData()
            calendarViewController?.loadEvents()
        }
    }

    func allDone(_ result: String) {
        
    }

    func retrievedUserDocuments(_ result: String) {
        
        // NOTE(tsd): How should we handle this issue?
        
    }

    func retrievedUserFolders(_ result: String) {
        
        // NOTE(tsd): How should we handle this issue?
        
    }

    func retrievedGroupTags(_ result: String) {
        
        //groupTagViewController?.reloadGroupTags()
    }

    // MARK: - Notification Methods

    func incomingVideoGroupCallNotification(_ notification: Notification) {
        
        let videoInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let groupCallID = videoInfo["groupCallID"] as! String
        print("GroupCall ID = " + groupCallID)
        let groupCallName = videoInfo["groupCallName"] as! String
        print("GroupCall Name = " + groupCallName)
        let groupCallUserId = videoInfo["uID"] as! String
        print("FROM: " + groupCallUserId)
        //let users = videoInfo["users"] as! Array<String>
        
        let alert = UIAlertController(title: "Video Group Call", message: "Do you want to join group call: " + groupCallName + " from " + userList.getUserName(groupCallUserId), preferredStyle: UIAlertControllerStyle.alert)
        
        let rejectCallAction = UIAlertAction(title: "Reject", style: .cancel) { (action:UIAlertAction!) in

            globalSignal.sendGlobalSignalRejectCall(groupCallID)
        }
        alert.addAction(rejectCallAction)
        
        let OKAction = UIAlertAction(title: "Accept", style: .default) { (action:UIAlertAction!) in
            
            userList.setUserStatus(UserOnlineStatus.onlineOnCall)
            
            print("Accept Video Group Call button pressed")
            var storyboard = UIStoryboard(name: "Call", bundle: nil)
            if self.screenMode == 1 {
                
                storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
            }
            let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = groupCallID
            vc.isPresentation = false
            vc.remoteGroupCallInfo = videoInfo
            vc.callType = .groupCall
            vc.isRemoteRequestForGroupCall = true
            vc.isEmailLinkGroupCall = false
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        
        self.present(alert, animated: true, completion: nil)
    }

    func incomingAudioGroupCallNotification(_ notification: Notification) {
    
        let audioInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let groupCallID = audioInfo["groupCallID"] as! String
        print("GroupCall ID = " + groupCallID)
        let groupCallName = audioInfo["groupCallName"] as! String
        print("GroupCall Name = " + groupCallName)
        let groupCallUserId = audioInfo["uID"] as! String
        print("FROM: " + groupCallUserId)

        let alert = UIAlertController(title: "Audio Group Call", message: "Do you want to join a Audio Group Call: " + groupCallName + " from " + userList.getUserName(groupCallUserId), preferredStyle: UIAlertControllerStyle.alert)
        
        let rejectCallAction = UIAlertAction(title: "Reject", style: .cancel) { (action:UIAlertAction!) in

            globalSignal.sendGlobalSignalRejectCall(groupCallID)
        }
        alert.addAction(rejectCallAction)
        
        let OKAction = UIAlertAction(title: "Accept", style: .default) { (action:UIAlertAction!) in
            
            userList.setUserStatus(UserOnlineStatus.onlineOnCall)
            
            print("Accept Audio Call button pressed")
            var storyboard = UIStoryboard(name: "Call", bundle: nil)
            if self.screenMode == 1 {
                
                storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
            }
            let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = groupCallID
            vc.isPresentation = false
            vc.isRemoteRequestForGroupCall = true
            vc.isEmailLinkGroupCall = false
            vc.remoteGroupCallInfo = audioInfo
            vc.callType = .audioCall
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func refeshUserProfileInfoNotification(_ notification: Notification) {
        
        let userPicPath = userList.userLoggedIn?.picPath
        //print(userPicPath)
    
        Alamofire.request(userPicPath!)
            .responseImage { response in
                //debugPrint(response)
                //print(response.request)
                //print(response.response)
                //debugPrint(response.result)
                
                if let image = response.result.value {
                    
                    //print("image downloaded: \(image)")
                    //let radius: CGFloat = 20.0
                    //let roundedImage = image.af_imageWithRoundedCornerRadius(radius)
                    let circularImage = image.af_imageRoundedIntoCircle()
                    self.imgViewAvatar.image = circularImage
                }
        }

        // Update login user image and user name
        //let user = userList.userLoggedIn
        //lblUserInfo.text = user!.jobTitle + " " + user!.firstName + " " + user!.lastName + ", " + user!.credentials
    }

    func refeshCalendarNotification(_ notification: Notification) {
        
        calendarViewController?.loadEvents()
    }

    func incomingMeetingLinkCall(_ notification: Notification) {
        
        let meetingInfo = notification.userInfo as! [String:String?]
        
        print(meetingInfo)

        var storyboard = UIStoryboard(name: "Call", bundle: nil)
        if self.screenMode == 1 {
            
            storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
        }
        let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
        vc.modalPresentationStyle = .fullScreen
        vc.isPresentation = false
        vc.isRemoteRequestForGroupCall = false
        vc.isEmailLinkGroupCall = true
        vc.remoteGroupCallInfo = meetingInfo as Dictionary<String, AnyObject>?
        vc.callType = .groupCall
        self.present(vc, animated: true, completion: nil)
    }

    func incomingPresenterGroupCall(_ notification: Notification) {
        
        let presenterInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let groupCallID = presenterInfo["groupCallID"] as! String
        print("GroupCall ID = " + groupCallID)
        let groupCallName = presenterInfo["groupCallName"] as! String
        print("GroupCall Name = " + groupCallName)
        let groupCallUserId = presenterInfo["uID"] as! String
        
        let alert = UIAlertController(title: "Presentation Group Call", message: "Do you want to join a Presenters Group Call: " + groupCallName + " from " + userList.getUserName(groupCallUserId), preferredStyle: UIAlertControllerStyle.alert)
        
        let rejectAction = UIAlertAction(title: "Reject", style: .cancel) { (action:UIAlertAction!) in
            
            globalSignal.sendGlobalSignalRejectCall(groupCallID)
        }
        alert.addAction(rejectAction)
        
        let OKAction = UIAlertAction(title: "Accept", style: .default) { (action:UIAlertAction!) in

            userList.setUserStatus(UserOnlineStatus.onlineOnCall)
           
            print("Accept Presentation Group Call button pressed")
            var storyboard = UIStoryboard(name: "Call", bundle: nil)
            if self.screenMode == 1 {
                
                storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
            }
            let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = groupCallID
            vc.remoteGroupCallInfo = presenterInfo
            vc.isPresentation = true
            vc.callType = .presentationCall
            vc.isRemoteRequestForGroupCall = true
            vc.isEmailLinkGroupCall = false
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Keyboard Notifications
    
    func keyboardWillShowNotification(_ notification: Notification) {
        
        //if isSearchBarSelected == true {

        //    return
        //}

        //contactsViewController!.view.hidden = true
        //calendarViewController!.view.hidden = true
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.view.frame.origin.y -= keyboardSize.height
        }
        
    }
    
    func keyboardWillHideNotification(_ notification: Notification) {
        
        //if isSearchBarSelected == true {
            
        //    isSearchBarSelected = false
        //    return
        //}

        //contactsViewController!.view.hidden = false
        //calendarViewController!.view.hidden = false
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.view.frame.origin.y += keyboardSize.height
        }
    }

}

// MARK: - MainViewDelegate Methods

extension MainViewController: MainViewDelegate {
    
    func searchBarSelected(open: Bool) {
        
        //isSearchBarSelected = true
    }
    
    func historyHomeButtonSelected() {
        
    }

}
