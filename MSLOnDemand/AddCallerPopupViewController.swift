//
//  AddCallerPopupViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 12/9/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class AddCallerPopupViewController: UIViewController {

    @IBOutlet weak var contactsContainer: UIView!

    var contactsViewController: ContactsViewController?

    var delegate:CallViewDelegate?

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        contactsViewController = storyboard.instantiateViewController(withIdentifier: "ContactsViewController") as? ContactsViewController
        contactsViewController!.turnOffAddToCallButton = false
        contactsViewController!.isAddCaller = true
        contactsViewController!.delegate = self
        contactsViewController!.view.frame = contactsContainer.bounds
        contactsContainer.addSubview(contactsViewController!.view)
        addChildViewController(contactsViewController!)
        contactsViewController!.didMove(toParentViewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension AddCallerPopupViewController: CallDetailsViewDelegate {
    
    func callCanceledButtonPressed() {
        
    }
    
    func addUserToCallPressed(_ uID: String) {
        
        let user = userList.getUser(uID)
        if user!.userStatus != .onlineAvailable {

            let alert = UIAlertController(title: "Warning", message: "Participate is not online at this time.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in }))
            self.present(alert, animated: true, completion: {})
        }
        else {
            
            delegate?.addUserToCallVideo(uID)
        }
    }
    
    func scheduleCallButtonPressed() {
        
    }
    
    func callNowButtonPressed() {
        
    }
    
    func noteViewSelected(open: Bool) {
        
    }

}
