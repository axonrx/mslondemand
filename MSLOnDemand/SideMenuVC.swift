//
//  SideMenuVC.swift
//  MSLOnDemand
//
//  Created by Kiran Patel on 1/2/16.
//  Copyright © 2016  SOTSYS175. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class SideMenuVC: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var imgBackground: UIImageView!

    var headerView = UIView()
    var headerImageView = UIImageView()
    var headerGreenImageView = UIImageView()
    var headerlblUserName = UILabel()
    var headerlblVersion = UILabel()

    let aData : NSArray = ["Main", "Profile", "Documents", "Setup Call", "Log Out"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(SideMenuVC.incomingVideoGroupCallNotification), name: NSNotification.Name(rawValue: incomingVideoGroupCallNotificationKey), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SideMenuVC.incomingAudioGroupCallNotification), name: NSNotification.Name(rawValue: incomingAudioGroupCallNotificationKey), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SideMenuVC.incomingMeetingLinkCall), name: NSNotification.Name(rawValue: incomingMeetingLinkPressedNotificationKey), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SideMenuVC.incomingPresenterGroupCall), name: NSNotification.Name(rawValue: incomingPresenterGroupCallNotificationKey), object: nil)

        userList.getUpdatedUsersList(uploadUserListComplete)

        headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 190))
        headerView.backgroundColor = UIColor.clear

        let frameGreen = CGRect(x: 0, y: 0, width: 90, height: 90)
        headerGreenImageView = UIImageView(frame: frameGreen)
        headerGreenImageView.image = UIImage(named: "avatar_eillipse")
        headerView.addSubview(headerGreenImageView)

        let frame = CGRect(x: 5, y: 5, width: 80, height: 80)
        headerImageView = UIImageView(frame: frame)
        headerView.addSubview(headerImageView)

        headerlblUserName = UILabel(frame: CGRect(x: headerImageView.frame.size.width + 15, y: 30, width: self.view.frame.size.width, height: 21))
        headerlblUserName.textAlignment = NSTextAlignment.left
        headerlblUserName.textColor = UIColor.white
        headerlblUserName.text = ""
        headerView.addSubview(headerlblUserName)

        headerlblVersion = UILabel(frame: CGRect(x: 5, y: 93, width: self.view.frame.size.width, height: 10))
        headerlblVersion.textAlignment = NSTextAlignment.left
        headerlblVersion.textColor = UIColor.black
        headerlblVersion.font = headerlblVersion.font.withSize(10)
        headerView.addSubview(headerlblVersion)

        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        if applicationSetup.getApplicationSetupNetworkComplete() == true {

            setupBackground()
        }
        else {
            
            applicationSetup.getApplicationSetup("666", completion: { (result, code) in
                
                self.setupBackground()
            })
        }
    }

    func uploadUserListComplete(_ result: String, code: String) {
        
        if result == "success" {
            
            let user = userList.userLoggedIn
            headerlblUserName.text = user!.jobTitle + " " + user!.firstName + " " + user!.lastName + ", " + user!.credentials

            headerlblVersion.text = "Version: " + Bundle.main.releaseVersionNumber! + " Build: " + Bundle.main.buildVersionNumber!

            let userPicPath = userList.userLoggedIn?.picPath

            Alamofire.request(userPicPath!)
                .responseImage { response in
                    
                    if let image = response.result.value {
                        
                        let circularImage = image.af_imageRoundedIntoCircle()
                        self.headerImageView.image = circularImage
                    }
            }
            
        }
    }

    // MARK: - Local Methods
    
    func setupBackground() {

        let backgroundUrl = applicationSetup.getApplicationBackgroundUrl()
        if backgroundUrl != nil {
            
            Alamofire.request(backgroundUrl!)
                .responseImage { response in
                    //debugPrint(response)
                    //print(response.request)
                    //print(response.response)
                    //debugPrint(response.result)
                    
                    if let image = response.result.value {
                        
                        //print("image downloaded: \(image)")
                        self.imgBackground.image = image
                    }
                    else {
                        
                        Gradient.setBackgroundGradient(self.view)
                    }
            }
        }
        else {
            
            Gradient.setBackgroundGradient(self.view)
        }
    }
    
    // MARK: - Notification Methods
    
    func incomingVideoGroupCallNotification(_ notification: Notification) {
        
        let videoInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let groupCallID = videoInfo["groupCallID"] as! String
        print("GroupCall ID = " + groupCallID)
        let groupCallName = videoInfo["groupCallName"] as! String
        print("GroupCall Name = " + groupCallName)
        let groupCallUserId = videoInfo["uID"] as! String
        print("FROM: " + groupCallUserId)
        //let users = videoInfo["users"] as! Array<String>
        
        let alert = UIAlertController(title: "Video Group Call", message: "Do you want to join group call: " + groupCallName + " from " + userList.getUserName(groupCallUserId), preferredStyle: UIAlertControllerStyle.alert)
        
        let rejectCallAction = UIAlertAction(title: "Reject", style: .cancel) { (action:UIAlertAction!) in
            
            globalSignal.sendGlobalSignalRejectCall(groupCallID)
        }
        alert.addAction(rejectCallAction)
        
        let OKAction = UIAlertAction(title: "Accept", style: .default) { (action:UIAlertAction!) in
            
            userList.setUserStatus(UserOnlineStatus.onlineOnCall)
            
            print("Accept Video Group Call button pressed")
            let storyboard = UIStoryboard(name: "CalliPhone", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CalliPhoneViewController") as! CalliPhoneViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = groupCallID
            vc.isPresentation = false
            vc.remoteGroupCallInfo = videoInfo
            vc.callType = .groupCall
            vc.isRemoteRequestForGroupCall = true
            vc.isEmailLinkGroupCall = false
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        
        sideMenuVC.closeMenu()
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func incomingAudioGroupCallNotification(_ notification: Notification) {
        
        let audioInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let groupCallID = audioInfo["groupCallID"] as! String
        print("GroupCall ID = " + groupCallID)
        let groupCallName = audioInfo["groupCallName"] as! String
        print("GroupCall Name = " + groupCallName)
        let groupCallUserId = audioInfo["uID"] as! String
        print("FROM: " + groupCallUserId)
        
        let alert = UIAlertController(title: "Audio Group Call", message: "Do you want to join a Audio Group Call: " + groupCallName + " from " + userList.getUserName(groupCallUserId), preferredStyle: UIAlertControllerStyle.alert)
        
        let rejectCallAction = UIAlertAction(title: "Reject", style: .cancel) { (action:UIAlertAction!) in
            
            globalSignal.sendGlobalSignalRejectCall(groupCallID)
        }
        alert.addAction(rejectCallAction)
        
        let OKAction = UIAlertAction(title: "Accept", style: .default) { (action:UIAlertAction!) in
            
            userList.setUserStatus(UserOnlineStatus.onlineOnCall)
            
            print("Accept Audio Call button pressed")
            let storyboard = UIStoryboard(name: "CalliPhone", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CalliPhoneViewController") as! CalliPhoneViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = groupCallID
            vc.isPresentation = false
            vc.isRemoteRequestForGroupCall = true
            vc.isEmailLinkGroupCall = false
            vc.remoteGroupCallInfo = audioInfo
            vc.callType = .audioCall
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        
        sideMenuVC.closeMenu()
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func incomingMeetingLinkCall(_ notification: Notification) {
        
        let meetingInfo = notification.userInfo as! [String:String?]
        
        print(meetingInfo)
        
        let storyboard = UIStoryboard(name: "CalliPhone", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CalliPhoneViewController") as! CalliPhoneViewController
        vc.modalPresentationStyle = .fullScreen
        vc.isPresentation = false
        vc.isRemoteRequestForGroupCall = false
        vc.isEmailLinkGroupCall = true
        vc.remoteGroupCallInfo = meetingInfo as Dictionary<String, AnyObject>?
        vc.callType = .groupCall
        self.present(vc, animated: true, completion: nil)
        
        sideMenuVC.closeMenu()
    }
    
    func incomingPresenterGroupCall(_ notification: Notification) {
        
        let presenterInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let groupCallID = presenterInfo["groupCallID"] as! String
        print("GroupCall ID = " + groupCallID)
        let groupCallName = presenterInfo["groupCallName"] as! String
        print("GroupCall Name = " + groupCallName)
        let groupCallUserId = presenterInfo["uID"] as! String
        
        let alert = UIAlertController(title: "Presentation Group Call", message: "Do you want to join a Presenters Group Call: " + groupCallName + " from " + userList.getUserName(groupCallUserId), preferredStyle: UIAlertControllerStyle.alert)
        
        let rejectAction = UIAlertAction(title: "Reject", style: .cancel) { (action:UIAlertAction!) in
            
            globalSignal.sendGlobalSignalRejectCall(groupCallID)
        }
        alert.addAction(rejectAction)
        
        let OKAction = UIAlertAction(title: "Accept", style: .default) { (action:UIAlertAction!) in
            
            userList.setUserStatus(UserOnlineStatus.onlineOnCall)
            
            print("Accept Presentation Group Call button pressed")
            let storyboard = UIStoryboard(name: "CalliPhone", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CalliPhoneViewController") as! CalliPhoneViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = groupCallID
            vc.remoteGroupCallInfo = presenterInfo
            vc.isPresentation = true
            vc.callType = .presentationCall
            vc.isRemoteRequestForGroupCall = true
            vc.isEmailLinkGroupCall = false
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        
        sideMenuVC.closeMenu()
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension SideMenuVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return aData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let aCell = tableView.dequeueReusableCell(
            withIdentifier: "kCell", for: indexPath)
        let aLabel : UILabel = aCell.viewWithTag(10) as! UILabel
        aLabel.text = aData[indexPath.row] as? String
        return aCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            _ = kConstantObj.SetIntialMainViewController("MainiPhoneViewController")
        } else if indexPath.row == 1 {
            
            sideMenuVC.closeMenu()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        } else if indexPath.row == 2 {
            
            _ = kConstantObj.SetIntialMainViewController("DocumentsiPhoneViewController")
        } else if indexPath.row == 3 {
            
            _ = kConstantObj.SetIntialMainViewController("SetupCalliPhoneViewController")
        } else if indexPath.row == 4 {
            
            sideMenuVC.closeMenu()
            
            UserDefaults.standard.set(false, forKey: "userLoggedIn")
            
            // Close and go to login View
            let storyboard = UIStoryboard(name: "LoginiPhone", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
            
            let window = UIApplication.shared.windows[0] as UIWindow;
            UIView.transition(
                from: window.rootViewController!.view,
                to: vc.view,
                duration: 0.65,
                options: .transitionCrossDissolve,
                completion: {
                    finished in //window.rootViewController = vc
                    UIApplication.shared.keyWindow?.rootViewController = vc;
            })
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
