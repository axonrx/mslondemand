//
//  StringUtils.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/22/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//


extension String {

    func StringDateTimeDate() -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: self)
    }

    func StringDateTimeDateCallDetails() -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: self)
    }
}
