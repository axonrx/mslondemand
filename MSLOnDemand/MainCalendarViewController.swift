//
//  MainCalendarViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 6/30/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit

class MainCalendarViewController: UIViewController {
    
    var calendar: CKCalendarView?

    var data : NSMutableDictionary
    
    required init?(coder aDecoder: NSCoder) {
        
        data = NSMutableDictionary()
        
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        
        self.data = NSMutableDictionary()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    override func viewDidLoad() {

        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        loadEvents()
    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)

        if calendar == nil {
            
            calendar = CKCalendarView()
            calendar!.viewSize = self.view.frame.size
            calendar!.delegate = self
            calendar!.dataSource = self
            self.view.addSubview(calendar!)
        }
        else {
            
            calendar?.reload()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loadEvents() {

        self.data.removeAllObjects()

        let historyEvents = groupCallList.getGroupCallHistoryList()
        
        for historyEvent: GroupCall in historyEvents {
            
            if historyEvent.callType != .canceledCall {
             
                let groupcall_info: [String:String] = [
                    "groupCallID" : historyEvent.groupCallID,
                    "callType": String(describing: historyEvent.callType),
                    "isParticipate": String(historyEvent.isParticipate),
                    "notes" : historyEvent.notes
                ]
                
                var eventColor = UIColor(red: 251/255, green: 0/255, blue: 26/255, alpha: 1)
                if historyEvent.isParticipate == true {
                    
                    eventColor = UIColor(red: 252/255, green: 119/255, blue: 8/255, alpha: 1)
                }
                else {
                    
                    if historyEvent.callType == .presentationCall {
                        
                        eventColor = UIColor(red: 0/255, green: 0/255, blue: 202/255, alpha: 1)
                    }
                    else if historyEvent.callType == .groupCall {
                        
                        eventColor = UIColor(red: 226/255, green: 0/255, blue: 255/255, alpha: 1)
                    }
                    else if historyEvent.callType == .audioCall {
                        
                        eventColor = UIColor(red: 0/255, green: 255/255, blue: 0/255, alpha: 1)
                    }
                }
                
                let stringDate = historyEvent.scheduledCallTime
                var date : Date = Date()
                if !stringDate.isEmpty {

                    date = stringDate.StringDateTimeDate()! as Date
                }
                
                let event : CKCalendarEvent = CKCalendarEvent(title: historyEvent.groupCallName as String, andDate: date, andInfo: groupcall_info, andColor: eventColor)
                
                self.data.setObject(event, forKey: date as NSCopying)
            }
        }

        let scheduledEvents = groupCallList.getGroupCallScheduledList()
        
        for scheduledEvent: GroupCall in scheduledEvents {

            let groupcall_info: [String:String] = [
                "groupCallID" : scheduledEvent.groupCallID,
                "callType": String(describing: scheduledEvent.callType),
                "isParticipate": String(scheduledEvent.isParticipate),
                "notes" : scheduledEvent.notes
            ]

            var eventColor = UIColor(red: 251/255, green: 0/255, blue: 26/255, alpha: 1)
            if scheduledEvent.isParticipate == true {
                
                eventColor = UIColor(red: 252/255, green: 119/255, blue: 8/255, alpha: 1)
           }
            else {
                
                if scheduledEvent.callType == .presentationCall {
                    
                    eventColor = UIColor(red: 0/255, green: 0/255, blue: 202/255, alpha: 1)
                }
                else if scheduledEvent.callType == .groupCall {
                    
                    eventColor = UIColor(red: 226/255, green: 0/255, blue: 255/255, alpha: 1)
                }
                else if scheduledEvent.callType == .audioCall {
                    
                    eventColor = UIColor(red: 0/255, green: 255/255, blue: 0/255, alpha: 1)
                }
            }
            
            let stringDate = scheduledEvent.scheduledCallTime
            let date : Date = stringDate.StringDateTimeDate()! as Date

            let event : CKCalendarEvent = CKCalendarEvent(title: scheduledEvent.groupCallName as String, andDate: date, andInfo: groupcall_info, andColor: eventColor)

            self.data.setObject(event, forKey: date as NSCopying)
        }
        
        calendar?.reload()
    }
}

extension MainCalendarViewController: CKCalendarViewDataSource, CKCalendarViewDelegate {

    //  MARK: - CKCalendarDataSource
    
    public func calendarView(_ calendarView: CKCalendarView!, eventsFor date: Date!) -> [Any]! {

        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.day , .month , .year], from: date)
        let year =  components.year
        let month = components.month
        let day = components.day
        let calanderDate : Date = NSDate(day: UInt(day!), month: UInt(month!), year: UInt(year!)) as Date

        var events = Array<CKCalendarEvent>()
        let enumerator = self.data.keyEnumerator()
        while let key = enumerator.nextObject() {

            let components = (calendar as NSCalendar).components([.day , .month , .year], from: key as! Date)
            let year =  components.year
            let month = components.month
            let day = components.day
            let eventDate : Date = NSDate(day: UInt(day!), month: UInt(month!), year: UInt(year!)) as Date
            
            if eventDate == calanderDate {
                
                let event = self.data.object(forKey: key) as! CKCalendarEvent
                events.append(event)
            }
        }

        return events as [AnyObject]
    }
    
    //  MARK: - CKCalendarViewDelegate
    
    func calendarView(_ CalendarView: CKCalendarView!, willSelect date: Date!) {
        
    }
    
    func calendarView(_ CalendarView: CKCalendarView!, didSelect date: Date!) {
        
    }
    
    func calendarView(_ CalendarView: CKCalendarView!, didSelect date: CKCalendarEvent!) {
        
        //print(date.info)
        
        let storyboard = UIStoryboard(name: "CallDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallDetailsViewController") as! CallDetailsViewController
        vc.selectedGroupCallID = date.info["groupCallID"] as! String
        self.present(vc, animated: true, completion: nil)
    }

}
