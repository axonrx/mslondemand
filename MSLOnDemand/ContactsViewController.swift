//
//  ContactsViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 6/29/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import MBProgressHUD


protocol MainViewDelegate {
    
    func searchBarSelected(open: Bool)
    func historyHomeButtonSelected()
    
}

class ContactsViewController: UIViewController {

    @IBOutlet weak var viewINSSeachBar: UIView!
    @IBOutlet weak var tblViewContacts: UITableView!
    @IBOutlet weak var sortSegmentControl: UISegmentedControl!
    @IBOutlet weak var lblSort: UILabel!
    
    var screenMode = 0

    let searchBar: INSSearchBar = INSSearchBar(frame: CGRect(x: 0.0, y: 0.0, width: 100.0, height: 34.0))
    
    var searchResults: Array<User>?
    var turnOffAddToCallButton = true
    
    var selectedUsers: Array<String> = []

    var selectedFilterIndx = 0

    var isAddCaller = false

    var delegate:CallDetailsViewDelegate?
    var mainDelegate:MainViewDelegate?

    var hud: MBProgressHUD?
    
    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(ContactsViewController.updateRemoveUserFromCallNotification), name: NSNotification.Name(rawValue: updateRemoveUserFromCallNotificationKey), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(ContactsViewController.updateContactsForTagNotification), name: NSNotification.Name(rawValue: updateContactsForTagNotificationKey), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(ContactsViewController.updateUserStatusNotification), name: NSNotification.Name(rawValue: updateUserStatusNotificationKey), object: nil)
        
        if isAddCaller == true {
         
            sortSegmentControl.isHidden = true
            lblSort.isHidden = true
        }
        else {
            
            sortSegmentControl.isHidden = false
            lblSort.isHidden = false
        }
    }
   
    override func viewWillAppear(_ animated: Bool) {
    
        selectedUsers.removeAll()

        super.viewWillAppear(animated)
        if isAddCaller == false {
         
            searchResults = nil
        }
        
        let orient = UIApplication.shared.statusBarOrientation
        
        switch orient {
            
        case .portrait:
            self.screenMode = 0
            break
        default:
            self.screenMode = 1
            break
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)

        if isAddCaller == false {
            
            self.searchBar.frame = CGRect(x: 20, y: 0, width: viewINSSeachBar.frame.width - 40, height: viewINSSeachBar.frame.height)
            viewINSSeachBar.addSubview(self.searchBar)
            searchBar.delegate = self
            searchBar.showSearchBar(self)

            sortUserListOnIndx(selectedFilterIndx)
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator);
        
        coordinator.animate(alongsideTransition: nil, completion: {
            _ in

            if self.isAddCaller == false {
                
                self.searchBar.frame = CGRect(x: 20, y: 0, width: self.viewINSSeachBar.frame.width - 40, height: self.viewINSSeachBar.frame.height)
            }
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
                
            case .portrait:
                self.screenMode = 0
                break
            default:
                self.screenMode = 1
                break
            }

        })
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func filterControlValueChanged(_ sender: AnyObject) {

        selectedFilterIndx = sender.selectedSegmentIndex
        sortUserListOnIndx(selectedFilterIndx)
        tblViewContacts.reloadData()
    }
    
    // MARK: - Notification Methods
    
    func updateContactsForTagNotification(_ notification: Notification) {
        
        let tagInfo = notification.userInfo as! Dictionary<String,AnyObject>
        let tagText = tagInfo["tagText"] as! String

        searchResults = userList.findTagInUserProgram(tagText)
        tblViewContacts.reloadData()
    }

    func updateUserStatusNotification(_ notification: Notification) {
        
        sortUserListOnIndx(selectedFilterIndx)
        tblViewContacts.reloadData()
    }
    
    func updateRemoveUserFromCallNotification(_ notification: Notification) {
        
        let userInfo = notification.userInfo as! Dictionary<String,AnyObject>
        let removeUID = userInfo["uID"] as! String

        var idx = 0
        for uID: String in selectedUsers {

            if uID == removeUID {
              
                selectedUsers.remove(at: idx)
                break;
            }
            idx = idx + 1
        }

        tblViewContacts.reloadData()
    }

    // MARK: - Class Methods

    func sortUserListOnIndx(_ indx: Int) {
        
        searchResults = nil
        
        switch indx {
            
        case 0: // Online
            userList.sortUsersDescendingOnlineStatus()
        case 1: // Name
            userList.sortUsersAscendingLastName()
        case 2: // Type
            userList.sortUsersDescendingFocus()
        case 3: // History
            userList.sortUsersDescendingLastSeen()
        default:
            break;
        }
    }

    func updateAvailableUsers(_ result: String) {
        
        if (result == "success") {
            
            sortUserListOnIndx(selectedFilterIndx)
            tblViewContacts.reloadData()
        }
    }

    func newGroupCallVideoCreated(_ result: String, gID: String) {
        
        hud?.hide(animated: true)
        
        if !DeviceType.IS_IPAD && !DeviceType.IS_IPAD_PRO {

            let storyboard = UIStoryboard(name: "CalliPhone", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CalliPhoneViewController") as! CalliPhoneViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = gID
            vc.callType = .groupCall
            vc.isPresentation = false
            vc.isRemoteRequestForGroupCall = false
            vc.isEmailLinkGroupCall = false
            self.present(vc, animated: true, completion: nil)
        }
        else {
            
            var storyboard = UIStoryboard(name: "Call", bundle: nil)
            if self.screenMode == 1 {
                
                storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
            }
            let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = gID
            vc.callType = .groupCall
            vc.isPresentation = false
            vc.isRemoteRequestForGroupCall = false
            vc.isEmailLinkGroupCall = false
            self.present(vc, animated: true, completion: nil)
        }
    }

    func newGroupCallAudioCreated(_ result: String, gID: String) {
        
        hud?.hide(animated: true)

        if !DeviceType.IS_IPAD && !DeviceType.IS_IPAD_PRO {
            
            let storyboard = UIStoryboard(name: "CalliPhone", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CalliPhoneViewController") as! CalliPhoneViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = gID
            vc.callType = .audioCall
            vc.isPresentation = false
            vc.isRemoteRequestForGroupCall = false
            vc.isEmailLinkGroupCall = false
            self.present(vc, animated: true, completion: nil)
        }
        else {
            
            var storyboard = UIStoryboard(name: "Call", bundle: nil)
            if self.screenMode == 1 {
                
                storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
            }
            let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = gID
            vc.callType = .audioCall
            vc.isPresentation = false
            vc.isRemoteRequestForGroupCall = false
            vc.isEmailLinkGroupCall = false
            self.present(vc, animated: true, completion: nil)
        }
    }

    // MARK: - Button Target Methods

    func addUserToCallButtonPressed(_ sender:UIButton) {
        
        let buttonRow = sender.tag
        let user = userList.getUserList()[buttonRow]
        delegate?.addUserToCallPressed(user.uID)
        selectedUsers.append(user.uID)
        tblViewContacts.reloadData()
    }
    
    func callAudioButtonPressed(_ sender:UIButton) {
        
        let buttonRow = sender.tag
        print("call audio button pressed for row: ", buttonRow)

        let user = userList.getUserList()[buttonRow]
/*
        if user.userStatus != .OnlineAvailable {
            
            let alert = UIAlertController(title: "Call Error", message: "User is not online for a audio call.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
*/
        // If this is a new group call then save if off before call. Need to get OpenTok session, etc.
        hud = MBProgressHUD.showAdded(to: (self.parent?.view)!, animated:true)
        hud!.mode = MBProgressHUDMode.indeterminate
        
        hud!.label.text = "Creating Audio Call..."

        let docIds = Array<String>()
        let callType = GroupCallType.audioCall
        var users = Array<String>()
        users.append(user.uID)
        let notes = ""
        let callName = user.jobTitle + " " + user.firstName + " " + user.lastName + ", " + user.credentials
        
        groupCallList.postNewGroupCallDetails(callName, scheduled: Date().getUTCFormateDate(), users: users, notes: notes, documents: docIds, type: callType, completion: newGroupCallAudioCreated)
    }

    func callVideoButtonPressed(_ sender:UIButton) {
        
        let buttonRow = sender.tag
        print("call video button pressed for row: ", buttonRow)

        let user = userList.getUserList()[buttonRow]

        if user.userStatus != .onlineAvailable {
            
            let alert = UIAlertController(title: "Call Error", message: "User is not online for a video call.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }

        // If this is a new group call then save if off before call. Need to get OpenTok session, etc.
        hud = MBProgressHUD.showAdded(to: (self.parent?.view)!, animated:true)
        hud!.mode = MBProgressHUDMode.indeterminate

        hud!.label.text = "Creating Video Call..."

        let docIds = Array<String>()
        let callType = GroupCallType.groupCall
        var users = Array<String>()
        users.append(user.uID)
        let notes = ""
        let callName = user.jobTitle + " " + user.firstName + " " + user.lastName + ", " + user.credentials

        groupCallList.postNewGroupCallDetails(callName, scheduled: Date().getUTCFormateDate(), users: users, notes: notes, documents: docIds, type: callType, completion: newGroupCallVideoCreated)
    }

}

// MARK: - UITableViewDataSource and UITableViewDelegate

extension ContactsViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchResults == nil {
            
            //print("user list count = ", userList.getUserList().count)
            return userList.getUserList().count
        }
        else {

            //print("search array count = ", (searchResults?.count)!)
            return (searchResults?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsTableViewCell", for: indexPath) as! ContactsTableViewCell
        
        var user = userList.getUserList()[indexPath.row]
        if searchResults != nil {
            
            user = searchResults![indexPath.row]
        }

        cell.lblUserName.text = user.jobTitle + " " + user.firstName + " " + user.lastName + ", " + user.credentials
        cell.lblProgramLocation.text = user.programs + ", " + user.location
        cell.imgViewAvailable.image = UIImage(named: "available_unavailable")

        cell.vwSelected.isHidden = true
        if selectedUsers.contains(user.uID) {
            
            cell.vwSelected.isHidden = false
        }

        if turnOffAddToCallButton == true {
        
            cell.btnAddToCall.isHidden = true

            cell.btnCallAudio.tag = indexPath.row
            cell.btnCallAudio.addTarget(self, action: #selector(ContactsViewController.callAudioButtonPressed(_:)), for: UIControlEvents.touchUpInside)
            cell.btnCallAudio.isHidden = false

            cell.btnCallVideo.tag = indexPath.row
            cell.btnCallVideo.addTarget(self, action: #selector(ContactsViewController.callVideoButtonPressed(_:)), for: UIControlEvents.touchUpInside)
            cell.btnCallVideo.isHidden = false
        }
        else {
            
            cell.btnAddToCall.tag = indexPath.row
            cell.btnAddToCall.addTarget(self, action: #selector(ContactsViewController.addUserToCallButtonPressed(_:)), for: UIControlEvents.touchUpInside)
            cell.btnAddToCall.isHidden = false

            cell.btnCallAudio.isHidden = true
            cell.btnCallVideo.isHidden = true
        }
        
        cell.btnCallAudio.setImage(UIImage(named: "audio_call_offline"), for: UIControlState())
        cell.btnCallVideo.setImage(UIImage(named: "video_call_offline"), for: UIControlState())
        if user.userStatus == .onlineAvailable {
            
            cell.imgViewAvailable.image = UIImage(named: "available_available")
            cell.btnCallAudio.setImage(UIImage(named: "audio_call_online"), for: UIControlState())
            cell.btnCallVideo.setImage(UIImage(named: "video_call_online"), for: UIControlState())
        }
        
        if user.userStatus == .onlineOnCall {
            
            cell.imgViewAvailable.image = UIImage(named: "available_oncall")
            cell.btnCallAudio.setImage(UIImage(named: "audio_call_oncall"), for: UIControlState())
            cell.btnCallVideo.setImage(UIImage(named: "video_call_oncall"), for: UIControlState())
        }

        let thumbnailPath = userList.getUserThumbnailPath(user.uID)
        
        //let image = UIImage(named: "avatar-stand-in")
        //cell.imgViewUserThumbnail.image = image!.af_imageRoundedIntoCircle()
        
        Alamofire.request(thumbnailPath)
            .responseImage { response in
                //debugPrint(response)
                //print(response.request)
                //print(response.response)
                //debugPrint(response.result)
                
                if let image = response.result.value {

                    //print("image downloaded: \(image)")
                    //let radius: CGFloat = 20.0
                    //let roundedImage = image.af_imageWithRoundedCornerRadius(radius)
                    //let circularImage = image.af_imageRoundedIntoCircle()
                    //cell.imgViewUserThumbnail.image = circularImage
                    cell.imgViewUserThumbnail.image = image
                }
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

// MARK: - INSSearchBarDelegate

extension ContactsViewController: INSSearchBarDelegate {

    func searchBarKeyboardNotification(open: Bool) {
        
        mainDelegate?.searchBarSelected(open: open)
    }

    
    func destinationFrameForSearchBar(_ searchBar: INSSearchBar) -> CGRect {

        return searchBar.originalFrame;
    }
    
    func searchBar(_ searchBar: INSSearchBar, willStartTransitioningToState destinationState: INSSearchBarState) {
        
    }
    
    func searchBar(_ searchBar: INSSearchBar, didEndTransitioningFromState previousState: INSSearchBarState) {
        
        //if previousState == .SearchBarVisible {
            
            searchResults = nil
        //}

        tblViewContacts.reloadData()
    }
    
    func searchBarDidTapReturn(_ searchBar: INSSearchBar) {
        
    }
    
    func searchBarTextDidChange(_ searchBar: INSSearchBar) {

        if searchBar.searchField.text?.characters.count == 0 {
        
            searchResults = nil
        }
        else {
        
            let searchArray = userList.getUserList()
            let lastNameResults = searchArray.filter { $0.lastName.lowercased().range(of: (searchBar.searchField.text?.lowercased())!) != nil }
            let firstNameResults = searchArray.filter { $0.firstName.lowercased().range(of: (searchBar.searchField.text?.lowercased())!) != nil }

            searchResults = lastNameResults + firstNameResults
        }
        
        tblViewContacts.reloadData()
    }
}

extension ContactsViewController: CallDetailsViewDelegate {
    
    func callCanceledButtonPressed() {
        
    }

    func addUserToCallPressed(_ uID: String) {
        
    }
    
    func scheduleCallButtonPressed() {
        
    }

    func callNowButtonPressed() {
        
    }

    func noteViewSelected(open: Bool) {
        
    }

}
