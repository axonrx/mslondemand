//
//  HistoryViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 6/29/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class HistoryViewController: UIViewController {
    
    @IBOutlet weak var tblViewCallHistory: UITableView!
    @IBOutlet weak var imgBackground: UIImageView!

    var mainDelegate:MainViewDelegate?
    var screenMode = 0

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(HistoryViewController.updateGroupCallsComplete), name: NSNotification.Name(rawValue: updateGroupCallsCompleteNotificationKey), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(HistoryViewController.incomingMeetingLinkCall), name: NSNotification.Name(rawValue: incomingMeetingLinkPressedNotificationKey), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HistoryViewController.incomingPresenterGroupCall), name: NSNotification.Name(rawValue: incomingPresenterGroupCallNotificationKey), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(HistoryViewController.incomingVideoGroupCallNotification), name: NSNotification.Name(rawValue: incomingVideoGroupCallNotificationKey), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if applicationSetup.getApplicationSetupNetworkComplete() == true {
            
            setupApplicationLook()
        }
        else {
            
            applicationSetup.getApplicationSetup("666", completion: { (result, code) in
                
                if result.contains("success") {
                    
                    self.setupApplicationLook()
                }
            })
        }

        let orient = UIApplication.shared.statusBarOrientation
        
        switch orient {
            
        case .portrait:
            self.screenMode = 0
            break
        default:
            self.screenMode = 1
            break
        }

        tblViewCallHistory.reloadData()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator);
        
        coordinator.animate(alongsideTransition: nil, completion: {
            _ in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
                
            case .portrait:
                self.screenMode = 0
                break
            default:
                self.screenMode = 1
                break
            }
            
        })
    }

    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: incomingVideoGroupCallNotificationKey), object: nil)
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Local Methods

    func setupApplicationLook() {
        
        let backgroundUrl = applicationSetup.getApplicationBackgroundUrl()
        if backgroundUrl != nil {
            
            Alamofire.request(backgroundUrl!)
                .responseImage { response in
                    //debugPrint(response)
                    //print(response.request)
                    //print(response.response)
                    //debugPrint(response.result)
                    
                    if let image = response.result.value {
                        
                        //print("image downloaded: \(image)")
                        self.imgBackground.image = image
                    }
                    else {
                        
                        Gradient.setBackgroundGradient(self.view)
                    }
            }
        }
        else {
            
            Gradient.setBackgroundGradient(self.view)
        }
    }

    // MARK: - Notification Methods

    func updateGroupCallsComplete(_ notification: Notification) {
        
        tblViewCallHistory.reloadData()
    }

    // MARK: - IBActions

    func historyCallDetailsButtonPressed(_ sender:UIButton) {
        
        let buttonRow = sender.tag
        //print("Call details button pressed for row ", buttonRow)

        let groupCall = groupCallList.getGroupCallHistoryList()[buttonRow]

        let storyboard = UIStoryboard(name: "CallDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallDetailsViewController") as! CallDetailsViewController
        vc.selectedGroupCallID = groupCall.groupCallID
        self.present(vc, animated: true, completion: nil)
    }

    @IBAction func homeButtonPressed(_ sender: Any) {

        mainDelegate?.historyHomeButtonSelected()
        self.dismiss(animated: true, completion: {})
    }
    
    func incomingMeetingLinkCall(_ notification: Notification) {
        
        let meetingInfo = notification.userInfo as! [String:String?]
        
        print(meetingInfo)
        
        var storyboard = UIStoryboard(name: "Call", bundle: nil)
        if self.screenMode == 1 {
            
            storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
        }
        let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
        vc.modalPresentationStyle = .fullScreen
        vc.isPresentation = false
        vc.isRemoteRequestForGroupCall = false
        vc.isEmailLinkGroupCall = true
        vc.remoteGroupCallInfo = meetingInfo as Dictionary<String, AnyObject>?
        vc.callType = .groupCall
        self.present(vc, animated: true, completion: nil)
    }
    
    func incomingPresenterGroupCall(_ notification: Notification) {
        
        let presenterInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let groupCallID = presenterInfo["groupCallID"] as! String
        print("GroupCall ID = " + groupCallID)
        let groupCallName = presenterInfo["groupCallName"] as! String
        print("GroupCall Name = " + groupCallName)
        let groupCallUserId = presenterInfo["uID"] as! String
        
        let alert = UIAlertController(title: "Presentation Group Call", message: "Do you want to join a Presenters Group Call: " + groupCallName + " from " + userList.getUserName(groupCallUserId), preferredStyle: UIAlertControllerStyle.alert)
        
        let rejectAction = UIAlertAction(title: "Reject", style: .cancel) { (action:UIAlertAction!) in
            
            globalSignal.sendGlobalSignalRejectCall(groupCallID)
        }
        alert.addAction(rejectAction)
        
        let OKAction = UIAlertAction(title: "Accept", style: .default) { (action:UIAlertAction!) in
            
            userList.setUserStatus(UserOnlineStatus.onlineOnCall)
            
            print("Accept Presentation Group Call button pressed")
            var storyboard = UIStoryboard(name: "Call", bundle: nil)
            if self.screenMode == 1 {
                
                storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
            }
            let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = groupCallID
            vc.remoteGroupCallInfo = presenterInfo
            vc.isPresentation = true
            vc.callType = .presentationCall
            vc.isRemoteRequestForGroupCall = true
            vc.isEmailLinkGroupCall = false
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        
        self.present(alert, animated: true, completion: nil)
    }

    func incomingVideoGroupCallNotification(_ notification: Notification) {
        
        let videoInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let groupCallID = videoInfo["groupCallID"] as! String
        print("GroupCall ID = " + groupCallID)
        let groupCallName = videoInfo["groupCallName"] as! String
        print("GroupCall Name = " + groupCallName)
        let groupCallUserId = videoInfo["uID"] as! String
        print("FROM: " + groupCallUserId)
        //let users = videoInfo["users"] as! Array<String>
        
        let alert = UIAlertController(title: "Video Group Call", message: "Do you want to join group call: " + groupCallName + " from " + userList.getUserName(groupCallUserId), preferredStyle: UIAlertControllerStyle.alert)
        
        let rejectCallAction = UIAlertAction(title: "Reject", style: .cancel) { (action:UIAlertAction!) in
            
            globalSignal.sendGlobalSignalRejectCall(groupCallID)
        }
        alert.addAction(rejectCallAction)
        
        let OKAction = UIAlertAction(title: "Accept", style: .default) { (action:UIAlertAction!) in
            
            userList.setUserStatus(UserOnlineStatus.onlineOnCall)
            
            print("Accept Video Group Call button pressed")
            var storyboard = UIStoryboard(name: "Call", bundle: nil)
            if self.screenMode == 1 {
                
                storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
            }
            let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = groupCallID
            vc.isPresentation = false
            vc.remoteGroupCallInfo = videoInfo
            vc.callType = .groupCall
            vc.isRemoteRequestForGroupCall = true
            vc.isEmailLinkGroupCall = false
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        
        self.present(alert, animated: true, completion: nil)
    }

}

// MARK: - UITableViewDataSource and UITableViewDelegate

extension HistoryViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 34
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let returnedView = UIView(frame: CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: 34))
        returnedView.backgroundColor = UIColor.black
        
        let label = UILabel(frame: CGRect(x: 5, y: 2, width: 100, height: 32))
        label.text = "Call History"
        label.textColor = UIColor.white
        returnedView.addSubview(label)
        
        return returnedView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //print("group call list count = ", groupCallList.getGroupCallHistoryList().count)
        return groupCallList.getGroupCallHistoryList().count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell

        let groupCall = groupCallList.getGroupCallHistoryList()[indexPath.row]

        cell.btnCallDetails.tag = indexPath.row
        cell.btnCallDetails.addTarget(self, action: #selector(HistoryViewController.historyCallDetailsButtonPressed(_:)), for: UIControlEvents.touchUpInside)

        cell.lblCallTitle.text = groupCall.groupCallName

        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = TimeZone(identifier: "UTC")
        
        if let date = df.date(from: groupCall.scheduledCallTime) {
            
            df.timeZone = TimeZone.autoupdatingCurrent
            df.dateFormat = "yyyy-MM-dd h:mm a"
            let twelveHourDateTime = df.string(from: date)
            cell.lblCallTime.text = "Date: " + twelveHourDateTime
        }

        if groupCall.isParticipate == true {
            
            cell.imgViewCallType.image = UIImage(named: "call-type-participate")
        }
        else {
            
            switch groupCall.callType {
            case .canceledCall:
                cell.imgViewCallType.image = UIImage(named: "call-type-canceled")
            case .presentationCall:
                cell.imgViewCallType.image = UIImage(named: "call-type-presentation")
            case .groupCall:
                cell.imgViewCallType.image = UIImage(named: "call-type-groupcall")
            case .audioCall:
                cell.imgViewCallType.image = UIImage(named: "call-type-audio")
            }
        }

        var userNames = String()
        for userID in groupCall.users {
            
            let userFirstName = userList.getUserName(userID)
            userNames += userFirstName + ", "
        }
        
        let trunc = String(userNames.characters.dropLast())
        cell.lblCallNames.text = String(trunc.characters.dropLast())

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

}
