//
//  VersionUtils.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 8/17/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//


extension Bundle {
    
    var releaseVersionNumber: String? {

        return self.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var buildVersionNumber: String? {

        return self.infoDictionary?["CFBundleVersion"] as? String
    }
    
}
