//
//  SelectedContactsTableViewCell.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/8/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class SelectedContactsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblUserInfo: UILabel!
    //@IBOutlet weak var lblProgram: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
}
