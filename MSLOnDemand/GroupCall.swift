//
//  GroupCall.swift
//  MLS Connect
//
//  Created by Chris Mutkoski on 4/21/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation


class GroupCall {
    
    var groupCallID: String         // GroupCall ID returned from server after if was added to DB
    var groupCallName: String       // GroupCall name
    var uID: String                 // User ID that created the group
    var users: Array<String>        // User ID's on group call
    var lastCallTime: String        // Last time group call completed group call
    var notes: String               // Call notes
    var localNotes: String          // Call notes specific to user
    var scheduledCallTime: String   // Schedule future group call
    var documents: Array<String>    // Document ID's for group call
    var callType: GroupCallType     // Group call type - Presentation or Group Call
    var isParticipate: Bool         // Paticipate on Group call?

    init(uID: String, groupCallID: String, groupCallName: String, users: Array<String>, date: String, notes: String, localNotes: String, scheduledTime: String, documents: Array<String>, callType: GroupCallType, participate: Bool) {
        
        self.groupCallID = groupCallID
        self.groupCallName = groupCallName
        self.uID = uID
        self.users = users
        self.lastCallTime = date
        self.notes = notes
        self.localNotes = localNotes
        self.scheduledCallTime = scheduledTime
        self.documents = documents
        self.callType = callType
        self.isParticipate = participate
    }
}
