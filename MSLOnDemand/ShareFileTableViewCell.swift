//
//  ShareFileTableViewCell.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 12/28/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class ShareFileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var imgFileIcon: UIImageView!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
}
