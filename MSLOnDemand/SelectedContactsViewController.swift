//
//  SelectedContactsViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/8/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


protocol CallDetailsViewDelegate {
    
    func callCanceledButtonPressed()
    func addUserToCallPressed(_ uID: String)
    func scheduleCallButtonPressed()
    func callNowButtonPressed()
    func noteViewSelected(open: Bool)

}

class SelectedContactsViewController: UIViewController {
    
    @IBOutlet weak var tblViewSelectedContacts: UITableView!

    var selectedUsers = Array<String>()
    var groupCallDetails: GroupCall?
    var hideCancelCallButton = false
    var selectedGroupCallID: String = String()

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if groupCallDetails != nil {
            
            for uID: String in (self.groupCallDetails?.users)! {
                
                selectedUsers.append(uID)
            }
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //print(segue.identifier)
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Class Methods
    
    func addUserToCall(_ uID: String) {
        
        selectedUsers.append(uID)
        tblViewSelectedContacts.reloadData()
    }

    func setGroupCallDetails(_ groupCall: GroupCall) {
        
        groupCallDetails = groupCall

        selectedUsers.removeAll()
        if groupCallDetails != nil {
            
            for uID: String in (self.groupCallDetails?.users)! {
                
                selectedUsers.append(uID)
            }
            
            tblViewSelectedContacts.reloadData()
        }
    }

    func getGroupCallUsers() -> Array<String> {
        
        return selectedUsers
    }

}

// MARK: - UITableViewDataSource and UITableViewDelegate

extension SelectedContactsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Selected Participants"
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

        let header:UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        
        header.textLabel!.textColor = UIColor.black
        header.textLabel!.font = UIFont.boldSystemFont(ofSize: 15)
        header.textLabel!.frame = header.frame
        header.textLabel!.textAlignment = NSTextAlignment.left
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 85.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return selectedUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedContactsTableViewCell", for: indexPath) as! SelectedContactsTableViewCell
        
        let uID = selectedUsers[indexPath.row]
        let user = userList.getUser(uID)
        cell.lblUserInfo.text = user!.jobTitle + " " + (user?.firstName)! + " " + (user?.lastName)!
        //cell.lblProgram.text = user!.programs
        cell.lblLocation.text = user!.location
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title: "Remove") { action, index in
            
            //print("remove user from call")
            let removeUid = self.selectedUsers[indexPath.row]

            let dic = ["uID" : removeUid]
            NotificationCenter.default.post(name: Notification.Name(rawValue: updateRemoveUserFromCallNotificationKey), object: self, userInfo: dic)

            self.selectedUsers.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)

            if self.groupCallDetails == nil {
                
                return
            }

            var indx = 0
            for uID: String in (self.groupCallDetails?.users)! {
                
                if uID == removeUid {

                    self.groupCallDetails?.users.remove(at: indx)
                }
                
                indx += 1
            }
        }

        delete.backgroundColor = UIColor.red
        
        return [delete]
    }

}
