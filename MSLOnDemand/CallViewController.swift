//
//  CallViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/14/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



protocol CallViewDelegate {
    
    func activeUserSelected(_ uID: String)
    func shareDocument(_ dID: String)
    func muteUser(_ uID: String)
    func unMuteUser(_ uID: String)
    func isPresentaCallView() -> Bool
    func addUserToCallAudio(_ uID: String)
    func addUserToCallVideo(_ uID: String)

}

class CallViewController: UIViewController {

    @IBOutlet weak var containerVideoView: UIView!
    @IBOutlet weak var containerChatView: UIView!
    @IBOutlet weak var textFieldChat: UITextField!
    @IBOutlet weak var viewChatControls: UIView!
    @IBOutlet weak var textFieldContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgBackground: UIImageView!

    @IBOutlet weak var imgViewBrandLogo: UIImageView!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var vwVideoView: UIView!
    @IBOutlet weak var vwParticipantsPreview: UIView!
    @IBOutlet weak var vwDocumentsView: UIView!
    @IBOutlet weak var vwGroupChatView: UIView!
    @IBOutlet weak var vwNotes: UIView!

    var selectedGroupCallID: String = String()
    var groupCallDetails: GroupCall?
    var callType = GroupCallType.groupCall
    var ignoreKeyboard = false

    var allUsersOnCall = Array<String>()

    var isRemoteRequestForGroupCall = false
    var isEmailLinkGroupCall = false
    var isPresenta = false
    var isPresentation = false
    var remoteGroupCallInfo: Dictionary<String,AnyObject>?

    var activeCallersViewController: ActiveCallersViewController?
    var fileSelectViewController: FileSelectViewController?
    var videoViewController: VideoViewController?
    var groupChatViewController: GroupChatViewController?
    var notesViewController: NotesViewController?

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        registerForKeyboardNotifications(self)

        NotificationCenter.default.addObserver(self, selector: #selector(CallViewController.keyboardWillShowNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CallViewController.keyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(CallViewController.rejectCallNotification), name: NSNotification.Name(rawValue: incomingGroupRejectNotificationKey), object: nil)
        
        userList.getUpdatedUsersList(uploadUserListComplete)

        userList.resetUsersMuteState()

        if isRemoteRequestForGroupCall == true {

            currentGroupCallID = remoteGroupCallInfo!["groupCallID"] as! String
            currentGroupCallLeadUiD = remoteGroupCallInfo!["uID"] as! String
            allUsersOnCall = remoteGroupCallInfo!["users"] as! Array<String>
            allUsersOnCall.append(remoteGroupCallInfo!["uID"] as! String)
            activeCallersViewController?.setUsersOnCall(allUsersOnCall)
        }
        else {

            groupCallDetails = groupCallList.getGroupCallDetails(selectedGroupCallID)

            currentGroupCallID = (groupCallDetails?.groupCallID)!
            currentGroupCallLeadUiD = (groupCallDetails?.uID)!

            allUsersOnCall = (groupCallDetails?.users)!
            allUsersOnCall.append((groupCallDetails?.uID)!)
            activeCallersViewController?.setUsersOnCall(allUsersOnCall)
            fileSelectViewController?.setDocumentIDs((groupCallDetails?.documents)!)
        }
        
        self.view.bringSubview(toFront: viewChatControls)

        UIApplication.shared.isIdleTimerDisabled = true

        vwVideoView.layer.borderWidth = 1
        vwVideoView.layer.borderColor = UIColor.darkGray.cgColor

        vwParticipantsPreview.layer.borderWidth = 1
        vwParticipantsPreview.layer.borderColor = UIColor.darkGray.cgColor

        vwDocumentsView.layer.borderWidth = 1
        vwDocumentsView.layer.borderColor = UIColor.darkGray.cgColor

        vwGroupChatView.layer.borderWidth = 1
        vwGroupChatView.layer.borderColor = UIColor.darkGray.cgColor

        vwNotes.layer.borderWidth = 1
        vwNotes.layer.borderColor = UIColor.darkGray.cgColor
        
        let storyboard = UIStoryboard(name: "CallDetails", bundle: nil)
        notesViewController = storyboard.instantiateViewController(withIdentifier: "NotesViewController") as? NotesViewController
        notesViewController!.delegate = self
        notesViewController!.view.frame = vwNotes.bounds
        vwNotes.addSubview(notesViewController!.view)
        addChildViewController(notesViewController!)
        notesViewController!.didMove(toParentViewController: self)
        
        lblVersion.text = "Version: " + Bundle.main.releaseVersionNumber! + " Build: " + Bundle.main.buildVersionNumber!        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        applicationSetup.getApplicationSetup("666", completion: { (result, code) in

            let imageUrl = applicationSetup.getApplicationLogoUrl()
            if imageUrl != nil {
                
                Alamofire.request(imageUrl!)
                    .responseImage { response in
                        
                        if let image = response.result.value {
                            
                            self.imgViewBrandLogo.image = image
                        }
                }
            }

            let backgroundUrl = applicationSetup.getApplicationBackgroundUrl()
            if backgroundUrl != nil {
                
                Alamofire.request(backgroundUrl!)
                    .responseImage { response in
                        //debugPrint(response)
                        //print(response.request)
                        //print(response.response)
                        //debugPrint(response.result)
                        
                        if let image = response.result.value {
                            
                            //print("image downloaded: \(image)")
                            self.imgBackground.image = image
                        }
                        else {
                            
                            Gradient.setBackgroundGradient(self.view)
                        }
                }
            }
            else {
                
                Gradient.setBackgroundGradient(self.view)
            }
        })
    }

    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        unregisterFromKeyboardNotifications()
    }

    override var shouldAutorotate : Bool {

        return false
    }

    override var prefersStatusBarHidden : Bool {
    
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //print(segue.identifier)
        if let vc = segue.destination as? VideoViewController, segue.identifier == "VideoView" {
            
            videoViewController = vc
            videoViewController?.delegate = self
            videoViewController?.callType = callType
            videoViewController?.isEmailLinkGroupCall = isEmailLinkGroupCall
            videoViewController?.selectedGroupCallID = selectedGroupCallID
        }
        
        if let vc = segue.destination as? ActiveCallersViewController, segue.identifier == "ActiveCallersView" {
            
            activeCallersViewController = vc
            activeCallersViewController?.delegate = self
        }

        if let vc = segue.destination as? FileSelectViewController, segue.identifier == "FileSelectView" {
            
            fileSelectViewController = vc
            fileSelectViewController?.delegate = self
            fileSelectViewController?.selectedGroupCallID = selectedGroupCallID
        }

        if let vc = segue.destination as? GroupChatViewController, segue.identifier == "GroupChatView" {
            
            groupChatViewController = vc
            groupChatViewController!.removeOldChat()
            groupChatViewController?.selectedGroupCallID = selectedGroupCallID
        }
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions

    @IBAction func sendButtonPressed(_ sender: AnyObject) {
    
        if textFieldChat.text?.characters.count > 0 {
            
            globalSignal.sendGlobalSignalChat(currentGroupCallID, text: textFieldChat.text!)
            textFieldChat.text = ""
        }
        
        textFieldChat.resignFirstResponder()
    }

    // MARK: - Keyboard Notifications
    
    func keyboardWillShowNotification(_ notification: Notification) {
        
        // NOTE(tsd): This "ignoreKeyboard" is a cluster fuck. Wish I had time to write it correctly. Oh well......
        if ignoreKeyboard == true {
            
            return
        }
        
        ignoreKeyboard = true

        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.view.frame.origin.y -= keyboardSize.height
        }
        
    }
    
    func keyboardWillHideNotification(_ notification: Notification) {
        
        if ignoreKeyboard == false {
            
            return
        }
        
        ignoreKeyboard = false

        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.view.frame.origin.y += keyboardSize.height
        }
    }

    // MARK: - Notifcation Methods

    func rejectCallNotification(_ notification: Notification) {

        let userInfo = notification.userInfo as! Dictionary<String,AnyObject>
        let userId = userInfo["uID"] as! String

        var idx = 0
        for uID:String in allUsersOnCall {
            
            if uID == userId {
                
                allUsersOnCall.remove(at: idx)
                break
            }
            
            idx += 1
        }

        
        if self.allUsersOnCall.count == 1 {
            
            self.videoViewController!.endCallNow()
        }
        
        activeCallersViewController!.setUserRejectedCall(userId)
    }

    // MARK: - Local Completion Methods
    
    func uploadUserListComplete(_ result: String, code: String) {
        
    }

}

extension CallViewController: VideoViewButtonPressedDelegate {
    
    func callEndedButtonPressed() {
        
        groupCallList.updateGroupCallDetailsWithNotes(selectedGroupCallID, notes: (notesViewController?.getNotes())!, completion: groupCallUpdated)

        UIApplication.shared.isIdleTimerDisabled = false

        groupChatViewController!.cleanupChat()

        self.dismiss(animated: true, completion: nil)
    }

    func groupCallUpdated(_ result: String) {
        
        groupCallList.getAllUserGroupCalls(userList.getLoginUserID(), completion: groupCallAllGrupCallsUpdated)
    }

    func groupCallAllGrupCallsUpdated(_ result: String) {
        
    }

    func userConnectedToStream(_ uID: String) {

        activeCallersViewController!.setUserConnectedCall(uID)
    }
    
    func userDisconnectedFromStream(_ uID: String) {

        var idx = 0
        for userId:String in allUsersOnCall {
            
            if userId == uID {
                
                allUsersOnCall.remove(at: idx)
                break
            }
            
            idx += 1
        }

        if self.allUsersOnCall.count == 1 {
            
            let alert = UIAlertController(title: "Call Ended", message: "Everyone has disconnected. The Call will end now.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            
                self.videoViewController!.endCallNow()
            }))
            self.present(alert, animated: true, completion: {})
        }

        activeCallersViewController!.setUserDisconnectedCall(uID)
    }

    func isRemoteCall() -> Bool {
        
        return isRemoteRequestForGroupCall
    }

    func getRemoteCallInfo() -> Dictionary<String,AnyObject>? {
        
        return remoteGroupCallInfo
    }

    func isPresentaCall() -> Bool {
        
        return isPresenta
    }

    func setPresentaCall(_ presenta: Bool) {
        
        isPresenta = presenta
    }

    func isPresentationCall() -> Bool {
        
        return isPresentation
    }

    func stopDocumentSharingCall() {
        
        fileSelectViewController?.currentSharedDocumentID = ""
    }
    
}

extension CallViewController: CallViewDelegate {
    
    func activeUserSelected(_ uID: String) {
        
        // Move video view to this user
        //print("Change video to user ", uID)

        videoViewController?.switchVideoToUser(uID)
    }

    func shareDocument(_ dID: String) {
        
        videoViewController!.shareDocument(dID)
    }

    func muteUser(_ uID: String) {
        
        globalSignal.sendGlobalSignalClientMute(uID)
    }

    func unMuteUser(_ uID: String) {
        
        globalSignal.sendGlobalSignalClientUnMute(uID)
    }

    func isPresentaCallView() -> Bool {
        
        return isPresenta
    }

    func addUserToCallAudio(_ uID: String) {
        
        videoViewController?.addUserToCallAudio(uID)
    }

    func addUserToCallVideo(_ uID: String) {
        
        allUsersOnCall.append(uID)
        activeCallersViewController?.setUsersOnCall(allUsersOnCall)
        videoViewController?.addUserToCallVideo(uID)
    }

}

extension CallViewController: KeyboardStateDelegate {
    
    func keyboardWillTransition(_ state: KeyboardState) {
        
        // keyboard will show or hide
    }
    
    func keyboardTransitionAnimation(_ state: KeyboardState) {
/*
        switch state {
        case .activeWithHeight(let height):
            textFieldContainerBottomConstraint.constant = height
        case .hidden:
            textFieldContainerBottomConstraint.constant = 0.0
        }
*/
        view.layoutIfNeeded()
    }
    
    func keyboardDidTransition(_ state: KeyboardState) {
        
        // keyboard animation finished
    }
}

extension CallViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if textFieldChat.text?.characters.count > 0 {
            
            globalSignal.sendGlobalSignalChat(currentGroupCallID, text: textFieldChat.text!)
            textFieldChat.text = ""
        }

        textField.resignFirstResponder()
        return true
    }
}

extension CallViewController: CallDetailsViewDelegate {
    
    func noteViewSelected(open: Bool) {

    }
    
    func callCanceledButtonPressed() {
        
    }
    
    func addUserToCallPressed(_ uID: String) {
        
    }
    
    func alertUserParticipate() {
        
    }
    
    func scheduleCallButtonPressed() {
        
    }
    
    func callNowButtonPressed() {
        
    }
    
}
