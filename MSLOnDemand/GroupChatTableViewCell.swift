//
//  GroupChatTableViewCell.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/15/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class GroupChatTableViewCell: UITableViewCell {

    @IBOutlet weak var lblUserText: UILabel!
    @IBOutlet weak var lblUserName: UILabel!

    override func awakeFromNib() {
        
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
}
