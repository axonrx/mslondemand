//
//  AppConfig.swift
//  MLS Connect
//
//  Created by Chris Mutkoski on 4/21/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation

let videoWidth : CGFloat = 320
let videoHeight : CGFloat = 240

let ApiKey = "45103162"

// Global Signal Notifications
let incomingVideoGroupCallNotificationKey = "com.axonrx.incomingVideoGroupCall"
let incomingPresenterGroupCallNotificationKey = "com.axonrx.incomingPresenterGroupCall"
let incomingClientMuteNotificationKey = "com.axonrx.incomingClientMute"
let incomingClientUnMuteNotificationKey = "com.axonrx.incomingClientUnMute"
let incomingAudioGroupCallNotificationKey = "com.axonrx.incomingAudioGroupCall"
let incomingScreenShareNotificationKey = "com.axonrx.incomingScreenShare"
let presenterDisconnectUserGroupCallNotificationKey = "com.axonrx.presenterDisconnectUserGroupCall"
let presenterSwitchVideoToUserGroupCallNotificationKey = "com.axonrx.presenterSwitchVideoToUserGroupCall"
let incomingMeetingLinkPressedNotificationKey = "com.axonrx.incomingMeetingLink"
let incomingGroupChatNotificationKey = "com.axonrx.incomingGroupChat"
let incomingGroupQuestionNotificationKey = "com.axonrx.incomingGroupQuestion"
let incomingGroupLeaderQuestionNotificationKey = "com.axonrx.incomingGroupLeaderQuestion"
let incomingGroupRejectNotificationKey = "com.axonrx.incomingGroupReject"
let incomingCalendarUpdateNotificationKey = "com.axonrx.incomingCalendarUpdate"
let incomingUserStatusPingNotificationKey = "com.axonrx.incomingUserStatusPing"

// GUI Update Notifications
let incomingrefeshUserProfileInfoNotificationKey = "com.axonrx.refeshUserProfileInfo"
let updateFolderNotificationKey = "com.axonrx.updateFolderList"
let updateContactsForTagNotificationKey = "com.axonrx.updateContactsForTag"
let updateUserStatusNotificationKey = "com.axonrx.updateUserStatus"
let updateCallBaudwidthStatusNotificationKey = "com.axonrx.updateCallBaudwidthStatus"
let updateGroupCallsCompleteNotificationKey = "com.axonrx.updateGroupCallsComplete"
let updateRemoveUserFromCallNotificationKey = "com.axonrx.updateRemoveUserFromCall"

// Global objects
var globalSignal : GlobalSignalHandler = GlobalSignalHandler()
var userList: UserList = UserList()
var groupCallList: GroupCallManager = GroupCallManager()
var applicationSetup: ApplicationSetup = ApplicationSetup()
var documentManager: DocumentManager = DocumentManager()
var folderManager: FolderManager = FolderManager()
var chatManager: ChatManager = ChatManager()
//var groupTagManager: GroupTagManager = GroupTagManager()
var pushNotifications: PushNotifications = PushNotifications()

var currentGroupCallID: String = String()
var currentGroupCallLeadUiD: String = String()

// KSideMenu
let sideMenuVC = KSideMenuVC()
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let kConstantObj = kConstant()

// Constants
let userStatusTimeout = 10.0

struct MSLConnect_Constants {

    static let baseURL = "https://mslconnect.axonrx.com/function_api.php"
    static let apiToken = "iueghvisudgvisudgv^654sdb6_9@6s5dv1*"
    
}

// MARK: - Device Information

enum UIUserInterfaceIdiom : Int {

    case unspecified
    case phone
    case pad
}

enum UserOnlineStatus : Int {
    case offline = 0
    case onlineAvailable
    case onlineOnCall
    case onlineOnBusy
}

enum UserCallState : Int {
    case none = 0
    case disconnected
    case connected
    case rejected
}

enum GroupCallType : Int {
    case groupCall = 0
    case presentationCall
    case canceledCall
    case audioCall
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}
