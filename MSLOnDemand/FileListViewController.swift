//
//  FileListViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/14/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import MBProgressHUD
import MobileCoreServices


class FileListViewController: UIViewController {
    
    @IBOutlet weak var tblViewFileList: UITableView!
    @IBOutlet weak var btnAddToCall: UIButton!

    var selectedFileUrl: URL?
    var delegate:DocumentsViewDelegate?
    var selectedFolder = 0
    var selectedDocument = 0
    var documentArray = [Document]()

    var hideAddFolderToCall = true

    var hud: MBProgressHUD?
    
    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if hideAddFolderToCall == true {
            
            btnAddToCall.isHidden = true
        }
        else {
            
            btnAddToCall.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func addDocumentToCallButtonPressed(_ sender: AnyObject) {
    
        let document = self.documentArray[selectedDocument]
        delegate!.addDocumentToCall(document.dID)
    }

    @IBAction func addDocumentButtonPressed(_ sender: AnyObject) {
    
        let importMenu = UIDocumentMenuViewController(documentTypes:[kUTTypeContent as String], in: .import)
        importMenu.delegate = self
        importMenu.popoverPresentationController?.sourceView = self.view
        present(importMenu, animated: true, completion: nil)
    }

    // MARK: - Class Methods

    func showFolder(_ row: Int) {

        selectedFolder = row

        if selectedFolder == 0 {
            
            documentArray = documentManager.documentList
        }
        else {
            
            // Walk docs for folder and show
            let folder = folderManager.folderList[row - 1]
            if folder.dIDs.count == 0 {
                
                documentArray = [Document]()
            }
            else {
                
                documentArray.removeAll()

                // Match doc Id's with all document array -> Make funtion in DocumentManager
                for document: Document in documentManager.documentList {
                    
                    if folder.dIDs.contains(document.dID) {
                        
                        documentArray.append(document)
                    }
                }
            }
        }

        tblViewFileList.reloadData()
    }
    
    func deleteDocumentComplete(_ result: String) {
        
        documentArray = documentManager.documentList
        tblViewFileList.reloadData()
    }

}

// MARK: - UITableViewDataSource and UITableViewDelegate

extension FileListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return documentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileTableViewCell", for: indexPath) as! FileTableViewCell

        let document = documentArray[indexPath.row]
        
        cell.lblFileName.text = document.name
        
        cell.imgViewFile.image = UIImage(named: "file_list_view_document")
        if document.name.contains("http:") || document.name.contains("https:") {
            
            cell.imgViewFile.image = UIImage(named: "file_list_website")
        }
        else if document.name.contains(".doc") || document.name.contains(".docx") {
            
            cell.imgViewFile.image = UIImage(named: "file_list_word")
        }
        else if document.name.contains(".ppsx") || document.name.contains(".ppt") || document.name.contains(".pptx") {
            
            cell.imgViewFile.image = UIImage(named: "file_list_powerpoint")
        }
        else if document.name.contains(".pdf") {
            
            cell.imgViewFile.image = UIImage(named: "file_list_pdf")
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedDocument = indexPath.row
        let document = self.documentArray[selectedDocument]
        delegate!.newFileSelected(document.dID)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        var delete: UITableViewRowAction?

        if selectedFolder == 0 {
            
            delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
                
                //print("document delete")
                
                let document = self.documentArray[indexPath.row]
                self.delegate?.documentDeletedUpdateFolders(document.dID)
                documentManager.deleteDocument(document.dID, completion: self.deleteDocumentComplete)
                self.documentArray.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                
                if self.documentArray.count > 0 {
                    
                    let document = self.documentArray[0]
                    self.delegate!.newFileSelected(document.dID)
                }
            }
        }
        else {
            
            delete = UITableViewRowAction(style: .normal, title: "Remove") { action, index in
                
                //print("folder document remove")
                
                let document = self.documentArray[indexPath.row]
                self.delegate?.documentDeletedUpdateFolders(document.dID)

                let folder = folderManager.folderList[indexPath.row]
                folderManager.removeDocumentFromFolder(folder.fID, dID: document.dID)

                self.documentArray.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)

                if self.documentArray.count > 0 {
                    
                    let document = self.documentArray[0]
                    self.delegate!.newFileSelected(document.dID)
                }
            }
        }

        let addToFolder = UITableViewRowAction(style: .normal, title: "Add To Folder") { action, index in
            
            //print("Add to folder")
            let document = self.documentArray[indexPath.row]
            self.delegate!.fileSelectedMoveToFolder(document.dID)
        }

        delete!.backgroundColor = UIColor.red
        addToFolder.backgroundColor = UIColor.blue
        
        return [delete!, addToFolder]
    }

    func documentRemovedUpdateFolder(_ result: String!) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: updateFolderNotificationKey), object: self)
    }

    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        
        //print("didEndEditingRowAtIndexPath")
        self.delegate!.fileSelectedMoveToFolder("")
    }
}

extension FileListViewController: UIDocumentMenuDelegate {
    
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
        
    }
    
}

extension FileListViewController: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        // Save the selected file url for later reference
        selectedFileUrl = url;
        
        let uID = userList.getLoginUserID()
        
        hud = MBProgressHUD.showAdded(to: (self.parent?.view)!, animated:true)
        hud!.mode = MBProgressHUDMode.indeterminate
        hud!.label.text = "Uploading..."
        
        let fileName = selectedFileUrl!.lastPathComponent
        
        let notes = delegate!.getFilePreviewNotes()

        documentManager.uploadFileToAxonServer(uID, fileName: fileName,  notes: notes, fileUrl: selectedFileUrl!, hud:hud!, completion: importDocumentComplete)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
    }
    
    func importDocumentComplete(_ result: String) {

        hud?.hide(animated: true)
        
        if result == "success" {
            
            //print("Import Document to Axon server successful")
            documentArray = documentManager.documentList
            tblViewFileList.reloadData()
        }
    }

}
