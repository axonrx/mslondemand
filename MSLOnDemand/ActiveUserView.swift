//
//  ActiveUserView.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 8/26/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class ActiveUserView: UIView {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgViewAvatar: UIImageView!

    class func instanceFromNib() -> UIView {

        return UINib(nibName: "ActiveUserView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

}
