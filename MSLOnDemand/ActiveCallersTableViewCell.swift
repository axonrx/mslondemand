//
//  ActiveCallersTableViewCell.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/15/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class ActiveCallersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserCallState: UILabel!    
    @IBOutlet weak var imgAvatar: UIImageView!

    override func awakeFromNib() {
        
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
}
