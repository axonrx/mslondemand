//
//  DocumentTableViewCell.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 8/24/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class DocumentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblDocumentName: UILabel!
    @IBOutlet weak var lblDocumentUploadDate: UILabel!
    @IBOutlet weak var lblDocumentNotes: UILabel!
    @IBOutlet weak var wvDocument: UIWebView!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
}
