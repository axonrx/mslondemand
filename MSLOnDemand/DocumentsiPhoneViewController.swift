//
//  DocumentsiPhoneViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 8/24/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import MBProgressHUD
import MobileCoreServices


class DocumentsiPhoneViewController: UIViewController {

    @IBOutlet weak var tblViewFileList: UITableView!
    @IBOutlet weak var btnAddDocument: UIButton!
    @IBOutlet weak var imgMenuButton: UIButton!

    var delegate:CalliPhoneViewDelegate?

    var selectedFileUrl: URL?
    var selectedDocument = 0
    var documentArray = [Document]()

    var hud: MBProgressHUD?

    var isVideoCall = false

    override func viewDidLoad() {
        
        super.viewDidLoad()

        Gradient.setBackgroundGradient(self.view)

        documentArray = documentManager.documentList
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        if isVideoCall == true {
            
            imgMenuButton.isHidden = true
            btnAddDocument.isHidden = true
        }
        else {
            
            imgMenuButton.isHidden = false
            btnAddDocument.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions Methods
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        
        sideMenuVC.toggleMenu()
    }
    
    @IBAction func addDocumentButtonPressed(_ sender: AnyObject) {
    
        let importMenu = UIDocumentMenuViewController(documentTypes:[kUTTypeContent as String], in: .import)
        importMenu.delegate = self
        importMenu.popoverPresentationController?.sourceView = self.view
        present(importMenu, animated: true, completion: nil)
    }
    
    // MARK: - Local Completion Methods
    
    func deleteDocumentComplete(_ result: String) {
        
        documentArray = documentManager.documentList
        tblViewFileList.reloadData()
    }
    
    // MARK: - Notification Methods
    
}

// MARK: - UITableViewDataSource and UITableViewDelegate

extension DocumentsiPhoneViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if documentArray.count == 0 && isVideoCall == false {
            
            return "Add Documents"
        }
        else {

            return "Documents List"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if documentArray.count == 0 && isVideoCall {
            
            return 1
        }
        else {
            
            return documentArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentTableViewCell", for: indexPath) as! DocumentTableViewCell
        
        if documentArray.count == 0 && isVideoCall {
            
            cell.lblDocumentName.text = "No Documents Loaded."
            cell.lblDocumentUploadDate.text = ""
            cell.lblDocumentNotes.text = "Select to return or Add document below"
            cell.imageView?.isHidden = true
        }
        else {
            
            let document = documentArray[indexPath.row]
            
            cell.imageView?.isHidden = false

            cell.lblDocumentName.text = document.name
            cell.lblDocumentUploadDate.text = document.date_uploaded
            cell.lblDocumentNotes.text = document.notes
            
            let urlwithPercentEscapes = document.path.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            cell.wvDocument.loadRequest(URLRequest(url: URL(string: urlwithPercentEscapes!)!))
            cell.wvDocument.scalesPageToFit = true
            cell.wvDocument.stringByEvaluatingJavaScript(from: "document.body.style.zoom = 5.0;")
            cell.wvDocument.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if documentArray.count == 0  && isVideoCall {
            
            self.dismiss(animated: true, completion: nil)
        }
        else {
            
            selectedDocument = indexPath.row
            let document = self.documentArray[selectedDocument]
            delegate!.newFileSelected(document.dID)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
                
                //print("document delete")
                
                let document = self.documentArray[indexPath.row]
                documentManager.deleteDocument(document.dID, completion: self.deleteDocumentComplete)
                self.documentArray.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        delete.backgroundColor = UIColor.red
        
        return [delete]
    }
    
    func documentRemovedUpdateFolder(_ result: String!) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: updateFolderNotificationKey), object: self)
    }
    
}

extension DocumentsiPhoneViewController: UIDocumentMenuDelegate {
    
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
        
    }
    
}

extension DocumentsiPhoneViewController: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        // Save the selected file url for later reference
        selectedFileUrl = url;
        
        let uID = userList.getLoginUserID()
        
         if self.parent?.view != nil {
            
            hud = MBProgressHUD.showAdded(to: (self.parent?.view)!, animated:true)
        }
        else {
            
            hud = MBProgressHUD.showAdded(to: self.view, animated:true)
       }
        
        hud!.mode = MBProgressHUDMode.indeterminate
        hud!.label.text = "Uploading..."
        
        let fileName = selectedFileUrl!.lastPathComponent

        documentManager.uploadFileToAxonServer(uID, fileName: fileName,  notes: "", fileUrl: selectedFileUrl!, hud:hud!, completion: importDocumentComplete)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
        hud?.hide(animated: true)
    }
    
    func importDocumentComplete(_ result: String) {
        
        hud?.hide(animated: true)
        
        if result == "success" {
            
            //print("Import Document to Axon server successful")
            documentArray = documentManager.documentList
            tblViewFileList.reloadData()
        }
    }
    
}
