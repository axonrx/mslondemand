//
//  UserProfileViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 6/30/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import MBProgressHUD


class UserProfileViewController: UIViewController {
    
    @IBOutlet weak var vwAvatar: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblServerErrorMessage: UILabel!
    @IBOutlet weak var btnSignOut: UIButton!

    var avatarCaptureController: ZCSAvatarCaptureController? = nil
    var activeField: UITextField? = nil

    var firstNameLabel: JJMaterialTextfield? = nil
    var lastNameLabel: JJMaterialTextfield? = nil
    var phoneNumberLabel: JJMaterialTextfield? = nil
    var passwordLabel: JJMaterialTextfield? = nil
    var jobTitleLabel: JJMaterialTextfield? = nil
    var credentialsLabel: JJMaterialTextfield? = nil
    var programsLabel: JJMaterialTextfield? = nil
    var emailLabel: JJMaterialTextfield? = nil
    var cellLabel: JJMaterialTextfield? = nil
    var faxLabel: JJMaterialTextfield? = nil
    var locationLabel: JJMaterialTextfield? = nil

    var hud: MBProgressHUD?
    
    var showSignOutButton = false

    // MARK: - UIViewController handlers

    override func viewDidLoad() {
        
        super.viewDidLoad()

        if showSignOutButton == true {

            btnSignOut.isHidden = false
        }
        else {
            
            btnSignOut.isHidden = true
        }

        lblServerErrorMessage.isHidden = true

        self.view.backgroundColor = UIColor.clear
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.view.insertSubview(blurEffectView, at: 0)

        NotificationCenter.default.addObserver(self, selector: #selector(UserProfileViewController.keyboardWillShowNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UserProfileViewController.keyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        // Update login user image and user name
        let userPicPath = userList.userLoggedIn?.picPath
        
        Alamofire.request(userPicPath!)
            .responseImage { response in
                //debugPrint(response)
                //print(response.request)
                //print(response.response)
                //debugPrint(response.result)
                
                if let image = response.result.value {
                    
                    //print("image downloaded: \(image)")
                    //let radius: CGFloat = 20.0
                    //let roundedImage = image.af_imageWithRoundedCornerRadius(radius)
                    let circularImage = image.af_imageRoundedIntoCircle()
                    
                    self.avatarCaptureController = ZCSAvatarCaptureController()
                    self.avatarCaptureController!.delegate = self
                    self.avatarCaptureController!.image = circularImage
                    self.vwAvatar.addSubview(self.avatarCaptureController!.view)
                }
        }

        layoutTextFields()
    }
    
    override func viewWillDisappear(_ animated: Bool) {

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator);
        
        coordinator.animate(alongsideTransition: nil, completion: {
            _ in
            
            self.adjustTextFields()
            self.avatarCaptureController!.adjustSubViews()
            //self.vwAvatar.setNeedsDisplay()
        })
    }

    override var prefersStatusBarHidden : Bool {
        
        return true
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Keyboard Notifications

    func keyboardWillShowNotification(_ notification: Notification) {

        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeFieldPresent = activeField {
            
            if !aRect.contains(activeFieldPresent.frame.origin) {

                self.scrollView.scrollRectToVisible(activeFieldPresent.frame, animated: true)
            }
        }
    }
    
    func keyboardWillHideNotification(_ notification: Notification) {

        let desiredOffset = CGPoint(x: 0, y: -self.scrollView.contentInset.top)
        self.scrollView.setContentOffset(desiredOffset, animated: true)
    }

    // MARK: - IBActions
    
    @IBAction func closeButtonPressed(_ sender: AnyObject) {

        let user = User(uID: userList.getLoginUserID(), firstName: firstNameLabel!.text!, lastName: lastNameLabel!.text!, credentials: credentialsLabel!.text!, jobTitle: jobTitleLabel!.text!, picPath: "", lastSeen: 0.0, available: true, phoneNumber: phoneNumberLabel!.text!, email: emailLabel!.text!, cell: cellLabel!.text!, fax: faxLabel!.text!, location: locationLabel!.text!, techniques: (userList.userLoggedIn?.techniques)!, focus: (userList.userLoggedIn?.focus)!, programs: programsLabel!.text!, experience: (userList.userLoggedIn?.experience)!, education: (userList.userLoggedIn?.education)!,
                        callState: UserCallState.none)

        userList.updateUser(user, completion: updateUserResult)
        lblServerErrorMessage.isHidden = true
    }

    @IBAction func signOutButtonPressed(_ sender: AnyObject) {

        UserDefaults.standard.set(false, forKey: "userLoggedIn")

        // Close and go to login View
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController")

        lblServerErrorMessage.isHidden = true

        let window = UIApplication.shared.windows[0] as UIWindow;
        UIView.transition(
            from: window.rootViewController!.view,
            to: vc.view,
            duration: 0.65,
            options: .transitionCrossDissolve,
            completion: {
                finished in //window.rootViewController = vc
                UIApplication.shared.keyWindow?.rootViewController = vc;
        })
    }

    // MARK: - Local Completion Methods

    func updateUserResult(_ result: String, code: String) {
        
        if result == "success" {

            // Update user list - Will update once view closes and MainViewController willAppear
            userList.getUpdatedUsersList(uploadUserListComplete)
        }
        else {
            
            lblServerErrorMessage.isHidden = false
            lblServerErrorMessage.text = code
            print(code)
        }
    }

    // MARK: - Local Methods
    
    func layoutTextFields() {
        
        let labelWidth = self.view.bounds.width / 2
        let controlSpacing: CGFloat = 40.0
        
        firstNameLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: lblServerErrorMessage.frame.origin.y + controlSpacing, width: labelWidth, height: 60))
        firstNameLabel!.textColor = UIColor.black
        firstNameLabel!.enableMaterialPlaceHolder = true
        firstNameLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        firstNameLabel!.placeholder = "First Name"
        firstNameLabel!.lineColor = UIColor.black
        firstNameLabel!.tintColor = UIColor.black
        firstNameLabel!.autocapitalizationType = .none
        firstNameLabel!.autocorrectionType = .no
        firstNameLabel!.delegate = self
        firstNameLabel!.text = (userList.userLoggedIn?.firstName)!
        self.scrollView.addSubview(firstNameLabel!)
        
        lastNameLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: firstNameLabel!.frame.origin.y + firstNameLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60))
        lastNameLabel!.textColor = UIColor.black
        lastNameLabel!.enableMaterialPlaceHolder = true
        lastNameLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        lastNameLabel!.placeholder = "Last Name"
        lastNameLabel!.lineColor = UIColor.black
        lastNameLabel!.tintColor = UIColor.black
        lastNameLabel!.autocapitalizationType = .none
        lastNameLabel!.autocorrectionType = .no
        lastNameLabel!.delegate = self
        lastNameLabel!.text = (userList.userLoggedIn?.lastName)!
        self.scrollView.addSubview(lastNameLabel!)
        
        phoneNumberLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: lastNameLabel!.frame.origin.y + lastNameLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60))
        phoneNumberLabel!.textColor = UIColor.black
        phoneNumberLabel!.enableMaterialPlaceHolder = true
        phoneNumberLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        phoneNumberLabel!.placeholder = "Phone Number"
        phoneNumberLabel!.lineColor = UIColor.black
        phoneNumberLabel!.tintColor = UIColor.black
        phoneNumberLabel!.autocapitalizationType = .none
        phoneNumberLabel!.autocorrectionType = .no
        phoneNumberLabel!.delegate = self
        phoneNumberLabel!.text = (userList.userLoggedIn?.phoneNumber)!
        self.scrollView.addSubview(phoneNumberLabel!)
        
        passwordLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: phoneNumberLabel!.frame.origin.y + phoneNumberLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60))
        passwordLabel!.textColor = UIColor.black
        passwordLabel!.enableMaterialPlaceHolder = true
        passwordLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        passwordLabel!.placeholder = "Password"
        passwordLabel!.lineColor = UIColor.black
        passwordLabel!.tintColor = UIColor.black
        passwordLabel!.autocapitalizationType = .none
        passwordLabel!.autocorrectionType = .no
        passwordLabel!.delegate = self
        passwordLabel!.text = ""
        self.scrollView.addSubview(passwordLabel!)
        
        jobTitleLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: passwordLabel!.frame.origin.y + passwordLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60))
        jobTitleLabel!.textColor = UIColor.black
        jobTitleLabel!.enableMaterialPlaceHolder = true
        jobTitleLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        jobTitleLabel!.placeholder = "Job Title"
        jobTitleLabel!.lineColor = UIColor.black
        jobTitleLabel!.tintColor = UIColor.black
        jobTitleLabel!.autocapitalizationType = .none
        jobTitleLabel!.autocorrectionType = .no
        jobTitleLabel!.delegate = self
        jobTitleLabel!.text = (userList.userLoggedIn?.jobTitle)!
        self.scrollView.addSubview(jobTitleLabel!)
        
        credentialsLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: jobTitleLabel!.frame.origin.y + jobTitleLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60))
        credentialsLabel!.textColor = UIColor.black
        credentialsLabel!.enableMaterialPlaceHolder = true
        credentialsLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        credentialsLabel!.placeholder = "Credentials"
        credentialsLabel!.lineColor = UIColor.black
        credentialsLabel!.tintColor = UIColor.black
        credentialsLabel!.autocapitalizationType = .none
        credentialsLabel!.autocorrectionType = .no
        credentialsLabel!.delegate = self
        credentialsLabel!.text = (userList.userLoggedIn?.credentials)!
        self.scrollView.addSubview(credentialsLabel!)
        
        programsLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: credentialsLabel!.frame.origin.y + credentialsLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60))
        programsLabel!.textColor = UIColor.black
        programsLabel!.enableMaterialPlaceHolder = true
        programsLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        programsLabel!.placeholder = "Programs"
        programsLabel!.lineColor = UIColor.black
        programsLabel!.tintColor = UIColor.black
        programsLabel!.autocapitalizationType = .none
        programsLabel!.autocorrectionType = .no
        programsLabel!.delegate = self
        programsLabel!.text = (userList.userLoggedIn?.programs)!
        self.scrollView.addSubview(programsLabel!)
        
        cellLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: programsLabel!.frame.origin.y + programsLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60))
        cellLabel!.textColor = UIColor.black
        cellLabel!.enableMaterialPlaceHolder = true
        cellLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        cellLabel!.placeholder = "Cell"
        cellLabel!.lineColor = UIColor.black
        cellLabel!.tintColor = UIColor.black
        cellLabel!.autocapitalizationType = .none
        cellLabel!.autocorrectionType = .no
        cellLabel!.delegate = self
        cellLabel!.text = (userList.userLoggedIn?.cell)!
        self.scrollView.addSubview(cellLabel!)
        
        emailLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: cellLabel!.frame.origin.y + cellLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60))
        emailLabel!.textColor = UIColor.black
        emailLabel!.enableMaterialPlaceHolder = true
        emailLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        emailLabel!.placeholder = "Email"
        emailLabel!.lineColor = UIColor.black
        emailLabel!.tintColor = UIColor.black
        emailLabel!.autocapitalizationType = .none
        emailLabel!.autocorrectionType = .no
        emailLabel!.delegate = self
        emailLabel!.text = (userList.userLoggedIn?.email)!
        self.scrollView.addSubview(emailLabel!)
        
        faxLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: emailLabel!.frame.origin.y + emailLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60))
        faxLabel!.textColor = UIColor.black
        faxLabel!.enableMaterialPlaceHolder = true
        faxLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        faxLabel!.placeholder = "Fax"
        faxLabel!.lineColor = UIColor.black
        faxLabel!.tintColor = UIColor.black
        faxLabel!.autocapitalizationType = .none
        faxLabel!.autocorrectionType = .no
        faxLabel!.delegate = self
        faxLabel!.text = (userList.userLoggedIn?.fax)!
        self.scrollView.addSubview(faxLabel!)
        
        locationLabel = JJMaterialTextfield(frame: CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: faxLabel!.frame.origin.y + faxLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60))
        locationLabel!.textColor = UIColor.black
        locationLabel!.enableMaterialPlaceHolder = true
        locationLabel!.errorColor = UIColor.init(red: 0.910, green: 0.329, blue: 0.271, alpha: 1.000)
        locationLabel!.placeholder = "Location"
        locationLabel!.lineColor = UIColor.black
        locationLabel!.tintColor = UIColor.black
        locationLabel!.autocapitalizationType = .none
        locationLabel!.autocorrectionType = .no
        locationLabel!.delegate = self
        locationLabel!.text = (userList.userLoggedIn?.location)!
        self.scrollView.addSubview(locationLabel!)
        
        self.scrollView.contentSize = CGSize(width: self.view.bounds.width, height: locationLabel!.frame.origin.y + locationLabel!.frame.size.height + controlSpacing)
    }

    func adjustTextFields() {
        
        let labelWidth = self.view.bounds.width / 2
        let controlSpacing: CGFloat = 40.0
        
        firstNameLabel!.frame = CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: lblServerErrorMessage.frame.origin.y + controlSpacing, width: labelWidth, height: 60)
        
        lastNameLabel!.frame = CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: firstNameLabel!.frame.origin.y + firstNameLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60)
        
        phoneNumberLabel!.frame = CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: lastNameLabel!.frame.origin.y + lastNameLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60)
        
        passwordLabel!.frame = CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: phoneNumberLabel!.frame.origin.y + phoneNumberLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60)
        
        jobTitleLabel!.frame = CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: passwordLabel!.frame.origin.y + passwordLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60)
        
        credentialsLabel!.frame = CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: jobTitleLabel!.frame.origin.y + jobTitleLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60)
        
        cellLabel!.frame = CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: credentialsLabel!.frame.origin.y + credentialsLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60)
        
        emailLabel!.frame = CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: cellLabel!.frame.origin.y + cellLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60)
        
        faxLabel!.frame = CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: emailLabel!.frame.origin.y + emailLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60)
        
        locationLabel!.frame = CGRect(x: (self.view.bounds.width - labelWidth) / 2, y: faxLabel!.frame.origin.y + faxLabel!.frame.size.height + controlSpacing, width: labelWidth, height: 60)
        
        self.scrollView.contentSize = CGSize(width: self.view.bounds.width, height: locationLabel!.frame.origin.y + locationLabel!.frame.size.height + controlSpacing)
    }

}

// MARK: - ZCSAvatarCaptureControllerDelegate

extension UserProfileViewController: ZCSAvatarCaptureControllerDelegate {

    func imageSelected(_ image: UIImage) {

        let newImage = image.imageWithSize(CGSize(width: 500, height: 500))

        // Upload new image to the server
        if let data = UIImagePNGRepresentation(newImage) {
            
            let fileName = String(format: "%@_%@", ProcessInfo.processInfo.globallyUniqueString, "avatar.png")
            let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
            try? data.write(to: URL(fileURLWithPath: fileURL.path), options: [.atomic])

            hud = MBProgressHUD.showAdded(to: self.view, animated:true)
            hud!.mode = MBProgressHUDMode.indeterminate
            hud!.label.text = "Uploading Avatar..."

            UploadAvatarImage.uploadAvatarImageToAxonServer(userList.getLoginUserID(), fileUrl: fileURL, hud: hud!, completion: uploadAvatarImageComplete)
        }
    }

    func imageSelectionCancelled() {
        
        //print("imageSelectionCancelled");
    }

    func uploadAvatarImageComplete(_ result: String) {
        
        if result == "success" {
            
            print("Upload Avatar Image to Axon server successful")
            
            // Update user list
            userList.getUpdatedUsersList(uploadUserListComplete)
        }
        else {
            
            hud?.hide(animated: true)

            let alert = UIAlertController(title: "Avatar Error", message: result, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func uploadUserListComplete(_ result: String, code: String) {
        
        hud?.hide(animated: true)

        if result == "success" {
            
            print("Updated user list successful")

            NotificationCenter.default.post(name: Notification.Name(rawValue: incomingrefeshUserProfileInfoNotificationKey), object: nil)

            self.dismiss(animated: true, completion: nil)
        }
    }

}

// MARK: - UITextFieldDelegate

extension UserProfileViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return false
    }

}
