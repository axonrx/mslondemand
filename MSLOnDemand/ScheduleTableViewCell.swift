//
//  ScheduleTableViewCell.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 8/24/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class ScheduleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgViewCallType: UIImageView!
    @IBOutlet weak var lblCallDateTime: UILabel!
    @IBOutlet weak var lblCallTitle: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
}
