//
//  ScheduledCallListViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 8/24/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import MBProgressHUD
import MobileCoreServices


class ScheduledCallListViewController: UIViewController {

    @IBOutlet weak var tblScheduledCalls: UITableView!
    
    var scheduledEvents = [GroupCall]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        scheduledEvents = groupCallList.getGroupCallScheduledList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Public Methods
    
    func reloadScheduledCallsList() {
        
        scheduledEvents = groupCallList.getGroupCallScheduledList()
        tblScheduledCalls.reloadData()
    }
}

// MARK: - UITableViewDataSource and UITableViewDelegate

extension ScheduledCallListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Upcoming Calls"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return scheduledEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleTableViewCell", for: indexPath) as! ScheduleTableViewCell
        
        let scheduledEvent = scheduledEvents[indexPath.row]

        cell.lblCallTitle.text = scheduledEvent.groupCallName
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.timeZone = TimeZone(identifier: "UTC")
        
        if let date = df.date(from: scheduledEvent.scheduledCallTime) {
            
            df.timeZone = TimeZone.autoupdatingCurrent
            df.dateFormat = "yyyy-MM-dd h:mm a"
            let twelveHourDateTime = df.string(from: date)
            cell.lblCallDateTime.text = twelveHourDateTime
        }

        cell.imgViewCallType.image = UIImage(named: "call-type-canceled")
        if (scheduledEvent.callType == .audioCall) {
            
            cell.imgViewCallType.image = UIImage(named: "call-type-audio")
        }
        else if (scheduledEvent.callType == .groupCall) {
            
            cell.imgViewCallType.image = UIImage(named: "call-type-groupcall")
        }
        else if (scheduledEvent.callType == .presentationCall) {
            
            cell.imgViewCallType.image = UIImage(named: "call-type-presentation")
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        // See if can make call now. Tell user if can't
        let scheduledEvent = scheduledEvents[indexPath.row]

        // NOTE(tsd): Check timing first?
/*
        let callDateTime = scheduledEvent.scheduledCallTime

        let date : NSDate = callDateTime.StringDateTimeDate()!

        let currentCallDateTime = NSDate()
        
        var userMadeChanges = false
        switch date.compare(currentCallDateTime) {
        case .OrderedAscending:
            userMadeChanges = true
        case .OrderedDescending:
            userMadeChanges = true
        case .OrderedSame:
            userMadeChanges = false
        }
*/

        let storyboard = UIStoryboard(name: "CalliPhone", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CalliPhoneViewController") as! CalliPhoneViewController
        vc.modalPresentationStyle = .fullScreen
        vc.selectedGroupCallID = scheduledEvent.groupCallID
        vc.callType = scheduledEvent.callType
        if scheduledEvent.callType == .presentationCall {
            
            vc.isPresentation = true
        }
        else {
            
            vc.isPresentation = false
        }
        vc.isRemoteRequestForGroupCall = false
        vc.isEmailLinkGroupCall = false
        self.present(vc, animated: true, completion: nil)

    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
}
