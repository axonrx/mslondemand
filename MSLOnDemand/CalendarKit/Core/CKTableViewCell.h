//
//  CKTableViewCell.h
//  MBCalendarKit
//
//  Created by Rachel Hyman on 6/2/14.
//  Copyright (c) 2014 Moshe Berman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CKTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgViewCallType;
@property (weak, nonatomic) IBOutlet UILabel *lblCallTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCallTime;
@property (weak, nonatomic) IBOutlet UITextView *txtNotes;

@end
