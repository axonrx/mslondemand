//
//  FilePreviewViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/14/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class FilePreviewViewController: UIViewController {
    
    @IBOutlet weak var webViewFilePreview: UIWebView!

    var dID: String?
    
    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        webViewFilePreview.delegate = self
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Get Data Methods

    func showDocument(_ dID: String) {

        if documentManager.documentList.count == 0 {
        
            return
        }
        
        var document = documentManager.documentList[0]
        if dID != "-1" {
            
            document = documentManager.getDocumentByID(dID)!
        }
        
        self.dID = document.dID
        
        let urlwithPercentEscapes = document.path.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        webViewFilePreview.loadRequest(URLRequest(url:URL(string: urlwithPercentEscapes!)!))
        webViewFilePreview.scalesPageToFit = true

        //print(document.path)
    }
    
    func getDocumentNotes() -> String {
    
        return ""
    }

    func updateDocumentNotesComplete(_ result: String) {
        
        if result == "success" {
            
            //print("Update Document notes to Axon server successful")
        }
    }

}

extension FilePreviewViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
    }
    
}
