//
//  Gradient.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/5/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//


class Gradient {

    static func setBackgroundGradient(_ view: UIView) {

        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [(UIColor(hexString: applicationSetup.getApplicationGradientStart())?.cgColor)!, (UIColor(hexString: applicationSetup.getApplicationGradientEnd())?.cgColor)!]
        gradient.locations = [0.0, 1.0]
        view.layer.insertSublayer(gradient, at: 0)
    }

}

extension UIColor {
    
    public convenience init?(hexString: String) {
        
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            
            let start = hexString.characters.index(hexString.startIndex, offsetBy: 1)
            let hexColor = hexString.substring(from: start)
            
            if hexColor.characters.count == 8 {
                
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}

