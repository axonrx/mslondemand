//
//  PresenterConnection.swift
//  MLS Connect
//
//  Created by Chris Mutkoski on 5/2/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation
import Alamofire


class PresenterConnection {
    
    var groupCallSessionID: String?
    var groupCallToken: String?
    
    func getPresenterGroupCallToken(groupCallID: String, name: String, completion: (result: String) -> Void) {
        
        let parameters = [
            "token":  MSLConnect_Constants.apiToken,
            "command": "GETPRESENTERGROUPCALLTOKEN",
            "groupCallID": groupCallID,
            "groupCallName": name
        ]
        
        let options = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        let jsonString = String(data: options, encoding: NSUTF8StringEncoding)
        let jsonParams = ["json" : jsonString as! AnyObject]
        
        Alamofire.request(.POST, MSLConnect_Constants.baseURL, parameters: jsonParams)
            .responseJSON { response in
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                
                if let JSON = response.result.value {
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON as! NSDictionary
                        
                        let result = response.objectForKey("result") as! NSDictionary
                        
                        self.groupCallSessionID = result["groupcall_session_id"] as? String
                        self.groupCallToken = result["token"] as? String
                        
                        completion(result: "success")
                    }
                    else {
                        
                        completion(result: JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion(result: "No data returned from server.")
                }
        }
    }
    
}
