//
//  GroupsViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 6/29/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
//import CloudTagView


class GroupsViewController: UIViewController {
/*
    @IBOutlet weak var groupTagsView: UIView!
    @IBOutlet weak var txtFieldAddGroupTag: UITextField!
    
    //let cloudView = CloudTagView()
    
    var allowGroupTagAdding = true

    // MARK: - UIViewController handlers

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //cloudView.frame = CGRectMake(0, 20, view.frame.width, 10)
        //cloudView.delegate = self
        
        //view.addSubview(cloudView)
        view.bringSubviewToFront(txtFieldAddGroupTag)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if allowGroupTagAdding == true {
            
            txtFieldAddGroupTag.hidden = false
        }
        else {
            
            txtFieldAddGroupTag.hidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Public Methods

    func reloadGroupTags() {

        //cloudView.tags.removeAll()
        //for groupTag: GroupTag in groupTagManager.groupTagList {
            
            //cloudView.tags.append(TagView(text: groupTag.text))
        //}
    }

}

extension GroupsViewController : TagViewDelegate {
    
    func tagDismissed(tag: TagView) {
        
        print("tag removed: " + tag.text)
        
        //func deleteGroupTag(gID: String, completion: (result: String) -> Void) {

        let tag = groupTagManager.getGroupTagByText(tag.text)
        if tag != nil {
            
            groupTagManager.deleteGroupTag((tag?.gID)!, completion: updatedDeletedGroupTags)
        }
    }
    
    func tagTouched(tag: TagView) {
        
        print("tag touched: " + tag.text)
        
        let parameters = ["tagText": tag.text ]

        NSNotificationCenter.defaultCenter().postNotificationName(updateContactsForTagNotificationKey, object: self, userInfo: parameters)
    }

    func updatedDeletedGroupTags(result: String) {
        
        self.reloadGroupTags()
    }

}

extension GroupsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if txtFieldAddGroupTag.text?.characters.count > 0 {
            
            var newTagText = txtFieldAddGroupTag.text! as String
            let lastChar = newTagText.characters.last!
            if lastChar == " " {
                
                newTagText = newTagText.substringToIndex(newTagText.endIndex.predecessor())
            }
            let tag = groupTagManager.getGroupTagByText(newTagText)
            if tag == nil {
                
                //let newGroupTag = TagView(text: txtFieldAddGroupTag.text!)
                //newGroupTag.marginTop = 20
                //cloudView.tags.append(newGroupTag)
                //cloudView.tags.append(TagView(text: txtFieldAddGroupTag.text!))

                groupTagManager.newGroupTag(newTagText, completion: updatedNewGroupTags)
                
                txtFieldAddGroupTag.text = ""
            }
            else {
                
                let alert = UIAlertController(title: "Tag", message: "This tag already exists. Please enter another tag.", preferredStyle: UIAlertControllerStyle.Alert)
                //alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:{ (UIAlertAction)in}))
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in }))
                self.presentViewController(alert, animated: true, completion: {})
            }
        }
        
        textField.resignFirstResponder()
        return true
    }

    func updatedNewGroupTags(result: String) {
        
        self.reloadGroupTags()
    }
*/
}
