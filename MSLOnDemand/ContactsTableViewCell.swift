//
//  ContactsTableViewCell.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/5/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class ContactsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgViewUserThumbnail: UIImageView!
    @IBOutlet weak var imgViewAvailable: UIImageView!
    @IBOutlet weak var btnAddToCall: UIButton!
    @IBOutlet weak var btnCallAudio: UIButton!
    @IBOutlet weak var btnCallVideo: UIButton!
    @IBOutlet weak var lblProgramLocation: UILabel!
    @IBOutlet weak var vwSelected: UIView!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
}
