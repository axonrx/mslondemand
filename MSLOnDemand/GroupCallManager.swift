//
//  GroupCallManager.swift
//  MLS Connect
//
//  Created by Chris Mutkoski on 4/21/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import Foundation
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



class GroupCallManager {
    
    var historyGroupCalls: [GroupCall] = [GroupCall]()     // Array of GroupCalls -> history
    var scheduledGroupCalls: [GroupCall] = [GroupCall]()     // Array of GroupCalls -> scheduled

    // Get all group calls that the user created
    func getUserGroupCalls(_ uID: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "GETGROUPCALLS",
            "uID": uID
        ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]

        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        self.processGroupCallDataFromServer(response, participate: false)
                        
                        completion("success")
                    }
                    else {
                        
                        completion("failed")
                    }
                }
                else {
                    
                    print("Error parsing GETGROUPCALLS response JSON")
                    completion("failed")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    // Get all group calls the user is part of
    func getAllUserGroupCalls(_ uID: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "GETGROUPCALLSUSER",
            "uID": uID
        ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        self.processGroupCallDataFromServer(response, participate: true)
                        
                        completion("success")
                    }
                    else {
                        
                        completion("failed")
                    }
                }
                else {
                    
                    print("Error parsing GETGROUPCALLSUSER response JSON")
                    completion("failed")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    // MARK: Basic Group Call API
    
    func postNewGroupCall(_ uID: String, name: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "NEWGROUPCALL",
            "uID": uID,
            "groupCallName": name
        ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func updateUsersGroupCall(_ groupCallID: String, users: Array <String>, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "UPDATEUSERGROUPCALL",
            "groupCallID": groupCallID,
            "users": users
        ] as [String : Any]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    // MARK: Detailed Group Call API

    func postNewGroupCallDetails(_ groupCallName: String, scheduled: String, users: Array <String>, notes: String, documents: Array <String>, type: GroupCallType, completion: @escaping (_ result: String, _ gID: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "NEWGROUPCALLDETAILS",
            "uID": userList.getLoginUserID(),
            "groupCallName": groupCallName,
            "scheduled": scheduled,
            "notes": notes,
            "users": users,
            "documents": documents,
            "type": String(type.rawValue)
        ] as [String : Any]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let groupCall_id = JSON["groupCall_id"] as! Int
                        
                        let response = JSON
                        self.processGroupCallDataFromServer(response, participate: false)
                        
                        completion("success", String(groupCall_id))
                    }
                    else {
                        
                        completion(JSON["code"] as! String, "")
                    }
                }
                else {
                    
                    completion("No data returned from server.", "")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed", response.result.error!.localizedDescription)
                break
                
            }
        }
    }

    func updateGroupCallDetails(_ groupCallID: String, completion: @escaping (_ result: String) -> Void) {
        
        let groupCallDetails = groupCallList.getGroupCallDetails(groupCallID)

        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "UPDATEGROUPCALLDETAILS",
            "uID": userList.getLoginUserID(),
            "groupCallID": groupCallID,
            "groupCallName": groupCallDetails!.groupCallName,
            "lastCallTime": groupCallDetails!.lastCallTime,
            "scheduled": groupCallDetails!.scheduledCallTime,
            "notes": groupCallDetails!.notes,
            "users": groupCallDetails!.users,
            "documents": groupCallDetails!.documents,
            "type": String(groupCallDetails!.callType.rawValue)
        ] as [String : Any]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        self.processGroupCallDataFromServer(response, participate: false)
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func updateGroupCallDetailsWithNotes(_ groupCallID: String, notes: String, completion: @escaping (_ result: String) -> Void) {
        
        let groupCallDetails = groupCallList.getGroupCallDetails(groupCallID)
        
        var groupCallName = ""
        var lastCallTime = ""
        var scheduled = ""
        var users = Array<String>()
        var documents = Array<String>()
        var type = "1"

        if groupCallDetails != nil {
            
            groupCallName = groupCallDetails!.groupCallName
            lastCallTime = groupCallDetails!.lastCallTime
            scheduled = groupCallDetails!.scheduledCallTime
            users = groupCallDetails!.users
            documents = groupCallDetails!.documents
            type = String(groupCallDetails!.callType.rawValue)
        }

        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "UPDATEGROUPCALLDETAILS",
            "uID": userList.getLoginUserID(),
            "groupCallID": groupCallID,
            "groupCallName": groupCallName,
            "lastCallTime": lastCallTime,
            "scheduled": scheduled,
            "notes": notes,
            "users": users,
            "documents": documents,
            "type": type
            ] as [String : Any]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        self.processGroupCallDataFromServer(response, participate: false)
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func endGroupCall(_ groupCallID: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "ENDGROUPCALL",
            "groupCallID": groupCallID,
        ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("ENDGROUPCALL JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func cancelGroupCall(_ groupCallID: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "CANCELGROUPCALL",
            "uID": userList.getLoginUserID(),
            "groupCallID": groupCallID,
            ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("CANCELGROUPCALL JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        self.processGroupCallDataFromServer(response, participate: false)
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func processGroupCallDataFromServer(_ response: NSDictionary, participate: Bool) {
        
        let result = response.object(forKey: "result") as! NSDictionary
        let totalGroupCalls = result.object(forKey: "total") as! Int
        //print(totalGroupCalls)
        
        if totalGroupCalls > 0 {

            if participate == false {
                
                self.historyGroupCalls.removeAll()
                self.scheduledGroupCalls.removeAll()
            }
            
            let groupCallsResult = result.object(forKey: "groupCalls") as! NSArray
            for groupCallItem in groupCallsResult as! [Dictionary<String, AnyObject>] {
                
                let groupCallID = groupCallItem["groupCallID"] as! String
                let groupCallName = groupCallItem["groupCallName"] as! String
                let groupCallCreator_uID = groupCallItem["uID"] as! String
                let num_users = groupCallItem["num_users"] as! String
                var users = Array<String>()
                if Int(num_users) > 0 {
                    
                    users = groupCallItem["users"] as! Array<String>
                }
                
                let lastCallTime = groupCallItem["lastCallTime"] as! String
                //print(lastCallTime)
                
                let notes = groupCallItem["notes"] as! String
                
                var scheduleD = groupCallItem["scheduled"] as! String
                //print(scheduleD)
                
                let num_documents = groupCallItem["num_documents"] as! String
                var documents = Array<String>()
                if groupCallCreator_uID == userList.getLoginUserID() {
                    
                    if Int(num_documents) > 0 {
                        
                        documents = groupCallItem["documents"] as! Array<String>
                    }

                    // Clean "f" folder ID's out. This is for web app only
                    var clean_documents = Array<String>()
                    for document: String in documents {
                        
                        if !document.contains("f") {
                            
                            clean_documents.append(document)
                        }
                    }
                    documents = clean_documents
                }
                
                let callTypeString = groupCallItem["type"] as! String
                let callType = GroupCallType(rawValue: Int(callTypeString)!)

                if scheduleD.range(of: "-00-") != nil {
                    
                    scheduleD = scheduleD.replacingOccurrences(of: "-00-", with: "-01-")
                }

                var scheduleDate = scheduleD.StringDateTimeDateCallDetails()
                if scheduleDate == nil {
                    
                    scheduleDate = Date()
                }
                var isHistory = false
                switch scheduleDate!.compare(Date()) {
                case .orderedAscending:
                    isHistory = true
                case .orderedDescending:
                    isHistory = false
                case .orderedSame:
                    isHistory = true
                }

                if callType == .canceledCall {
                    
                    isHistory = true
                }

                var userParticipate = true
                if groupCallCreator_uID == userList.getLoginUserID() {
                    
                    userParticipate = false
                }

                if isHistory == true {

                    self.historyGroupCalls.append(GroupCall(uID: groupCallCreator_uID,
                        groupCallID: groupCallID,
                        groupCallName: groupCallName,
                        users: users,
                        date: lastCallTime,
                        notes: notes,
                        localNotes: "",
                        scheduledTime: scheduleD,
                        documents: documents,
                        callType: callType!,
                        participate: userParticipate))
                }
                else {
                    
                    self.scheduledGroupCalls.append(GroupCall(uID: groupCallCreator_uID,
                        groupCallID: groupCallID,
                        groupCallName: groupCallName,
                        users: users,
                        date: lastCallTime,
                        notes: notes,
                        localNotes: "",
                        scheduledTime: scheduleD,
                        documents: documents,
                        callType: callType!,
                        participate: userParticipate))
                }
            }
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: updateGroupCallsCompleteNotificationKey), object: self, userInfo: nil)
        }
    }

    func getGroupCallLocalNotes(_ uID: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "GETUSERNOTES",
            "uID": uID,
            ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    print("GETUSERNOTES JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        let result = response.object(forKey: "result") as! NSDictionary
                        let totalNotesGroupCalls = result.object(forKey: "total") as! Int
                        
                        if totalNotesGroupCalls > 0 {
                            
                            let groupCallNotessResult = result.object(forKey: "notes") as! NSArray
                            for groupCallNotes in groupCallNotessResult as! [Dictionary<String, AnyObject>] {
                                
                                let gID = groupCallNotes["gID"] as! String
                                let note = groupCallNotes["note"] as! String
                                
                                self.addGroupCallLocalNotes(gID, localNotes: note);
                            }
                        }
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func newGroupCallLocalNotes(_ groupCallID: String, note: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "NEWUSERNOTE",
            "uID": userList.getLoginUserID(),
            "gID": groupCallID,
            "note": note,
            ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]

        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    //print("NEWUSERNOTE JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        let result = response.object(forKey: "result") as! NSDictionary
                        let totalNotesGroupCalls = result.object(forKey: "total") as! Int
                        
                        if totalNotesGroupCalls > 0 {
                            
                            let groupCallNotessResult = result.object(forKey: "notes") as! NSArray
                            for groupCallNotes in groupCallNotessResult as! [Dictionary<String, AnyObject>] {
                                
                                let gID = groupCallNotes["gID"] as! String
                                let note = groupCallNotes["note"] as! String
                                
                                self.addGroupCallLocalNotes(gID, localNotes: note);
                            }
                        }
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func updateGroupCallLocalNotes(_ groupCallID: String, note: String, completion: @escaping (_ result: String) -> Void) {
        
        let parameters = [
            "token": MSLConnect_Constants.apiToken,
            "command": "UPDATEUSERNOTE",
            "uID": userList.getLoginUserID(),
            "gID": groupCallID,
            "note": note,
            ]
        
        let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        let jsonString = String(data: options, encoding: String.Encoding.utf8)
        let jsonParams = ["json" : jsonString as AnyObject]
        
        Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    print("UPDATEUSERNOTE JSON: \(JSON)")
                    
                    if JSON["status"] as! String == "success" {
                        
                        let response = JSON
                        let result = response.object(forKey: "result") as! NSArray
                        
                        if result.count > 0 {
                            
                            for groupCallNote in result as! [Dictionary<String, AnyObject>] {
                                
                                let gID = groupCallNote["gID"] as! String
                                let note = groupCallNote["note"] as! String
                                
                                self.addGroupCallLocalNotes(gID, localNotes: note);
                            }
                        }
                        
                        completion("success")
                    }
                    else {
                        
                        completion(JSON["code"] as! String)
                    }
                }
                else {
                    
                    completion("No data returned from server.")
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                completion("failed")
                break
                
            }
        }
    }

    func getGroupCallHistoryList() -> [GroupCall] {
        
        return self.historyGroupCalls
    }

    func getGroupCallScheduledList() -> [GroupCall] {
        
        return self.scheduledGroupCalls
    }

    func getGroupCallDetails(_ groupCallID: String) -> GroupCall? {

        for groupCall: GroupCall in self.historyGroupCalls {
            
            if groupCall.groupCallID == groupCallID {
                
                return groupCall;
            }
        }

        for groupCall: GroupCall in self.scheduledGroupCalls {
            
            if groupCall.groupCallID == groupCallID {
                
                return groupCall;
            }
        }

        return nil
    }

    func addGroupCallLocalNotes(_ groupCallID: String, localNotes: String) {
        
        for groupCall: GroupCall in self.historyGroupCalls {
            
            if groupCall.groupCallID == groupCallID {

                groupCall.localNotes = localNotes
            }
        }
        
        for groupCall: GroupCall in self.scheduledGroupCalls {
            
            if groupCall.groupCallID == groupCallID {
                
                groupCall.localNotes = localNotes
            }
        }
    }

}
