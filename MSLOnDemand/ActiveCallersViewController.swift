//
//  ActiveCallersViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/14/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class ActiveCallersViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var tblViewActiveList: UITableView!
    @IBOutlet weak var btnAddCaller: UIButton!

    var delegate:CallViewDelegate?

    var users: Array<String>?               // User ID's on group call

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(ActiveCallersViewController.incomingClientChangeMute), name: NSNotification.Name(rawValue: incomingClientMuteNotificationKey), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ActiveCallersViewController.incomingClientChangeMute), name: NSNotification.Name(rawValue: incomingClientUnMuteNotificationKey), object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - IBAction Methods

    @IBAction func addCallerButtonPressed(_ sender: AnyObject) {

        print("addCallerButtonPressed")

        let storyboard = UIStoryboard(name: "Call", bundle: nil)
        let popupVc = storyboard.instantiateViewController(withIdentifier: "AddCallerPopupViewController") as! AddCallerPopupViewController

        popupVc.modalPresentationStyle = .popover
        popupVc.delegate = self

        let popoverPresentationController = popupVc.popoverPresentationController
        
        // result is an optional (but should not be nil if modalPresentationStyle is popover)
        if let _popoverPresentationController = popoverPresentationController {
            
            _popoverPresentationController.sourceView = (sender as! UIView)
            _popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirection.any;
            self.present(popupVc, animated: true, completion: nil)
        }
    }

    // MARK: - Class Methods

    func setUsersOnCall(_ groupUsers: Array<String>) {

        users = groupUsers
        
        tblViewActiveList.reloadData()
    }

    // MARK: - Public Methods
    
    func setUserRejectedCall(_ uId: String) {
        
        print("user rejected call: ", uId)

        userList.setUsersCallState(uId, callState: UserCallState.rejected)
        tblViewActiveList.reloadData()
    }

    func setUserConnectedCall(_ uId: String) {

        print("user connected call: ", uId)

        userList.setUsersCallState(uId, callState: UserCallState.connected)
        tblViewActiveList.reloadData()
    }

    func setUserDisconnectedCall(_ uId: String) {

        print("user disconnected call: ", uId)

        userList.setUsersCallState(uId, callState: UserCallState.disconnected)
        tblViewActiveList.reloadData()
    }

    // MARK: - Notification Methods

    func incomingClientChangeMute(_ notification: Notification) {
        
        let userInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let uID = userInfo["uID"] as! String
        print("user ID to Mute = " + uID)

        let user = userList.getUser(uID)
        
        if user?.isMuted == true {
            
            userList.setUserMuteState(uID, mute: false)
        }
        else {
            
            userList.setUserMuteState(uID, mute: true)
        }
        
        tblViewActiveList.reloadData()
    }

    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        
        print("prepare for presentation")
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        
        print("did dismiss")
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        
        print("should dismiss")
        return true
    }
}

// MARK: - UITableViewDataSource and UITableViewDelegate

extension ActiveCallersViewController: UITableViewDataSource, UITableViewDelegate {

/*
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:  self.view.frame.size.height))
        headerView.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        
        let headerLabel = UILabel(frame: CGRectMake(10, 5, self.view.frame.size.width, 21))
        headerLabel.textAlignment = NSTextAlignment.Left
        headerLabel.textColor = UIColor.blackColor()
        headerLabel.font = UIFont.boldSystemFontOfSize(16)
        headerLabel.text = "Active Callers"
        headerView.addSubview(headerLabel)
        
        return headerView;
    }
*/
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if users == nil {
            
            return 0
        }
        else {
            
            return users!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActiveCallersTableViewCell", for: indexPath) as! ActiveCallersTableViewCell
        
        let uID = users![indexPath.row]
        let user = userList.getUser(uID)
        
        cell.lblUserName.text = user!.jobTitle + " " + (user?.firstName)! + " " + (user?.lastName)!
        cell.lblUserCallState.text = user!.programs + ", " + user!.location


        let thumbnailPath = userList.getUserThumbnailPath(uID)
        if thumbnailPath.isEmpty {
            
            cell.imgAvatar.isHidden = true
        }
        else {

            Alamofire.request(thumbnailPath)
                .responseImage { response in
                    
                    if let image = response.result.value {
                        
                        cell.imgAvatar.isHidden = false
                        cell.imgAvatar.image = image
                    }
                    else {
                        
                        cell.imgAvatar.image = UIImage(named: "avatar-stand-in")
                    }
            }
        }
/*
         let user = userList.getUser(userID)

        var mutedString = ""
        if user?.isMuted == true {
            
            mutedString = ": MUTED"
        }

        let userCallState = userList.getUserCallState(userID)

        cell.lblUserCallState.text = String(format: "State: None%@", mutedString)
        if userCallState == UserCallState.Connected {
            
            cell.lblUserCallState.text = String(format: "State: Connected%@", mutedString)
        } else if userCallState == UserCallState.Disconnected {
            
            cell.lblUserCallState.text = String(format: "State: Disconnected%@", mutedString)
        } else if userCallState == UserCallState.Rejected {
        
            cell.lblUserCallState.text = String(format: "State: Rejected%@", mutedString)
        }
*/
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let userID = self.users![indexPath.row]        
        let user = userList.getUser(userID)
        delegate!.activeUserSelected(user!.uID)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if delegate?.isPresentaCallView() == true {
            
            return true
        }
        else {
            
            return false
/*
            let userID = self.users![indexPath.row]
            let userCallState = userList.getUserCallState(userID)
            if userCallState != .Connected {
                
                return true
            }
            else {
                
                return false
            }
*/
        }
    }

    // Allows iOS 8 editing to work
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        
        if delegate?.isPresentaCallView() == true {
        
            return true
        }
        else {
        
            return false
/*
            let userID = self.users![indexPath.row]
            let userCallState = userList.getUserCallState(userID)
            if userCallState != .Connected {
                
                return true
            }
            else {
                
                return false
            }
*/
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let userID = self.users![indexPath.row]
        let user = userList.getUser(userID)
        
        var rowActions = [UITableViewRowAction]()

        var muteString = "Mute"
        if user?.isMuted == true {
            
            muteString = "Unmute"
        }

//        if delegate?.isPresentaCallView() == true {
        
            let delete = UITableViewRowAction(style: .normal, title: muteString) { action, index in
                
                print("mute user")
                let userID = self.users![indexPath.row]
                if user?.isMuted == true {
                    
                    self.delegate?.unMuteUser(userID)
                }
                else {
                    
                    self.delegate?.muteUser(userID)
                }
            }
            
            delete.backgroundColor = UIColor.red
            
            rowActions.append(delete)
/*
        } else {
            
            let callAudio = UITableViewRowAction(style: .Normal, title: "Audio") { action, index in
                
                print("call user with audio ")
                let userID = self.users![indexPath.row]
                self.delegate?.addUserToCallAudio(userID)
            }
            
            callAudio.backgroundColor = UIColor.greenColor()
            
            rowActions.append(callAudio)

            let videoAudio = UITableViewRowAction(style: .Normal, title: "Video") { action, index in
                
                print("call user with audio ")
                let userID = self.users![indexPath.row]
                self.delegate?.addUserToCallVideo(userID)
            }
            
            videoAudio.backgroundColor = UIColor.blueColor()
            
            rowActions.append(videoAudio)
        }
*/
        return rowActions
    }

}

extension ActiveCallersViewController: CallViewDelegate {
    
    func activeUserSelected(_ uID: String) {
        
    }
    
    func shareDocument(_ dID: String) {
        
    }
    
    func muteUser(_ uID: String) {
        
    }

    func unMuteUser(_ uID: String) {
        
    }

    func isPresentaCallView() -> Bool {

        return false
    }
    
    func addUserToCallAudio(_ uID: String) {
        
        //delegate?.addUserToCallAudio(uID)
    }
    
    func addUserToCallVideo(_ uID: String) {
        
        delegate?.addUserToCallVideo(uID)

        //users?.append(uID)
        
        tblViewActiveList.reloadData()
    }
    
}
