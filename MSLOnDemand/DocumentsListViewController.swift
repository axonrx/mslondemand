//
//  DocumentsListViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/8/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class DocumentsListViewController: UIViewController {
    
    @IBOutlet weak var tblViewCallDocuments: UITableView!
    @IBOutlet weak var btnDocumentAdd: UIButton!

    var selectedGroupCallID: String = String()
    var groupCallDetails: GroupCall?
    var callFolderId = ""
    var isParticipate = false

    var documentIds = Array<String>()

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        groupCallDetails = groupCallList.getGroupCallDetails(selectedGroupCallID)        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        if callFolderId.isEmpty == false {
            
            let folder = folderManager.getFolderByID(callFolderId)
            documentIds = (folder?.dIDs)!
            tblViewCallDocuments.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    @IBAction func documentAddButtonPressed(_ sender: AnyObject) {

        if isParticipate == true {
            
            alertUserParticipate()
            return
        }

        let storyboard = UIStoryboard(name: "Documents", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DocumentsViewController") as! DocumentsViewController
        vc.backButtonString = "< Call Details"
        vc.selectedGroupCallID = selectedGroupCallID
        vc.delegate = self
        vc.hideAddFolderToCall = false
        vc.isSelectFolder = true
        self.present(vc, animated: true, completion: nil)
    }
    
    // MARK: - Class methods
    
    func getDocumentList() -> Array<String> {
        
        return documentIds
    }
    
    func setDocumentList(_ docs: Array<String>) {
        
        documentIds = docs
    }
    
    func alertUserParticipate() {
        
        let alert = UIAlertController(title: "Warning", message: "You are a Participate on this group call and unable to make changes.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in }))
        self.present(alert, animated: true, completion: {})
    }

}

// MARK: - UITableViewDataSource and UITableViewDelegate

extension DocumentsListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return "Selected Resources"
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if documentIds.count == 0 {
            
            return 1
        }
        else {
            
            return documentIds.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentListTableViewCell", for: indexPath) as! DocumentListTableViewCell

        //if documentIds.count == 0 {
            
        //    cell.lblFileName.text = "None Selected"
        //}
        //else {
        if documentIds.count != 0 {
            
            let document = documentManager.getDocumentByID(documentIds[indexPath.row])
            
            cell.lblFileName.text = document!.name
            
            cell.imgFileIcon.image = UIImage(named: "file_list_view_document")
            if document!.name.contains("http:") || document!.name.contains("https:") {
                
                cell.imgFileIcon.image = UIImage(named: "file_list_website")
            }
            else if document!.name.contains(".doc") || document!.name.contains(".docx") {
                
                cell.imgFileIcon.image = UIImage(named: "file_list_word")
            }
            else if document!.name.contains(".ppsx") || document!.name.contains(".ppt") || document!.name.contains(".pptx") {
                
                cell.imgFileIcon.image = UIImage(named: "file_list_powerpoint")
            }
            else if document!.name.contains(".pdf") {
                
                cell.imgFileIcon.image = UIImage(named: "file_list_pdf")
            }
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title: "Remove") { action, index in
                
            //print("document remove")

            self.documentIds.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    
        delete.backgroundColor = UIColor.red
    
        return [delete]
    }

}

extension DocumentsListViewController: DocumentsViewDelegate {
    
    func newFileSelected(_ dID: String) {
        
    }
    
    func newFolderSelected(_ row: Int) {
        
    }
    
    func getFilePreviewNotes() -> String {

        return String()
    }
    
    func fileSelectedMoveToFolder(_ dID: String) {
        
    }
    
    func folderSelectedForCallDetails(_ fID: String) {
        
        callFolderId = fID
        let folder = folderManager.getFolderByID(callFolderId)
        documentIds = (folder?.dIDs)!
        tblViewCallDocuments.reloadData()
    }

    func addFolderToCall() {
        
    }

    func addDocumentToCall(_ dID: String) {
        
        documentIds.append(dID)
        tblViewCallDocuments.reloadData()
    }

    func documentDeletedUpdateFolders(_ dID: String) {
        
    }

    func adjustViewForKeyboard(_ adjust: Bool) {
        
    }

}
