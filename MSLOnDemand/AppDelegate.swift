//
//  AppDelegate.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 6/29/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import AlamofireImage
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        // See https://github.com/Alamofire/AlamofireImage
        // for image cahcing. Need to implement. Uses both NSURLCache and it's own cache.
        _ = AutoPurgingImageCache(memoryCapacity: 100 * 1024 * 1024, preferredMemoryUsageAfterPurge: 60 * 1024 * 1024)

        URLCache.shared = {
            URLCache(memoryCapacity: 4 * 1024 * 1024, diskCapacity: 20 * 1024 * 1024, diskPath: nil)
        }()

        let authorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        switch authorizationStatus {
            
        case .notDetermined:
            // permission dialog not yet presented, request authorization
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo,
                                                      completionHandler: { (granted:Bool) -> Void in
                                                        if (granted == false) {
                                                            
                                                            print(granted)
                                                        }
                                                        else {
                                                            
                                                            print(granted)
                                                        }
            })
        case .authorized: break
        // go ahead
        case .denied, .restricted: break
            // the user explicitly denied camera usage or is not allowed to access the camera devices
        }

        self.window = UIWindow(frame: UIScreen.main.bounds)

        // iOS 10 support
        if #available(iOS 10, *) {
            
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        // iOS 9 support
        else if #available(iOS 9, *) {
            
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }

        let loggedIn = UserDefaults.standard.bool(forKey: "userLoggedIn")

        if loggedIn == false {
            
            if !DeviceType.IS_IPAD && !DeviceType.IS_IPAD_PRO {
                
                let storyboard = UIStoryboard(name: "LoginiPhone", bundle: nil)
                let loginVc = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
                self.window?.rootViewController = loginVc
            }
            else {

                let storyboard = UIStoryboard(name: "Login", bundle: nil)
                let loginVc = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
                self.window?.rootViewController = loginVc
            }
        }
        else {
            
            if !DeviceType.IS_IPAD && !DeviceType.IS_IPAD_PRO {
                
                let mainVcIntial = kConstantObj.SetIntialMainViewController("MainiPhoneViewController")
                self.window?.rootViewController = mainVcIntial
            }
            else {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainVc = storyboard.instantiateViewController(withIdentifier: "MainViewController")
                self.window?.rootViewController = mainVc
            }
        }

        self.window?.makeKeyAndVisible()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.

        //userList.setUserStatus(UserOnlineStatus.offline)
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        //userList.setUserStatus(UserOnlineStatus.offline)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //userList.setUserStatus(UserOnlineStatus.onlineAvailable)
        globalSignal.setupGlobalSignalSession(updateGlobalSignaluID)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    @nonobjc func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData!) {
        
        //let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        //print("APNs device token: \(deviceTokenString)")
        print("Got token data! \(deviceToken)")
        let token = String(format: "%@", deviceToken as CVarArg)
        pushNotifications.registerUserAPNS(deviceToken: token, completion: registerUserDeviceToken)
    }

    @nonobjc func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError!) {
    
        print("Couldn't register: \(error)")
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {

        print("Push notification received: \(data)")
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(center: UNUserNotificationCenter, willPresentNotification notification: UNNotification, withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void) {
        
        //Handle the notification
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(center: UNUserNotificationCenter, didReceiveNotificationResponse response: UNNotificationResponse, withCompletionHandler completionHandler: () -> Void) {
    
        //Handle the notification
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        
        let scheme = url.scheme
        let path = url.path
        let query = url.query
        
        print(scheme!)
        print(path)
        print(query!)
        
        if (path == "/meetingStart") {
            
            var meetingInfo = [String: String]()
            if (scheme == "mslondemand") {
                
                if (url.query != nil) {
                    
                    let queryKeyValue = url.query!.components(separatedBy: "&");
                    for keyValue in queryKeyValue {
                        
                        let kv = keyValue.components(separatedBy: "=");
                        if (kv.count > 1) {
                            
                            //print("\(kv[0]): \(kv[1])");
                            meetingInfo[kv[0]] = kv[1]
                        }
                    }
                }
                
                //print(meetingInfo)
                NotificationCenter.default.post(name: Notification.Name(rawValue: incomingMeetingLinkPressedNotificationKey), object: self, userInfo: meetingInfo)
            }
        }
        
        return true
    }

    // MARK: - Completion Methods

    func updateGlobalSignaluID(_ result: String) {

        if (result == "success") {

            userList.startSendingUserStatusTimer()
            userList.startUserStatusTimeoutTimer()
        }
    }

    func registerUserDeviceToken(_ result: String) {
        
        if (result == "success") {
            
        }
    }

}

