//
//  NotesViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/8/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class NotesViewController: UIViewController {

    @IBOutlet weak var txtViewNotes: UITextView!

    var selectedGroupCallID: String = String()
    var groupCallDetails: GroupCall?

    var delegate:CallDetailsViewDelegate?

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        txtViewNotes.textColor = UIColor(red: 32/255, green: 112/255, blue: 255/255, alpha: 1.0)

        groupCallDetails = groupCallList.getGroupCallDetails(selectedGroupCallID)
        
        if groupCallDetails?.notes.isEmpty == true || selectedGroupCallID.isEmpty {
        
            txtViewNotes.text = ""
        }
        else {
            
            txtViewNotes.text = groupCallDetails?.notes
        }
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - IBActions

    
    // MARK: - Class methods
    
    func getNotes() -> String {
        
        return txtViewNotes.text;
    }

    func setNotes(_ notes: String) {

        if notes.isEmpty == true {
            
            txtViewNotes.text = ""
        }
        else {
            
            txtViewNotes.text = notes
        }
    }

}

extension NotesViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {

        textView.textColor = UIColor.black
        delegate!.noteViewSelected(open: true)
    }
 
    func textViewDidEndEditing(_ textView: UITextView) {

        groupCallDetails?.notes = textView.text
        delegate!.noteViewSelected(open: false)
    }

}
