//
//  FolderViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/14/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class FolderViewController: UIViewController {

    @IBOutlet weak var tblViewFolderList: UITableView!

    var delegate:DocumentsViewDelegate?
    var txtFieldFolderName: UITextField!

    var documentToMoveToFolder = ""
    var textFieldRenameFolderName = ""

    var hideAddFolderToCall = true
    
    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(FolderViewController.updateFolderNotification), name: NSNotification.Name(rawValue: updateFolderNotificationKey), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func newFolderButtonPressed(_ sender: AnyObject) {
    
        delegate!.adjustViewForKeyboard(false)

        let alert = UIAlertController(title: "New Folder", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        textFieldRenameFolderName = ""
        alert.addTextField(configurationHandler: newFolderNameTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in

            self.delegate!.adjustViewForKeyboard(true)

            if self.txtFieldFolderName.text?.characters.count == 0 {
            
                return
            }

            folderManager.newFolder(userList.getLoginUserID(), folderName: self.txtFieldFolderName.text!, documentIDs: Array<String>(), completion: self.folderChangesComplete)
            //print("Folder name : \(self.txtFieldFolderName.text)")
        }))
        self.present(alert, animated: true, completion: {
            
            //print("completion block")
        })
    }

    // MARK: - Class Methods

    func newFolderNameTextField(_ textField: UITextField!) {

        if textFieldRenameFolderName == "" {
            
            textField.placeholder = "Enter Folder Name"
        }
        else {
            
            textField.placeholder = textFieldRenameFolderName
        }

        txtFieldFolderName = textField
    }

    func handleCancel(_ alertView: UIAlertAction!) {
        
        delegate!.adjustViewForKeyboard(true)
    }

    func folderChangesComplete(_ result: String!) {
        
        tblViewFolderList.reloadData()
    }

    func documentAddedToFolder(_ result: String!) {
        
        tblViewFolderList.reloadData()
    }

    func selectFolder(_ row: Int) {

        let path = IndexPath(row: 0, section: 0)
        tblViewFolderList.selectRow(at: path, animated: false, scrollPosition: UITableViewScrollPosition.top)
    }
    
    func deleteFolderComplete(_ result: String) {
        
        tblViewFolderList.reloadData()
    }

    // MARK: - Public Methods

    func documentSelectedMoveToFolder(_ dID: String!) {
        
        documentToMoveToFolder = dID
    }

    func documentDeletedUpdateFolders(_ dID: String!) {
        
        folderManager.updateFoldersDocumentDeleted(dID)
    }


    // MARK: - Notification Methods
    
    func updateFolderNotification() {
        
        tblViewFolderList.reloadData()
    }

    // MARK: - Button target    Methods

    func folderAddToCallButtonPressed(_ sender:UIButton) {
        
        let buttonRow = sender.tag
        print("foldr add to call button pressed for row: ", buttonRow)
        
        delegate!.addFolderToCall()
    }

}

// MARK: - UITableViewDataSource and UITableViewDelegate

extension FolderViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return folderManager.folderList.count + 1   // for view all documents
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "FolderTableViewCell", for: indexPath) as! FolderTableViewCell

        if indexPath.row == 0 {
            
            cell.lblFolderName.text = "View All Documents..."
            cell.lblFolderName.textColor = UIColor.gray
            cell.btnAddToCall.isHidden = true
        }
        else {
            
            let folder = folderManager.folderList[indexPath.row - 1]
            cell.lblFolderName.text = folder.name
            if hideAddFolderToCall == true {
                
                cell.btnAddToCall.isHidden = true
            }
            else {
                
                cell.btnAddToCall.tag = indexPath.row
                cell.btnAddToCall.addTarget(self, action: #selector(FolderViewController.folderAddToCallButtonPressed(_:)), for: UIControlEvents.touchUpInside)
                cell.btnAddToCall.isHidden = false
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // See if user wants to add docuemnt to folder
        if !documentToMoveToFolder.isEmpty && indexPath.row != 0 {
            
            let folder = folderManager.folderList[indexPath.row - 1]
            folder.dIDs.append(documentToMoveToFolder)
            folderManager.updateFolder(folder.fID, folderName: folder.name, documentIDs: folder.dIDs, completion: documentAddedToFolder)
        }

        delegate!.newFolderSelected(indexPath.row)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {

        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            
            //print("folder delete")
            
            let folder = folderManager.folderList[indexPath.row - 1]
            folderManager.deleteFolder(folder.fID, completion: self.deleteFolderComplete)
            folderManager.folderList.remove(at: indexPath.row - 1)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        let rename = UITableViewRowAction(style: .default, title: "Rename") { action, index in
            
            //print("folder rename")

            let folder = folderManager.folderList[indexPath.row - 1]
            self.textFieldRenameFolderName = folder.name

            let alert = UIAlertController(title: "Rename Folder", message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addTextField(configurationHandler: self.newFolderNameTextField)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:self.handleCancel))
            alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
                
                if self.txtFieldFolderName.text?.characters.count == 0 {
                    
                    return
                }
                
                folderManager.updateFolder(folder.fID, folderName: self.txtFieldFolderName.text!, documentIDs: folder.dIDs, completion: self.folderChangesComplete)
                //print("Folder name : \(self.txtFieldFolderName.text)")
            }))
            self.present(alert, animated: true, completion: {
                
                //print("completion block")
            })
        }

        rename.backgroundColor = UIColor.green
        delete.backgroundColor = UIColor.red

        return [delete, rename]
    }

}
