//
//  MainiPhoneViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 8/23/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import MBProgressHUD


class MainiPhoneViewController: UIViewController {

    @IBOutlet weak var contactsContainer: UIView!
    
    var contactsViewController: ContactsViewController?
    var scheduledCallListViewController: ScheduledCallListViewController?

    override func viewDidLoad() {
        
        super.viewDidLoad()

        userList.getUpdatedUsersList(uploadUserListComplete)

        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        contactsViewController = storyboard.instantiateViewController(withIdentifier: "ContactsViewController") as? ContactsViewController
        contactsViewController!.turnOffAddToCallButton = true
        contactsViewController?.isAddCaller = false
        contactsViewController!.delegate = self
        contactsViewController!.mainDelegate = self
        contactsViewController!.view.frame = contactsContainer.bounds
        contactsContainer.addSubview(contactsViewController!.view)
        addChildViewController(contactsViewController!)
        contactsViewController!.didMove(toParentViewController: self)        
    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if applicationSetup.getApplicationSetupNetworkComplete() == true {
            
            Gradient.setBackgroundGradient(self.view)
        }
        else {
            
            applicationSetup.getApplicationSetup("666", completion: { (result, code) in
                
                Gradient.setBackgroundGradient(self.view)
            })
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let vc = segue.destination as? ScheduledCallListViewController, segue.identifier == "ScheduleCallView" {
            
            scheduledCallListViewController = vc
        }
        
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - IBActions Methods

    @IBAction func btnSideMenuPressed(_ sender: UIButton) {

        sideMenuVC.toggleMenu()
    }

    // MARK: - Local Completion Methods

    func uploadUserListComplete(_ result: String, code: String) {
        
        if result == "success" {

            userList.setUserStatus(UserOnlineStatus.onlineAvailable)
            
            groupCallList.getUserGroupCalls(userList.getLoginUserID(), completion: updateScheduledCallsTableView)
            documentManager.getUserDocuments(userList.getLoginUserID(), completion: retrievedUserDocuments)

            contactsViewController?.tblViewContacts.reloadData()
        }
    }

    func retrievedUserDocuments(_ result: String) {
        
        // NOTE(tsd): How should we handle this issue?
        
    }

    func updateScheduledCallsTableView(_ result: String) {
        
        groupCallList.getAllUserGroupCalls(userList.getLoginUserID(), completion: updateSParticipatescheduledCallsTableView)
    }

    func updateSParticipatescheduledCallsTableView(_ result: String) {

        scheduledCallListViewController?.reloadScheduledCallsList()
    }

}

// MARK: - Delegate Methods

extension MainiPhoneViewController: MainViewDelegate {
    
    func searchBarSelected(open: Bool) {
        
    }

    func historyHomeButtonSelected() {
        
    }
    
}

extension MainiPhoneViewController: CallDetailsViewDelegate {
    
    func callCanceledButtonPressed() {
        
    }
    
    func addUserToCallPressed(_ uID: String) {
        
    }
    
    func scheduleCallButtonPressed() {
        
    }
    
    func callNowButtonPressed() {
        
    }
    
    func noteViewSelected(open: Bool) {
        
    }

}
