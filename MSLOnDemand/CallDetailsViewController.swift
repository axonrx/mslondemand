//
//  CallDetailsViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/6/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import AlamofireImage


class CallDetailsViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var contactsContainer: UIView!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var txtGroupCallName: UITextField!
    @IBOutlet weak var imgViewBrandLogo: UIImageView!
    //@IBOutlet weak var lblProgramLocation: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    //@IBOutlet weak var lblUserInfo: UILabel!
    @IBOutlet weak var imgViewAvatar: UIImageView!
    @IBOutlet weak var segControlCallType: UISegmentedControl!
    @IBOutlet weak var segControlCallSchedule: UISegmentedControl!

    @IBOutlet weak var vwSelectedUsers: UIView!
    @IBOutlet weak var vwTitleView: UIView!
    @IBOutlet weak var vwDocuments: UIView!
    @IBOutlet weak var vwNotes: UIView!
    @IBOutlet weak var vwSelectParticipants: UIView!
    
    var txtFieldExternalParticipant: UITextField!

    var callType = GroupCallType.groupCall

    var selectedGroupCallID: String = String()
    var groupCallDetails: GroupCall?
    var groupCallName: String?
    var textFieldExternalParticipant = ""

    var guests: [Any] = []

    var selectedContactsViewController: SelectedContactsViewController?
    var documentListViewController: DocumentsListViewController?
    var notesViewController: NotesViewController?
    var contactsViewController: ContactsViewController?
    var dateTimeViewController: DateTimePickerViewController?

    var changeDateTime: Date?

    var screenMode = 0

    var hud: MBProgressHUD?

    var ignoreKeyboard = false

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        userList.getUpdatedUsersList(uploadUserListComplete)

        if applicationSetup.getApplicationSetupNetworkComplete() == true {
            
            setupApplicationLook()
        }
        else {
            
            applicationSetup.getApplicationSetup("666", completion: { (result, code) in
                
                self.setupApplicationLook()
            })
        }

        if selectedGroupCallID.isEmpty == true {
            
            txtGroupCallName.text = ""
            dateTimeViewController!.setDateTime(Date().getUTCFormateDate())
            changeDateTime = dateTimeViewController!.getDateTime().StringDateTimeDateCallDetails() as Date?
        }
        else {
            
            groupCallDetails = groupCallList.getGroupCallDetails(selectedGroupCallID)
            txtGroupCallName.text = groupCallDetails?.groupCallName
            callType = (groupCallDetails?.callType)!
            selectedContactsViewController?.setGroupCallDetails(groupCallDetails!)

            notesViewController?.setNotes((groupCallDetails?.notes)!)
            
            documentListViewController?.setDocumentList((groupCallDetails?.documents)!)

            dateTimeViewController!.setDateTime((groupCallDetails?.scheduledCallTime)!)
            changeDateTime = dateTimeViewController!.getDateTime().StringDateTimeDateCallDetails() as Date?
        }

        groupCallName = txtGroupCallName.text

        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        contactsViewController = storyboard.instantiateViewController(withIdentifier: "ContactsViewController") as? ContactsViewController
        contactsViewController!.turnOffAddToCallButton = false
        contactsViewController?.isAddCaller = false
        contactsViewController!.delegate = self
        contactsViewController!.mainDelegate = self
        contactsViewController!.view.frame = contactsContainer.bounds
        contactsContainer.addSubview(contactsViewController!.view)
        addChildViewController(contactsViewController!)
        contactsViewController!.didMove(toParentViewController: self)

        if callType == GroupCallType.presentationCall {
            
            segControlCallType.selectedSegmentIndex = 0
        }
        else {
            
            segControlCallType.selectedSegmentIndex = 1
        }

        segControlCallSchedule.selectedSegmentIndex = 1
        dateTimeViewController?.changeButtonToSave()

        lblVersion.text = "Version: " + Bundle.main.releaseVersionNumber! + " Build: " + Bundle.main.buildVersionNumber!

        vwSelectedUsers.layer.borderWidth = 1
        vwSelectedUsers.layer.borderColor = UIColor.darkGray.cgColor

        vwTitleView.layer.borderWidth = 1
        vwTitleView.layer.borderColor = UIColor.darkGray.cgColor

        vwDocuments.layer.borderWidth = 1
        vwDocuments.layer.borderColor = UIColor.darkGray.cgColor

        vwNotes.layer.borderWidth = 1
        vwNotes.layer.borderColor = UIColor.darkGray.cgColor

        vwSelectParticipants.layer.borderWidth = 1
        vwSelectParticipants.layer.borderColor = UIColor.darkGray.cgColor
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        let orient = UIApplication.shared.statusBarOrientation
        
        switch orient {
            
        case .portrait:
            self.screenMode = 0
            break
        default:
            self.screenMode = 1
            break
        }

        NotificationCenter.default.addObserver(self, selector: #selector(CallDetailsViewController.keyboardWillShowNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CallDetailsViewController.keyboardWillHideNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.incomingVideoGroupCallNotification), name: NSNotification.Name(rawValue: incomingVideoGroupCallNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.incomingAudioGroupCallNotification), name: NSNotification.Name(rawValue: incomingAudioGroupCallNotificationKey), object: nil)

        //if selectedGroupCallID.isEmpty == true {
            
        //    selectedContactsViewController?.btnScheduleCall.setTitle("Schedule Call", forState: .Normal)
        //}
        //else {
            
        if selectedGroupCallID.isEmpty == false {

/*
            // Can't cancel a history call
            let convertDate = groupCallDetails?.scheduledCallTime
            let callDate = convertDate!.StringDateTimeDate()
           
            var isViewingHistoryCall = false
            switch callDate!.compare(Date()) {
            case .orderedAscending:
                isViewingHistoryCall = true
            case .orderedDescending:
                isViewingHistoryCall = false
            case .orderedSame:
                isViewingHistoryCall = false
            }

            if isViewingHistoryCall == true {
                
                selectedContactsViewController?.btnCancelCall.isEnabled = false
            }

            selectedContactsViewController?.btnScheduleCall.setTitle("Reschedule", forState: .Normal)
*/
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator);
        
        coordinator.animate(alongsideTransition: nil, completion: {
            _ in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
                
            case .portrait:
                self.screenMode = 0
                break
            default:
                self.screenMode = 1
                break
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: incomingVideoGroupCallNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: incomingAudioGroupCallNotificationKey), object: nil)
    }

    override var prefersStatusBarHidden : Bool {
        
        return true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //print(segue.identifier)
        if let vc = segue.destination as? SelectedContactsViewController, segue.identifier == "SelectedContactsView" {
                
                selectedContactsViewController = vc
                if selectedGroupCallID.isEmpty == false {
                    
                    groupCallDetails = groupCallList.getGroupCallDetails(selectedGroupCallID)
                    selectedContactsViewController?.selectedGroupCallID = self.selectedGroupCallID
                }
                selectedContactsViewController?.groupCallDetails = self.groupCallDetails
                selectedContactsViewController?.hideCancelCallButton = false
                selectedContactsViewController?.groupCallDetails = groupCallDetails
            }

        if let vc = segue.destination as? DateTimePickerViewController, segue.identifier == "DateTimeView" {
            
            dateTimeViewController = vc
            dateTimeViewController?.delegate = self
        }

        if let vc = segue.destination as? DocumentsListViewController, segue.identifier == "DocumentListView" {
                
                vc.selectedGroupCallID = self.selectedGroupCallID
                if self.selectedGroupCallID.isEmpty == true {

                    vc.isParticipate = false
                }
                else {
                        
                    let groupCall = groupCallList.getGroupCallDetails(selectedGroupCallID)
                    vc.isParticipate = groupCall!.isParticipate
                }
                documentListViewController = vc
            }

        if let vc = segue.destination as? NotesViewController, segue.identifier == "NotesView" {
            
            vc.selectedGroupCallID = self.selectedGroupCallID
            vc.delegate = self
            notesViewController = vc
            if groupCallDetails != nil {
                
                notesViewController?.setNotes((groupCallDetails?.notes)!)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - IBActions
    @IBAction func callScheduleSegmentedControlValueChanged(_ segment: UISegmentedControl) {
    
        switch segment.selectedSegmentIndex {
            
        case 0: // NOW
            dateTimeViewController?.changeButtonToCallNow()
        case 1: // LATER
            dateTimeViewController?.changeButtonToSave()
        default:
            break;
        }
    }
    
    @IBAction func callTypeSegmentedControlValueChanged(_ segment: UISegmentedControl) {
        
        switch segment.selectedSegmentIndex {
            
        case 0: // Presentation
            callType = GroupCallType.presentationCall
        case 1: // Group Call
            callType = GroupCallType.groupCall
        default:
            break;
        }
    }

    @IBAction func addExternalParticipantButtonPressed(_ sender: Any) {
    
        ignoreKeyboard = true
        let alert = UIAlertController(title: "Enter Email Address", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        textFieldExternalParticipant = ""
        alert.addTextField(configurationHandler: newExternalParticipantTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            
            self.ignoreKeyboard = false

            if self.txtFieldExternalParticipant.text?.characters.count == 0 {
                
                return
            }

            print("Send email out to that email address : \(self.txtFieldExternalParticipant.text)")
            self.guests.append(["email" : self.txtFieldExternalParticipant.text])
        }))
        self.present(alert, animated: true, completion: {
            
            self.ignoreKeyboard = false
            //print("completion block")
        })
    }
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {

        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - Keyboard Notifications
    
    func keyboardWillShowNotification(_ notification: Notification) {

        // NOTE(tsd): This "ignoreKeyboard" is a cluster fuck. Wish I had time to write it correctly. Oh well......
        if ignoreKeyboard == true {
        
            return
        }
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.view.frame.origin.y -= keyboardSize.height
        }
        
    }
    
    func keyboardWillHideNotification(_ notification: Notification) {
        
        if ignoreKeyboard == false {
            
            return
        }

        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.view.frame.origin.y += keyboardSize.height
        }
    }

    // MARK: - Notification Methods
    
    func incomingVideoGroupCallNotification(_ notification: Notification) {
        
        let videoInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let groupCallID = videoInfo["groupCallID"] as! String
        print("GroupCall ID = " + groupCallID)
        let groupCallName = videoInfo["groupCallName"] as! String
        print("GroupCall Name = " + groupCallName)
        let groupCallUserId = videoInfo["uID"] as! String
        print("FROM: " + groupCallUserId)
        //let users = videoInfo["users"] as! Array<String>
        
        let alert = UIAlertController(title: "Video Group Call", message: "Do you want to join group call: " + groupCallName + " from " + userList.getUserName(groupCallUserId), preferredStyle: UIAlertControllerStyle.alert)
        
        let rejectCallAction = UIAlertAction(title: "Reject", style: .cancel) { (action:UIAlertAction!) in
            
            globalSignal.sendGlobalSignalRejectCall(groupCallID)
        }
        alert.addAction(rejectCallAction)
        
        let OKAction = UIAlertAction(title: "Accept", style: .default) { (action:UIAlertAction!) in
            
            userList.setUserStatus(UserOnlineStatus.onlineOnCall)
            
            print("Accept Video Group Call button pressed")
            var storyboard = UIStoryboard(name: "Call", bundle: nil)
            if self.screenMode == 1 {
                
                storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
            }
            let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = groupCallID
            vc.isPresentation = false
            vc.remoteGroupCallInfo = videoInfo
            vc.callType = .groupCall
            vc.isRemoteRequestForGroupCall = true
            vc.isEmailLinkGroupCall = false
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func incomingAudioGroupCallNotification(_ notification: Notification) {
        
        let audioInfo = notification.userInfo as! Dictionary<String,AnyObject>
        
        let groupCallID = audioInfo["groupCallID"] as! String
        print("GroupCall ID = " + groupCallID)
        let groupCallName = audioInfo["groupCallName"] as! String
        print("GroupCall Name = " + groupCallName)
        let groupCallUserId = audioInfo["uID"] as! String
        print("FROM: " + groupCallUserId)
        
        let alert = UIAlertController(title: "Audio Group Call", message: "Do you want to join a Audio Group Call: " + groupCallName + " from " + userList.getUserName(groupCallUserId), preferredStyle: UIAlertControllerStyle.alert)
        
        let rejectCallAction = UIAlertAction(title: "Reject", style: .cancel) { (action:UIAlertAction!) in
            
            globalSignal.sendGlobalSignalRejectCall(groupCallID)
        }
        alert.addAction(rejectCallAction)
        
        let OKAction = UIAlertAction(title: "Accept", style: .default) { (action:UIAlertAction!) in
            
            userList.setUserStatus(UserOnlineStatus.onlineOnCall)
            
            print("Accept Audio Call button pressed")
            var storyboard = UIStoryboard(name: "Call", bundle: nil)
            if self.screenMode == 1 {
                
                storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
            }
            let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
            vc.modalPresentationStyle = .fullScreen
            vc.selectedGroupCallID = groupCallID
            vc.isPresentation = false
            vc.isRemoteRequestForGroupCall = true
            vc.isEmailLinkGroupCall = false
            vc.remoteGroupCallInfo = audioInfo
            vc.callType = .audioCall
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(OKAction)
        
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Local methods
    
    func newExternalParticipantTextField(_ textField: UITextField!) {
        
        if textFieldExternalParticipant == "" {
            
            textField.placeholder = "xxx@email.com"
        }
        else {
            
            textField.placeholder = textFieldExternalParticipant
        }
        
        txtFieldExternalParticipant = textField
    }

    func handleCancel(_ alertView: UIAlertAction!) {
        
        ignoreKeyboard = false
    }

    func setupApplicationLook() {
        
        let imageUrl = applicationSetup.getApplicationLogoUrl()
        if imageUrl != nil {
            
            Alamofire.request(imageUrl!)
                .responseImage { response in
                    
                    if let image = response.result.value {
                        
                        self.imgViewBrandLogo.image = image
                    }
            }
        }

        let backgroundUrl = applicationSetup.getApplicationBackgroundUrl()
        if backgroundUrl != nil {
            
            Alamofire.request(backgroundUrl!)
                .responseImage { response in
                    //debugPrint(response)
                    //print(response.request)
                    //print(response.response)
                    //debugPrint(response.result)
                    
                    if let image = response.result.value {
                        
                        //print("image downloaded: \(image)")
                        self.imgBackground.image = image
                    }
                    else {
                        
                        Gradient.setBackgroundGradient(self.view)
                    }
            }
        }
        else {
            
            Gradient.setBackgroundGradient(self.view)
        }
    }

    // MARK: - Local Completion Methods
    
    func uploadUserListComplete(_ result: String, code: String) {
        
        if result == "success" {
            
            if (UserDefaults.standard.object(forKey: "userAvaialble") as! Bool) == true {
                
                userList.setUserStatus(UserOnlineStatus.onlineAvailable)
            }

            let userPicPath = userList.userLoggedIn?.picPath
            //print(userPicPath)
            
            Alamofire.request(userPicPath!)
                .responseImage { response in
                    //debugPrint(response)
                    //print(response.request)
                    //print(response.response)
                    //debugPrint(response.result)
                    
                    if let image = response.result.value {
                        
                        let circularImage = image.af_imageRoundedIntoCircle()
                        self.imgViewAvatar.image = circularImage
                    }
            }
            
            // Update login user image and user name
            //let user = userList.userLoggedIn
            //lblUserInfo.text = user!.jobTitle + " " + user!.firstName + " " + user!.lastName + ", " + user!.credentials
            //lblProgramLocation.text = user!.programs + ", " + user!.location
            
            contactsViewController?.tblViewContacts.reloadData()
        }
    }

    func groupCallUpdated(_ result: String) {
        
        groupCallList.getAllUserGroupCalls(userList.getLoginUserID(), completion: groupCallAllGrupCallsUpdated)
    }

    func groupCallAllGrupCallsUpdated(_ result: String) {
        
    }

    func groupCallCreated(_ result: String, gID: String) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: incomingCalendarUpdateNotificationKey), object: self)
    }

    func newGroupCallCreated(_ result: String, gID: String) {
        
        hud?.hide(animated: true)

        var storyboard = UIStoryboard(name: "Call", bundle: nil)
        if self.screenMode == 1 {
            
            storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
        }
        let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
        vc.modalPresentationStyle = .fullScreen
        vc.selectedGroupCallID = gID
        vc.isPresentation = false
        vc.isRemoteRequestForGroupCall = false
        vc.isEmailLinkGroupCall = false
        self.present(vc, animated: true, completion: nil)
    }

    func groupCallNowUpdated(_ result: String) {

        groupCallList.getAllUserGroupCalls(userList.getLoginUserID(), completion: groupCallGetAllComplete)
    }

    func groupCallGetAllComplete(_ result: String) {
        
        hud?.hide(animated: true)
        
        var storyboard = UIStoryboard(name: "Call", bundle: nil)
        if self.screenMode == 1 {
            
            storyboard = UIStoryboard(name: "Call_Landscape", bundle: nil)
        }
        let vc = storyboard.instantiateViewController(withIdentifier: "CallViewController") as! CallViewController
        vc.modalPresentationStyle = .fullScreen
        vc.selectedGroupCallID = selectedGroupCallID
        vc.isPresentation = false
        vc.isRemoteRequestForGroupCall = false
        vc.isEmailLinkGroupCall = false
        self.present(vc, animated: true, completion: nil)
    }

    func groupCallCanceled(_ result: String) {

    }

    func groupCallLocalNotesUpdated(_ result: String) {
        
    }

    func handleGroupCallStateChange() {

        if selectedGroupCallID.isEmpty == false {
            
            groupCallDetails = groupCallList.getGroupCallDetails(selectedGroupCallID)

            groupCallDetails?.documents = documentListViewController!.getDocumentList()
            groupCallDetails?.groupCallName = groupCallName!
            groupCallDetails?.notes = (notesViewController?.getNotes())!
 
            let stringDateTime = dateTimeViewController!.getDateTime()
            let scheduleDateTime = stringDateTime.StringDateTimeDate()
            let utcDateTime = scheduleDateTime!.getUTCFormateDate()
            groupCallDetails?.scheduledCallTime = utcDateTime
            
            let users = selectedContactsViewController!.getGroupCallUsers()
            groupCallDetails?.users = users
            
            groupCallList.updateGroupCallDetails(selectedGroupCallID, completion: groupCallUpdated)
        }
        else {
            
            let users = selectedContactsViewController!.getGroupCallUsers()
            let docIds = documentListViewController!.getDocumentList()
            
            // Only save a new call if user made some valid changes
            if users.count == 0 && docIds.count == 0 {
                
                return
            }

            let notes = (notesViewController?.getNotes())!
            
            let scheduleDateTime = (dateTimeViewController?.getDateTime())!

            groupCallName = txtGroupCallName.text

            groupCallList.postNewGroupCallDetails(groupCallName!, scheduled: scheduleDateTime, users: users, notes: notes, documents: docIds, type: callType, completion: groupCallCreated)
        }
    }

    // MARK: - UITextFieldDelegate Methods

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
     
        ignoreKeyboard = true
        return true;
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    
        ignoreKeyboard = false
        return true;
    }

}

extension CallDetailsViewController: CallDetailsViewDelegate {
    
    func noteViewSelected(open: Bool) {
        
        ignoreKeyboard = open
    }

    func callCanceledButtonPressed() {
        
        if groupCallDetails?.isParticipate == true {
            
            alertUserParticipate()
            return
        }

        // Send cancel call email to users

        if selectedGroupCallID.isEmpty == false {
 
            groupCallList.cancelGroupCall(selectedGroupCallID, completion: groupCallLocalNotesUpdated)
        }

        self.dismiss(animated: true, completion: nil)
    }

    func addUserToCallPressed(_ uID: String) {

        if selectedContactsViewController?.selectedUsers.contains(uID) == true {
            
            return
        }
        
        selectedContactsViewController?.addUserToCall(uID)
    }

    func alertUserParticipate() {
        
        let alert = UIAlertController(title: "Warning", message: "You are a Participate on this group call and unable to make changes.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in }))
        self.present(alert, animated: true, completion: {})
    }

    func scheduleCallButtonPressed() {

        if groupCallDetails?.isParticipate == true {

            alertUserParticipate()
            return
        }

        let currentCallDateTime = dateTimeViewController!.getDateTime().StringDateTimeDateCallDetails()
        var userMadeChanges = false
        if changeDateTime != nil && currentCallDateTime != nil {
            
            switch currentCallDateTime!.compare(changeDateTime! as Date) {
            case .orderedAscending:
                userMadeChanges = true
            case .orderedDescending:
                userMadeChanges = true
            case .orderedSame:
                userMadeChanges = false
            }
        }

        if userMadeChanges == false {
            
            let alert = UIAlertController(title: "Issue", message: "Please adjust date/time to schedule the call", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in }))
            self.present(alert, animated: true, completion: {})
        }
        else {
         
            handleGroupCallStateChange()

            // Send schedule email to users
    
            let scheduleDateTime = dateTimeViewController!.getDateTime()

            let usersToEmail = selectedContactsViewController!.getGroupCallUsers()
            var users: [Dictionary <String, String>] = []
            for uID in usersToEmail {
                
                users.append(["uID" : uID])
            }
            
            let parameters = [
                "token": MSLConnect_Constants.apiToken,
                "command": "CREATEEMAILMEETING",
                "uID": userList.getLoginUserID(),
                "meetingTime": scheduleDateTime,
                "subject": "MSLOnDemand Call scheduled",
                "text": "You have a MSLOnDemand call for " + scheduleDateTime,
                "users": users,
                "guests": guests,
                ] as [String : Any]
            
            let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
            let jsonString = String(data: options, encoding: String.Encoding.utf8)
            let jsonParams = ["json" : jsonString as AnyObject]
            
            Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    
                    if let result = response.result.value {
                        
                        let JSON = result as! NSDictionary
                        //print("JSON: \(JSON)")
                        
                        if JSON["status"] as! String == "success" {
                            /*
                             let response = JSON as! NSDictionary
                             
                             let result = response.objectForKey("result") as! NSDictionary
                             
                             let meetingID = result.objectForKey("meeting_id") as! String
                             print("meetingID = " + meetingID)
                             */
                            print("success")
                        }
                        else {
                            
                            print("failed")
                        }
                    }
                    else {
                        
                        print("Error parsing CREATEEMAILMEETING response JSON")
                    }
                    break
                    
                case .failure(_):
                    print(response.result.error!)
                    break
                    
                }
            }
            
            self.dismiss(animated: true, completion: nil)
        }
    }

    func callNowButtonPressed() {

        if groupCallDetails?.isParticipate == true {
            
            alertUserParticipate()
            return
        }

        // If this is a new group call then save if off before call. Need to get OpenTok session, etc.
        hud = MBProgressHUD.showAdded(to: self.view, animated:true)
        hud!.mode = MBProgressHUDMode.indeterminate

        if selectedGroupCallID.isEmpty == true {

            hud!.label.text = "Starting Call..."

            let docIds = documentListViewController!.getDocumentList()
            let users = selectedContactsViewController!.getGroupCallUsers()
            let notes = (notesViewController?.getNotes())!
            
            let scheduleDateTime = (dateTimeViewController?.getDateTime())!
            
            groupCallName = txtGroupCallName.text

            groupCallList.postNewGroupCallDetails(groupCallName!, scheduled: scheduleDateTime, users: users, notes: notes, documents: docIds, type: callType, completion: newGroupCallCreated)
        }
        else {
            
            hud!.label.text = "Updating Call Information..."

            groupCallDetails = groupCallList.getGroupCallDetails(selectedGroupCallID)
            
            groupCallDetails?.callType = callType
            groupCallDetails?.documents = documentListViewController!.getDocumentList()
            groupCallDetails?.groupCallName = groupCallName!
            groupCallDetails?.notes = (notesViewController?.getNotes())!
            groupCallDetails?.scheduledCallTime = (dateTimeViewController?.getDateTime())!
            groupCallDetails?.users = selectedContactsViewController!.getGroupCallUsers()
            
            groupCallList.updateGroupCallDetails(selectedGroupCallID, completion: groupCallNowUpdated)
        }
    }

}

// MARK: - MainViewDelegate Methods

extension CallDetailsViewController: MainViewDelegate {
    
    func searchBarSelected(open: Bool) {
     
        ignoreKeyboard = open
    }
    
    func historyHomeButtonSelected() {
        
    }

}
