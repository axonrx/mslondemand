//
//  SetupCalliPhoneViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 8/30/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import MobileCoreServices


class SetupCalliPhoneViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var contactsContainer: UIView!
    @IBOutlet weak var selectedContactsContainer: UIView!

    var popDatePicker: PopDatePicker?
    var contactsViewController: ContactsViewController?
    var selectedContactsViewController: SelectedContactsViewController?
    var changeDateTime: Date?
    var hud: MBProgressHUD?

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        Gradient.setBackgroundGradient(self.view)

        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)

        contactsViewController = mainStoryboard.instantiateViewController(withIdentifier: "ContactsViewController") as? ContactsViewController
        contactsViewController!.turnOffAddToCallButton = false
        contactsViewController?.isAddCaller = false
        contactsViewController!.delegate = self
        contactsViewController!.view.frame = contactsContainer.bounds
        contactsContainer.addSubview(contactsViewController!.view)
        addChildViewController(contactsViewController!)
        contactsViewController!.didMove(toParentViewController: self)

        let callStoryboard = UIStoryboard(name: "CallDetails", bundle: nil)

        selectedContactsViewController = callStoryboard.instantiateViewController(withIdentifier: "SelectedContactsViewController") as? SelectedContactsViewController
        selectedContactsViewController?.hideCancelCallButton = true
        selectedContactsViewController!.view.frame = selectedContactsContainer.bounds
        selectedContactsContainer.addSubview(selectedContactsViewController!.view)
        addChildViewController(selectedContactsViewController!)
        selectedContactsViewController!.didMove(toParentViewController: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions Methods
    
    @IBAction func btnSideMenuPressed(_ sender: UIButton) {
        
        sideMenuVC.toggleMenu()
    }

    // MARK: - Completion Methods
    
    func newGroupCallCreated(_ result: String, gID: String) {
        
        hud?.hide(animated: true)
        
        let storyboard = UIStoryboard(name: "CalliPhone", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CalliPhoneViewController") as! CalliPhoneViewController
        vc.modalPresentationStyle = .fullScreen
        vc.selectedGroupCallID = gID
        vc.callType = .groupCall
        vc.isPresentation = false
        vc.isRemoteRequestForGroupCall = false
        vc.isEmailLinkGroupCall = false
        self.present(vc, animated: true, completion: nil)
    }

    func scheduleGroupCallCreated(_ result: String, gID: String) {
        
        let alert = UIAlertController(title: "Schedule Call", message: "Call has been scheduled successfully.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in }))
        self.present(alert, animated: true, completion: {})
    }

}

extension SetupCalliPhoneViewController: CallDetailsViewDelegate {
    
    func callCanceledButtonPressed() {
        
    }
    
    func addUserToCallPressed(_ uID: String) {
        
        if selectedContactsViewController?.selectedUsers.contains(uID) == true {
            
            return
        }
        
        selectedContactsViewController?.addUserToCall(uID)
    }
    
    func scheduleCallButtonPressed() {
/*
        let dateAsString = (selectedContactsViewController?.getDateTime())!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm a"
        let currentCallDateTime = dateFormatter.date(from: dateAsString)

        var userMadeChanges = false
        if changeDateTime != nil && currentCallDateTime != nil {
            
            switch currentCallDateTime!.compare(changeDateTime!) {
            case .orderedAscending:
                userMadeChanges = true
            case .orderedDescending:
                userMadeChanges = true
            case .orderedSame:
                userMadeChanges = false
            }
        }
        
        if userMadeChanges == false {
            
            let alert = UIAlertController(title: "Issue", message: "Please adjust date/time to Rreschedule the call", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in }))
            self.present(alert, animated: true, completion: {})
        }
        else {
            
            let users = selectedContactsViewController!.getGroupCallUsers()
            let docIds = Array<String>()
            
            // Only save a new call if user made some valid changes
            if users.count == 0 && docIds.count == 0 {
                
                return
            }
            
            let callType = GroupCallType.groupCall
            let notes = ""
            
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let scheduleDateTime = dateFormatter.string(from: currentCallDateTime!)

            let groupCallName = "iPhone Call - " + (selectedContactsViewController?.getDateTime())!

            groupCallList.postNewGroupCallDetails(groupCallName, scheduled: scheduleDateTime, users: users, notes: notes, documents: docIds, type: callType, completion: scheduleGroupCallCreated)
            
            // Send schedule email to users

            let usersToEmail = selectedContactsViewController!.getGroupCallUsers()
            var emailUsers = [[String: String]]()
            for uID in usersToEmail {
                
                emailUsers.append(["uID" : uID])
            }
            
            let guests: [AnyObject] = []
            //guests.append(["email" : "tomdiz@yahoo.com"])
            
            let parameters = [
                "token": MSLConnect_Constants.apiToken,
                "command": "CREATEEMAILMEETING",
                "uID": userList.getLoginUserID(),
                "meetingTime": scheduleDateTime,
                "subject": "MSLOnDemand Call scheduled",
                "text": String(format: "You have a MSLOnDemand call for %@", scheduleDateTime),
                "users": emailUsers,
                "guests": guests,
                ] as [String : Any]
            
            let options = try! JSONSerialization.data(withJSONObject: parameters, options: [])
            let jsonString = String(data: options, encoding: String.Encoding.utf8)
            let jsonParams = ["json" : jsonString as AnyObject]
            
            Alamofire.request(MSLConnect_Constants.baseURL, method: .get, parameters: jsonParams, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    
                    if let result = response.result.value {
                        
                        let JSON = result as! NSDictionary
                        //print("JSON: \(JSON)")
                        
                        if JSON["status"] as! String == "success" {
                            
                            print("success")
                        }
                        else {
                            
                            print("failed")
                        }
                    }
                    else {
                        
                        print("Error parsing CREATEEMAILMEETING response JSON")
                    }
                    break
                    
                case .failure(_):
                    print(response.result.error!)
                    print("failed", response.result.error!.localizedDescription)
                    break
                    
                }
            }
        }
 */
    }
    
    func callNowButtonPressed() {
        
        // If this is a new group call then save if off before call. Need to get OpenTok session, etc.
        hud = MBProgressHUD.showAdded(to: self.view, animated:true)
        hud!.mode = MBProgressHUDMode.indeterminate
        
        hud!.label.text = "Starting Call..."

        let docIds = Array<String>()
        let callType = GroupCallType.groupCall
        let users = selectedContactsViewController!.getGroupCallUsers()
        let notes = ""
        
        let scheduleDateTime = "";//(selectedContactsViewController?.getDateTime())!
        let groupCallName = "iPhone Call - "// + (selectedContactsViewController?.getDateTime())!
        
        groupCallList.postNewGroupCallDetails(groupCallName, scheduled: scheduleDateTime, users: users, notes: notes, documents: docIds, type: callType, completion: newGroupCallCreated)
    }
    
    func noteViewSelected(open: Bool) {
        
    }

}
