//
//  FileSelectViewController.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/14/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class FileSelectViewController: UIViewController {

    @IBOutlet weak var tblShareFileList: UITableView!

    var delegate:CallViewDelegate?
    var selectedGroupCallID: String = String()

    var documentIds = Array<String>()        // Document ID's on group call
    var currentSharedDocumentID = ""

    // MARK: - UIViewController handlers
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func addDocumentToCallButtonPressed(_ sender: AnyObject) {

        let storyboard = UIStoryboard(name: "Documents", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DocumentsViewController") as! DocumentsViewController
        vc.backButtonString = "< Call"
        vc.selectedGroupCallID = selectedGroupCallID
        vc.delegate = self
        vc.hideAddFolderToCall = false
        vc.isSelectFolder = true
        self.present(vc, animated: true, completion: nil)
    }

    // MARK: - Class Methods

    func setDocumentIDs(_ docIDs: Array<String>) {
        
        documentIds = docIDs
        tblShareFileList.reloadData()
    }

}

// MARK: - UITableViewDataSource and UITableViewDelegate

extension FileSelectViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return documentIds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShareFileTableViewCell", for: indexPath) as! ShareFileTableViewCell
        
        if documentIds.count != 0 {
            
            let document = documentManager.getDocumentByID(documentIds[indexPath.row])
            
            cell.lblFileName.text = document!.name
            
            cell.imgFileIcon.image = UIImage(named: "file_list_view_document")
            if document!.name.contains("http:") || document!.name.contains("https:") {
                
                cell.imgFileIcon.image = UIImage(named: "file_list_website")
            }
            else if document!.name.contains(".doc") || document!.name.contains(".docx") {
                
                cell.imgFileIcon.image = UIImage(named: "file_list_word")
            }
            else if document!.name.contains(".ppsx") || document!.name.contains(".ppt") || document!.name.contains(".pptx") {
                
                cell.imgFileIcon.image = UIImage(named: "file_list_powerpoint")
            }
            else if document!.name.contains(".pdf") {
                
                cell.imgFileIcon.image = UIImage(named: "file_list_pdf")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let document = documentManager.getDocumentByID(documentIds[indexPath.row])
        delegate?.shareDocument((document?.dID)!)
    }
    
}

extension FileSelectViewController: DocumentsViewDelegate {
    
    func newFileSelected(_ dID: String) {
        
    }
    
    func newFolderSelected(_ row: Int) {
        
    }
    
    func getFilePreviewNotes() -> String {
        
        return String()
    }
    
    func fileSelectedMoveToFolder(_ dID: String) {
        
    }
    
    func folderSelectedForCallDetails(_ fID: String) {
        
        let folder = folderManager.getFolderByID(fID)
        documentIds = (folder?.dIDs)!

        tblShareFileList.reloadData()
    }
    
    func addFolderToCall() {
        
    }
    
    func addDocumentToCall(_ dID: String) {
        
        documentIds.append(dID)

        tblShareFileList.reloadData()
    }
    
    func documentDeletedUpdateFolders(_ dID: String) {
        
    }
    
    func adjustViewForKeyboard(_ adjust: Bool) {
        
    }
    
}
