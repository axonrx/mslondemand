//
//  HistoryTableViewCell.swift
//  MSLOnDemand
//
//  Created by Chris Mutkoski on 7/6/16.
//  Copyright © 2016 Chris Mutkoski. All rights reserved.
//

import UIKit


class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var btnCallDetails: UIButton!
    @IBOutlet weak var lblCallTitle: UILabel!
    @IBOutlet weak var lblCallTime: UILabel!
    @IBOutlet weak var lblCallNames: UILabel!
    @IBOutlet weak var imgViewCallType: UIImageView!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
}
